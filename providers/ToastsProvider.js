import React, { useCallback, useMemo, useState } from "react";
import Toast from "../components/Toast";
import ToastsContainer from "../components/ToastsContainer";
import { createPortal } from "react-dom";
import { nanoid } from "@reduxjs/toolkit";
const autoDismissAfterMs = 10 * 1000;
export const ToastsContext = React.createContext({
  addToast: () => {
    throw new Error("To add a toast, wrap the app in a ToastsProvider.");
  },
  clearToasts: () => {
    throw new Error("To remove toasts, wrap the app in a ToastsProvider.");
  },
});
export default function ToastsProvider({ children, }) {
  const [toasts, setToasts] = useState([]);
  const addToast = useCallback((content, options = {}) => {
    const { autoDismiss = true } = options;
    const toastId = nanoid();
    const removeToast = () => {
      // O(n) is the best we can do here and is fine given the use case
      setToasts((prevToasts) => prevToasts.filter(({ id }) => id !== toastId));
    };
    let timeoutHandle;
    if (autoDismiss) {
      timeoutHandle = setTimeout(removeToast, autoDismissAfterMs);
    }
    // create toast object
    const toast = {
      id: toastId,
      content,
      autoDismiss,
      remove: () => {
        removeToast();
        timeoutHandle && clearTimeout(timeoutHandle);
      },
    };
    // add toast to list
    setToasts((prevToasts) => [...prevToasts, toast]);
    // return callback to kill toast
    return removeToast;
  }, []);
  const clearToasts = useCallback(() => {
    setToasts([]);
  }, []);
  const contextValue = useMemo(() => ({
    addToast,
    clearToasts,
  }), [addToast, clearToasts]);
  return (React.createElement(ToastsContext.Provider, { value: contextValue },
    children,
    createPortal(React.createElement(ToastsContainer, null, toasts.map(({ id, remove, content: { type, title } }) => (React.createElement(Toast, { key: id, type: type, title: title, onClick: remove })))), document.body)));
}