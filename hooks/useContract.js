import {
  DAI,
  USDC,
  USDT,
  BUSD,
  BNB,
  UST,
  BTCB,
  renBTC,
  // STABLECOIN_POOL_NAME,
  // STABLECOIN_SWAP_ADDRESSES,
  // STABLECOIN_SWAP_TOKEN,
  STABLE_COIN_POOL_NAME,
  STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES,
  UST_SWAP_TOKEN_CONTRACT_ADDRESSES,
  TWO_SWAP_TOKEN_CONTRACT_ADDRESSES,
  DOLLY_MINT_SWAP_TOKEN_CONTRACT_ADDRESSES,
  DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES,
  BTC_SWAP_TOKEN_CONTRACT_ADDRESSES,
  STABLE_SWAP_TOKEN,
  TWO_POOL_SWAP_TOKEN,
  BTC_POOL_SWAP_TOKEN,
  DOLLY_MINT_POOL_SWAP_TOKEN,
  DOLLY_POOL_SWAP_TOKEN,
  UST_POOL_SWAP_TOKEN,
  SWAP_CONTRACT_ADDRESS,
  CAKE_LP_TOKEN_ADDRESSES,
  FAIRLAUNCH_CONTRACT_ADDRESSES,
  UST_POOL_NAME,
  TWO_POOL_NAME,
  DOLLY_POOL_NAME,
  DOLLY_MINT_POOL_NAME,
  BTC_POOL_NAME,
  DOLLY,
  FAIRLAUNCH_ABIS,
} from "../constants";
import { useMemo, useState } from "react";
import ERC20_ABI from "../constants/abis/ERC20.abi.json";
import DOPPLE_TOKEN_ABI from "../constants/abis/Dopple.abi.json";
import SWAP_LP_TOKEN_ABI from "../constants/abis/swapTokenLP.abi.json";
import LPTOKEN_ABI from "../constants/abis/lpToken.abi.json";
import SWAP_ABI from "../constants/abis/swap.abi.json";
import DOLLY_SWAP_ABI from "../constants/abis/dollySwap.abi.json";
import CAKE_ABI from "../constants/abis/cake.abi.json";
import STAKING_TOKEN_ABI from "../constants/abis/stakingToken.abi.json";
import FACTORY_ABI from "../constants/abis/IUniswapV2Factory.json";
import PAIR_ABI from "../constants/abis/IUniswapV2Pair.json";
import ROUTER_ABI from "../constants/abis/IUniswapV2Router02.json";
import DIVIDEND_ABI from "../constants/abis/dividend.json";
import { getContract } from "../utils/index";
import { useActiveWeb3React } from "./index";
// returns null on errors
function useContract(
  address,
  ABI, // eslint-disable-line @typescript-eslint/no-explicit-any
  withSignerIfPossible = true
) {
  const { library, account } = useActiveWeb3React();
  return useMemo(() => {
    if (!address || !ABI || !library) return null;
    try {
      return getContract(
        address,
        ABI,
        library,
        withSignerIfPossible && account ? account : undefined
      );
    } catch (error) {
      console.error("Failed to get contract", error);
      return null;
    }
  }, [address, ABI, library, withSignerIfPossible, account]);
}
export function useTokenContract(t, withSignerIfPossible) {
  const { chainId } = useActiveWeb3React();
  const tokenAddress = chainId ? t.addresses[chainId] : undefined;
  return useContract(tokenAddress, DOPPLE_TOKEN_ABI, withSignerIfPossible);
}
export function useDiamondHandTokenContract(t, withSignerIfPossible) {
  const { chainId } = useActiveWeb3React();
  const tokenAddress = chainId ? t.addresses[chainId] : undefined;
  return useContract(tokenAddress, DIVIDEND_ABI, withSignerIfPossible);
}
export function useSwapLPContract(t, withSignerIfPossible) {
  const { chainId } = useActiveWeb3React();
  const tokenAddress = chainId ? t.addresses[chainId] : undefined;
  return useContract(tokenAddress, SWAP_LP_TOKEN_ABI, withSignerIfPossible);
}
export function useFairLaunchContract() {
  const withSignerIfPossible = true;
  const { chainId } = useActiveWeb3React();
  const fairLaunchContract = useContract(
    chainId ? FAIRLAUNCH_CONTRACT_ADDRESSES[chainId] : undefined,
    chainId ? FAIRLAUNCH_ABIS[chainId] : undefined,
    withSignerIfPossible
  );
  return fairLaunchContract;
}
export function useStakingTokenContract(t, withSignerIfPossible) {
  const { chainId } = useActiveWeb3React();
  const tokenAddress = chainId ? t.addresses[chainId] : undefined;
  return useContract(tokenAddress, STAKING_TOKEN_ABI, withSignerIfPossible);
}
export function useSwap() {
  const { chainId } = useActiveWeb3React();
  const swapContract = useContract(
    chainId ? SWAP_CONTRACT_ADDRESS[chainId] : undefined,
    SWAP_ABI
  );
  return swapContract;
}

export function useCakeLP() {
  const { chainId } = useActiveWeb3React();
  const cakeLPContract = useContract(
    chainId ? CAKE_LP_TOKEN_ADDRESSES[chainId] : undefined,
    CAKE_ABI
  );
  return cakeLPContract;
}

export function useDividendContract(
  address,
  ABI,
  withSignerIfPossible = false
) {
  return useContract(address, ABI, withSignerIfPossible);
}

// TODO rename
export function useSwapContract(poolName) {
  const withSignerIfPossible = true;
  const { chainId } = useActiveWeb3React();

  const customSwapContract = useContract(
    chainId ? STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    SWAP_ABI,
    withSignerIfPossible
  );
  const ustPoolSwapContract = useContract(
    chainId ? UST_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    SWAP_ABI,
    withSignerIfPossible
  );
  const twoPoolSwapContract = useContract(
    chainId ? TWO_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    SWAP_ABI,
    withSignerIfPossible
  );
  const dollyMintSwapContract = useContract(
    chainId ? DOLLY_MINT_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    DOLLY_SWAP_ABI,
    withSignerIfPossible
  );

  const dollySwapContract = useContract(
    chainId ? DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    SWAP_ABI,
    withSignerIfPossible
  );

  const btcSwapContract = useContract(
    chainId ? BTC_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId] : undefined,
    SWAP_ABI,
    withSignerIfPossible
  );

  return useMemo(() => {
    switch (poolName) {
      case STABLE_COIN_POOL_NAME:
        return customSwapContract;

      case UST_POOL_NAME:
        return ustPoolSwapContract;

      case TWO_POOL_NAME:
        return twoPoolSwapContract;

      case DOLLY_MINT_POOL_NAME:
        return dollyMintSwapContract;

      case DOLLY_POOL_NAME:
        return dollySwapContract;

      case BTC_POOL_NAME:
        return btcSwapContract;

      default:
        return null;
    }
  }, [
    // stablecoinSwapContract,
    customSwapContract,
    ustPoolSwapContract,
    twoPoolSwapContract,
    dollySwapContract,
    dollyMintSwapContract,
    btcSwapContract,
    poolName,
  ]);
}

export function useLPTokenContract(poolName) {
  const swapContract = useSwapContract(poolName);
  const [lpTokenAddress, setLPTokenAddress] = useState("");
  swapContract === null || swapContract === void 0
    ? void 0
    : swapContract
        .swapStorage()
        .then(({ lpToken }) => setLPTokenAddress(lpToken));
  return useContract(lpTokenAddress, LPTOKEN_ABI);
}
export function useFactoryContract(t) {
  const { chainId } = useActiveWeb3React();
  const factoryContract = useContract(
    chainId ? t.factoryAddresses[chainId] : undefined,
    FACTORY_ABI
  );

  return factoryContract;
}
export function usePairContract(t) {
  const pairContract = useContract(t, PAIR_ABI);
  return pairContract;
}

export function useRouterContract(t) {
  const { chainId } = useActiveWeb3React();
  const routerContract = useContract(
    chainId ? t.routerAddresses[chainId] : undefined,
    ROUTER_ABI
  );

  return routerContract;
}
export function useAllContracts() {
  const daiContract = useTokenContract(DAI);
  const usdcContract = useTokenContract(USDC);
  const usdtContract = useTokenContract(USDT);
  const busdContract = useTokenContract(BUSD);
  const bnbContract = useTokenContract(BNB);
  const ustContract = useTokenContract(UST);
  const dollyContract = useTokenContract(DOLLY);
  const btcbContract = useTokenContract(BTCB);
  const renbtcContract = useTokenContract(renBTC);

  // const stablecoinSwapTokenContract = useTokenContract(STABLECOIN_SWAP_TOKEN);
  const stableSwapTokenContract = useTokenContract(STABLE_SWAP_TOKEN);
  const ustPoolTokenContract = useTokenContract(UST_POOL_SWAP_TOKEN);
  const twoPoolTokenContract = useTokenContract(TWO_POOL_SWAP_TOKEN);
  const dollyPoolSwapTokenContract = useTokenContract(DOLLY_POOL_SWAP_TOKEN);
  const dollyMintSwapTokenContract = useTokenContract(
    DOLLY_MINT_POOL_SWAP_TOKEN
  );
  const btcPoolSwapTokenContract = useTokenContract(BTC_POOL_SWAP_TOKEN);

  return useMemo(() => {
    if (
      ![
        daiContract,
        usdcContract,
        usdtContract,
        busdContract,
        bnbContract,
        ustContract,
        dollyContract,
        btcbContract,
        renbtcContract,
        // btcSwapTokenContract,// TODO: add back when contract deployed
        // stablecoinSwapTokenContract,
        stableSwapTokenContract,
        ustPoolTokenContract,
        twoPoolTokenContract,
        dollyPoolSwapTokenContract,
        btcPoolSwapTokenContract,
      ].some(Boolean)
    )
      return null;
    return {
      [DAI.symbol]: daiContract,
      [USDC.symbol]: usdcContract,
      [USDT.symbol]: usdtContract,
      [BUSD.symbol]: busdContract,
      [BNB.symbol]: bnbContract,
      [UST.symbol]: ustContract,
      [DOLLY.symbol]: dollyContract,
      [BTCB.symbol]: btcbContract,
      [renBTC.symbol]: renbtcContract,
      // [STABLECOIN_SWAP_TOKEN.symbol]: stablecoinSwapTokenContract,
      [STABLE_SWAP_TOKEN.symbol]: stableSwapTokenContract,
      [UST_POOL_SWAP_TOKEN.symbol]: ustPoolTokenContract,
      [TWO_POOL_SWAP_TOKEN.symbol]: twoPoolTokenContract,
      [DOLLY_MINT_POOL_SWAP_TOKEN.symbol]: dollyMintSwapTokenContract,
      [DOLLY_POOL_SWAP_TOKEN.symbol]: dollyPoolSwapTokenContract,
      [BTC_POOL_SWAP_TOKEN.symbol]: btcPoolSwapTokenContract,
    };
  }, [
    daiContract,
    usdcContract,
    usdtContract,
    bnbContract,
    ustContract,
    dollyContract,
    btcbContract,
    renbtcContract,
    // stablecoinSwapTokenContract,
    stableSwapTokenContract,
    ustPoolTokenContract,
    twoPoolTokenContract,
    dollyMintSwapTokenContract,
    dollyPoolSwapTokenContract,
    btcPoolSwapTokenContract,
  ]);
}
