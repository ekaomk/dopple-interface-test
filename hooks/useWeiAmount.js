import { toWei } from "../utils/math-helpers";

export function useWeiAmount(amount, unit = "ether") {
  return toWei(amount, unit);
}
