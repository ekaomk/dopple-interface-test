import { useEffect, useRef } from "react";
// Source: https://github.com/austintgriffith/eth-hooks/blob/master/src/Poller.ts
const usePoller = (fn, delay) => {
    const savedCallback = useRef();
    // Remember the latest fn.
    useEffect(() => {
        savedCallback.current = fn;
    }, [fn]);
    // Set up the interval.
    useEffect(() => {
        // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
        function tick() {
            if (savedCallback.current)
                savedCallback.current();
        }
        if (delay !== null) {
            const id = setInterval(tick, delay);
            // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
            return () => clearInterval(id);
        }
    }, [delay]);
    // run at start too
    useEffect(() => {
        fn();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
};
export default usePoller;