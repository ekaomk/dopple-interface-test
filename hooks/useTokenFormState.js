import { numberInputStateCreator, } from "../utils/numberInputState";
import { BigNumber } from "@ethersproject/bignumber";
import React from "react";
export function useTokenFormState(tokens) {
    // Token input state handlers
    const tokenInputStateCreators = tokens.reduce((acc, token) => (Object.assign(Object.assign({}, acc), { [token.symbol]: numberInputStateCreator(token.decimals, BigNumber.from("0")) })), {});
    // Token input values, both "raw" and formatted "safe" BigNumbers
    const [tokenFormState, setTokenFormState] = React.useState(tokens.reduce((acc, token) => (Object.assign(Object.assign({}, acc), { [token.symbol]: tokenInputStateCreators[token.symbol]("") })), {}));
    // function updateTokenFormValue(tokenSymbol: string, value: string): void {
    //   setTokenFormState((prevState) => ({
    //     ...prevState,
    //     [tokenSymbol]: tokenInputStateCreators[tokenSymbol](value),
    //   }))
    // }
    function updateTokenFormState(newState) {
        const convertedNewState = Object.keys(newState).reduce((acc, symbol) => (Object.assign(Object.assign({}, acc), { [symbol]: tokenInputStateCreators[symbol](newState[symbol]) })), {});
        setTokenFormState((prevState) => (Object.assign(Object.assign({}, prevState), convertedNewState)));
    }
    return [tokenFormState, updateTokenFormState];
}