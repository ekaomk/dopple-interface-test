import { useEffect, useState } from "react";
import { formatBNToString } from "../utils";
import { useFactoryContract, useRouterContract } from "./useContract";
import { useActiveWeb3React } from "../hooks";
import { getContract } from "../utils/index";
import PAIR_ABI from "../constants/abis/IUniswapV2Pair.json";
import parseStringToBigNumber from "../utils/parseStringToBigNumber";

export function useSwapExchange(
  swapExchange,
  tokenForm,
  tokenTo,
  fromInputValue,
  toInputValue
) {
  const { account, chainId, library, error: web3Error } = useActiveWeb3React();
  const [state, setState] = useState({
    pairPrice: 0,
  });
  let pairReserves;
  let pairContractFromContract;
  let exchangeRate;
  let pairDecimals;
  const factoryContract = useFactoryContract(swapExchange);
  const routerContract = useRouterContract(swapExchange);
  const token0 = tokenForm.addresses[chainId];
  const token1 = tokenTo.addresses[chainId];

  useEffect(() => {
    (async () => {
      if (pairContractFromContract) {
        pairDecimals = await pairContractFromContract.decimals();
      }

      if (factoryContract) {
        const pairAddressFromContract = await factoryContract.getPair(
          token0,
          token1
        );
        pairContractFromContract = getContract(
          pairAddressFromContract,
          PAIR_ABI,
          library,
          false
        );
      }
      if (pairContractFromContract) {
        pairReserves = await pairContractFromContract.getReserves();
      }
      if (pairReserves) {
        const token0FromContract = await pairContractFromContract.token0();

        if (parseFloat(fromInputValue) > 0) {
          if (token0FromContract.toLowerCase() === token0.toLowerCase()) {
            exchangeRate = await routerContract.quote(
              parseStringToBigNumber(fromInputValue, pairDecimals).value,
              pairReserves.reserve0,
              pairReserves.reserve1
            );
          } else {
            exchangeRate = await routerContract.quote(
              parseStringToBigNumber(fromInputValue, pairDecimals).value,
              pairReserves.reserve1,
              pairReserves.reserve0
            );
          }

          const fee = parseStringToBigNumber(
            String(swapExchange.fee),
            pairDecimals
          ).value;

          const oneHundred = parseStringToBigNumber("100", pairDecimals).value;
          const result = exchangeRate.sub(
            exchangeRate.mul(fee).div(oneHundred)
          );

          setState({
            ...state,
            pairPrice: formatBNToString(result, pairDecimals, 4),
          });
        }
      }
    })();
  }, [tokenForm.symbol, fromInputValue, state.pairPrice]);

  return state;
}
