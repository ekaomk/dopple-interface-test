import { useCallback, useEffect, useState } from "react";
import { updateApiData } from "../state/user";
import { useDispatch } from "react-redux";

export function useGetApiData() {
  const [dopData, setDopData] = useState("");

  const dispatch = useDispatch();

  const update = useCallback(async () => {
    const response = await fetch("https://dopple-api.homepage.workers.dev/");

    const result = await response.json();
    dispatch(updateApiData(result));
    setDopData(result);
  }, [setDopData]);

  useEffect(() => {
    update();
  }, []);

  // return with float
  return dopData;
}
