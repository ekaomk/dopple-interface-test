import { useEffect, useState } from "react";
import { ACTIVE_POOL_NAME } from "../constants";
import { useSwapContract } from "../hooks/useContract";

export function useCalculateReceiveDopLp() {
  const poolName = ACTIVE_POOL_NAME;
  const swapContract = useSwapContract(poolName);

  const [lpTokenReceive, setLpTokenReceive] = useState(0);

  useEffect(() => {
    (async () => {
      try {
        setLpTokenReceive("0");
      } catch {
        // For handle value is more uint 256
        return;
      }
    })();
  }, [lpTokenReceive]);

  return lpTokenReceive;
}
