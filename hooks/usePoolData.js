import { POOLS_MAP, TRANSACTION_TYPES } from "../constants";
import { formatBNToPercentString, getContract } from "../utils/index";
import { useEffect, useState } from "react";
import { AddressZero } from "@ethersproject/constants";
import { BigNumber } from "@ethersproject/bignumber";
import LPTOKEN_ABI from "../constants/abis/lpToken.abi.json";
import { Zero } from "@ethersproject/constants";
import { parseUnits } from "@ethersproject/units";
import { useActiveWeb3React } from ".";
import { useSelector } from "react-redux";
import { useSwapContract } from "./useContract";
export default function usePoolData(poolName) {
  const { account, library } = useActiveWeb3React();
  const swapContract = useSwapContract(poolName);
  const [poolData, setPoolData] = useState([null, null]);
  const { lastTransactionTimes } = useSelector((state) => state.application);
  const lastDepositTime = lastTransactionTimes[TRANSACTION_TYPES.DEPOSIT];
  const lastWithdrawTime = lastTransactionTimes[TRANSACTION_TYPES.WITHDRAW];
  const lastSwapTime = lastTransactionTimes[TRANSACTION_TYPES.SWAP];
  useEffect(() => {
    async function getSwapData() {
      if (
        poolName == null ||
        swapContract == null ||
        library == null ||
        account == null
      )
        return;
      const POOL = POOLS_MAP[poolName];

      // Swap fees, price, and LP Token data
      const [userCurrentWithdrawFee, swapStorage] = await Promise.all([
        swapContract.calculateCurrentWithdrawFee(account || AddressZero),
        swapContract.swapStorage(),
      ]);
      const { adminFee, lpToken: lpTokenAddress, swapFee } = swapStorage;
      const lpTokenContract = getContract(
        lpTokenAddress,
        LPTOKEN_ABI,
        library,
        account !== null && account !== void 0 ? account : undefined
      );
      const [userLpTokenBalance, totalLpTokenBalance] = await Promise.all([
        lpTokenContract.balanceOf(account || AddressZero),
        lpTokenContract.totalSupply(),
      ]);
      const virtualPrice =
        totalLpTokenBalance == 0
          ? BigNumber.from(10).pow(18)
          : await swapContract.getVirtualPrice();
      // Pool token data
      const tokenBalances = await Promise.all(
        POOL.poolTokens.map(async (token, i) => {
          const balance = await swapContract.getTokenBalance(i);
          return BigNumber.from(10)
            .pow(18 - token.decimals) // cast all to 18 decimals
            .mul(balance);
        })
      );
      const tokenBalancesSum = tokenBalances.reduce((sum, b) => sum.add(b));

      // (weeksPerYear * KEEPPerWeek * KEEPPrice) / (BTCPrice * BTCInPool)
      const comparisonPoolToken = POOL.poolTokens[0];
      const userShare = userLpTokenBalance
        .mul(BigNumber.from(10).pow(18))
        .div(
          totalLpTokenBalance.isZero()
            ? BigNumber.from("1")
            : totalLpTokenBalance
        );
      const userPoolTokenBalances = tokenBalances.map((balance) => {
        return userShare.mul(balance).div(BigNumber.from(10).pow(18));
      });
      const userPoolTokenBalancesSum = userPoolTokenBalances.reduce((sum, b) =>
        sum.add(b)
      );
      const poolTokens = POOL.poolTokens.map((token, i) => ({
        symbol: token.symbol,
        percent: formatBNToPercentString(
          tokenBalances[i]
            .mul(10 ** 5)
            .div(
              totalLpTokenBalance.isZero()
                ? BigNumber.from("1")
                : tokenBalancesSum
            ),
          5
        ),
        value: tokenBalances[i],
      }));
      const userPoolTokens = POOL.poolTokens.map((token, i) => ({
        symbol: token.symbol,
        percent: formatBNToPercentString(
          tokenBalances[i]
            .mul(10 ** 5)
            .div(
              totalLpTokenBalance.isZero()
                ? BigNumber.from("1")
                : tokenBalancesSum
            ),
          5
        ),
        value: userPoolTokenBalances[i],
      }));
      const poolData = {
        name: poolName,
        tokens: poolTokens,
        totalLocked: totalLpTokenBalance,
        virtualPrice: virtualPrice,
        adminFee: adminFee,
        swapFee: swapFee,
        volume: "XXX",
        utilization: "XXX",
        apy: "XXX",
      };
      const userShareData = account
        ? {
            name: poolName,
            share: userShare,
            value: userPoolTokenBalancesSum,
            avgBalance: userPoolTokenBalancesSum,
            tokens: userPoolTokens,
            currentWithdrawFee: userCurrentWithdrawFee,
            lpTokenBalance: userLpTokenBalance,
            // lpTokenMinted: userLpTokenMinted,
          }
        : null;
      setPoolData([poolData, userShareData]);
    }
    getSwapData();
  }, [
    lastDepositTime,
    lastWithdrawTime,
    lastSwapTime,
    poolName,
    swapContract,
    // tokenPricesUSD,
    account,
    library,
  ]);
  return poolData;
}
