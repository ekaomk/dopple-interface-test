import { useCallback, useEffect, useState } from "react";
import { useActiveWeb3React } from "../hooks";
import mkrMulticall from "../utils/mkrMulticall";
import ERC20_ABI from "../constants/abis/ERC20.abi.json";
import DIVIDEND_ABI from "../constants/abis/dividend.json";
import DIAMONDHAND_LIST from "../constants/daimondhand";

export const useStakingAllowance = () => {
  const { account, chainId } = useActiveWeb3React();
  const [allowanceResult, setAllowanceResult] = useState([]);

  const update = useCallback(async () => {
    if (account && chainId) {
      const calls = DIAMONDHAND_LIST.map((dividend) => {
        return {
          address: dividend.deposit_token_address,
          name: "allowance",
          params: [account, dividend.address],
        };
      });
      // console.log("useStakingAllowance", calls);
      mkrMulticall(chainId, ERC20_ABI, calls).then((result) => {
        const resultMapping = DIAMONDHAND_LIST.map((dividend, i) => {
          return {
            stakingData: dividend,
            allowance: result[i][0],
          };
        });
        setAllowanceResult(resultMapping);
      });
    }
  }, [account, chainId]);

  useEffect(() => {
    update();
  }, [update]);

  return { allowanceResult, update };
};

export const useStakingBalance = () => {
  const { account, chainId } = useActiveWeb3React();
  const [balanceResult, setBalanceResult] = useState([]);

  const update = useCallback(async () => {
    if (account && chainId) {
      const calls = DIAMONDHAND_LIST.map((dividend) => {
        return {
          address: dividend.deposit_token_address,
          name: "balanceOf",
          params: [account],
        };
      });
      // console.log("useStakingBalance", calls);
      mkrMulticall(chainId, ERC20_ABI, calls).then((result) => {
        const resultMapping = DIAMONDHAND_LIST.map((dividend, i) => {
          return {
            stakingData: dividend,
            balance: result[i][0],
          };
        });
        setBalanceResult(resultMapping);
      });
    }
  }, [account, chainId]);

  useEffect(() => {
    update();
  }, [update]);

  return { balanceResult, update };
};

export const useStakingDepositBalance = () => {
  const { account, chainId } = useActiveWeb3React();
  const [depositBalanceResult, setDepositBalanceResult] = useState([]);

  const update = useCallback(async () => {
    if (account && chainId) {
      const calls = DIAMONDHAND_LIST.map((dividend) => {
        return {
          address: dividend.address,
          name: "userInfo",
          params: [account],
        };
      });
      // console.log("useStakingDepositBalance", calls);
      mkrMulticall(chainId, DIVIDEND_ABI, calls).then((result) => {
        const resultMapping = DIAMONDHAND_LIST.map((dividend, i) => {
          return {
            stakingData: dividend,
            amount: result[i].amount,
          };
        });
        setDepositBalanceResult(resultMapping);
      });
    }
  }, [account, chainId]);

  useEffect(() => {
    update();
  }, [update]);

  return { depositBalanceResult, update };
};

export const useStakingReward = () => {
  const { account, chainId } = useActiveWeb3React();
  const [stakingRewardResult, setStakingRewardResult] = useState([]);

  const update = useCallback(async () => {
    if (account && chainId) {
      const calls = DIAMONDHAND_LIST.map((dividend) => {
        return {
          address: dividend.address,
          name: "pendingReward",
          params: [account],
        };
      });
      // console.log("useStakingReward", calls);
      mkrMulticall(chainId, DIVIDEND_ABI, calls).then((result) => {
        const resultMapping = DIAMONDHAND_LIST.map((dividend, i) => {
          return {
            stakingData: dividend,
            reward: result[i][0],
          };
        });
        setStakingRewardResult(resultMapping);
      });
    }
  }, [account, chainId]);

  useEffect(() => {
    update();
  }, [update]);

  return { stakingRewardResult, update };
};
