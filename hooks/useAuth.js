import { UnsupportedChainIdError, useWeb3React } from "@web3-react/core";
import { CONNECTOR_STORAGE_KEY } from "../constants";

const useAuth = () => {
  const { activate, deactivate } = useWeb3React();

  const login = (connector, name) => {
    activate(connector, undefined, true).catch((error) => {
      if (error instanceof UnsupportedChainIdError) {
        activate(connector); // a little janky...can't use setError because the connector isn't set
      } else {
        // TODO: handle error
      }
    });
    window.localStorage.setItem(CONNECTOR_STORAGE_KEY, name);
  };
  const logout = () => {
    deactivate();
    window.localStorage.removeItem(CONNECTOR_STORAGE_KEY);
  };
  return { login, logout };
};

export default useAuth;
