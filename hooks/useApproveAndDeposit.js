import { POOLS_MAP, TRANSACTION_TYPES } from "../constants";
import { useAllContracts, useSwapContract } from "./useContract";
import { BigNumber } from "@ethersproject/bignumber";
import { GasPrices } from "../state/user";
import checkAndApproveTokenForTrade from "../utils/checkAndApproveTokenForTrade";
import { formatDeadlineToNumber } from "../utils";
import { getFormattedTimeString } from "../utils/dateTime";
import { parseUnits } from "@ethersproject/units";
import { subtractSlippage } from "../utils/slippage";
import { updateLastTransactionTimes } from "../state/application";
import { useActiveWeb3React } from ".";
import { useDispatch, useSelector } from "react-redux";
import { useToast } from "./useToast";
import { MaxUint256 } from "@ethersproject/constants";

export function useApproveAndDeposit(poolName) {
  const dispatch = useDispatch();
  const swapContract = useSwapContract(poolName);
  const tokenContracts = useAllContracts();
  const { account } = useActiveWeb3React();
  const { addToast, clearToasts } = useToast();
  const { gasStandard, gasFast, gasInstant } = useSelector(
    (state) => state.application
  );
  const {
    slippageCustom,
    slippageSelected,
    gasPriceSelected,
    gasCustom,
    transactionDeadlineCustom,
    transactionDeadlineSelected,
    infiniteApproval,
  } = useSelector((state) => state.user);
  const POOL = POOLS_MAP[poolName];
  return async function approveAndDeposit(state) {
    if (!account) throw new Error("Wallet must be connected");
    if (!swapContract) throw new Error("Swap contract is not loaded");
    const approveSingleToken = async (token) => {
      const spendingValue = BigNumber.from(state[token.symbol].valueSafe);
      if (spendingValue.isZero()) return;
      const tokenContract =
        tokenContracts === null || tokenContracts === void 0
          ? void 0
          : tokenContracts[token.symbol];
      if (tokenContract == null) return;

      await checkAndApproveTokenForTrade(
        tokenContract,
        swapContract.address,
        account,
        spendingValue,
        true,
        {
          onTransactionStart: () => {
            return addToast(
              {
                type: "pending",
                title: `${getFormattedTimeString()} Approving spend for ${
                  token.name
                }`,
              },
              {
                autoDismiss: false, // TODO: be careful of orphan toasts on error
              }
            );
          },
          onTransactionSuccess: () => {
            return addToast({
              type: "success",
              title: `${getFormattedTimeString()} Successfully approved spend for ${
                token.name
              }`,
            });
          },
          onTransactionError: () => {
            throw new Error("Your transaction could not be completed");
          },
        }
      );
    };
    try {
      // For each token being deposited, check the allowance and approve it if necessary
      await Promise.all(
        POOL.poolTokens.map((token) => approveSingleToken(token))
      );
      // "isFirstTransaction" check can be removed after launch
      const poolTokenBalances = await Promise.all(
        POOL.poolTokens.map(async (token, i) => {
          return await swapContract.getTokenBalance(i);
        })
      );
      const isFirstTransaction = poolTokenBalances.every((bal) => bal.isZero());
      let minToMint;
      if (isFirstTransaction) {
        minToMint = BigNumber.from("0");
      } else {
        minToMint = await swapContract.calculateTokenAmount(
          account,
          POOL.poolTokens.map(({ symbol }) => state[symbol].valueSafe),
          true
        );
      }
      minToMint = subtractSlippage(minToMint, slippageSelected, slippageCustom);
      const clearMessage = addToast({
        type: "pending",
        title: `${getFormattedTimeString()} Starting your deposit...`,
      });
      let gasPrice;
      if (gasPriceSelected === GasPrices.Custom) {
        gasPrice =
          gasCustom === null || gasCustom === void 0
            ? void 0
            : gasCustom.valueSafe;
      } else if (gasPriceSelected === GasPrices.Fast) {
        gasPrice = gasFast;
      } else if (gasPriceSelected === GasPrices.Instant) {
        gasPrice = gasInstant;
      } else {
        gasPrice = gasStandard;
      }
      gasPrice = parseUnits(String(gasPrice) || "45", 9);
      const deadline = formatDeadlineToNumber(
        transactionDeadlineSelected,
        transactionDeadlineCustom
      );
      // console.log(
      //   POOL.poolTokens.map(({ symbol }) => state[symbol].valueSafe),
      //   minToMint,
      //   Math.round(new Date().getTime() / 1000 + 60 * deadline),
      //   {
      //     gasPrice,
      //   }
      // );
      const spendTransaction = await swapContract.addLiquidity(
        POOL.poolTokens.map(({ symbol }) => state[symbol].valueSafe),
        minToMint,
        Math.round(new Date().getTime() / 1000 + 60 * deadline),
        {
          gasPrice,
        }
      );
      await spendTransaction.wait();
      dispatch(
        updateLastTransactionTimes({
          [TRANSACTION_TYPES.DEPOSIT]: Date.now(),
        })
      );
      clearMessage();
      addToast({
        type: "success",
        title: `${getFormattedTimeString()} Liquidity added`,
      });
      return Promise.resolve();
    } catch (e) {
      console.error(e);
      clearToasts();
      addToast({
        type: "error",
        title: `${getFormattedTimeString()} Unable to complete your transaction`,
      });
    }
  };
}

export function useApproveForDeposit(poolName, token) {
  const dispatch = useDispatch();
  const swapContract = useSwapContract(poolName);
  const tokenContracts = useAllContracts();
  const { account } = useActiveWeb3React();
  const { addToast, clearToasts } = useToast();

  const spendingValue = MaxUint256;

  return async function approveForDeposit() {
    if (!account) throw new Error("Wallet must be connected");
    if (!swapContract) throw new Error("Swap contract is not loaded");
    const approveSingleToken = async (token) => {
      const tokenContract =
        tokenContracts === null || tokenContracts === void 0
          ? void 0
          : tokenContracts[token.symbol];

      if (tokenContract == null) return;
      await checkAndApproveTokenForTrade(
        tokenContract,
        swapContract.address,
        account,
        spendingValue,
        true,
        {
          onTransactionStart: () => {
            return addToast(
              {
                type: "pending",
                title: `${getFormattedTimeString()} Approving spend for ${
                  token.name
                }`,
              },
              {
                autoDismiss: false, // TODO: be careful of orphan toasts on error
              }
            );
          },
          onTransactionSuccess: () => {
            return addToast({
              type: "success",
              title: `${getFormattedTimeString()} Successfully approved spend for ${
                token.name
              }`,
            });
          },
          onTransactionError: () => {
            throw new Error("Your transaction could not be completed");
          },
        }
      );
    };
    try {
      // For each token being deposited, check the allowance and approve it if necessary
      await approveSingleToken(token);
      // "isFirstTransaction" check can be removed after launch
      return Promise.resolve();
    } catch (e) {
      console.error(e);
      clearToasts();
      addToast({
        type: "error",
        title: `${getFormattedTimeString()} Unable to approve`,
      });
    }
  };
}
