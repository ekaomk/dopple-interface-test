import { ToastsContext, } from "../providers/ToastsProvider";
import { useContext } from "react";
export const useToast = () => {
    return useContext(ToastsContext);
};