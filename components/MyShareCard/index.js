import React, { ReactElement, useEffect, useState } from "react";
import { formatBNToPercentString, formatBNToString } from "../../utils";

import {
  TOKENS_MAP,
  DOPPLE_LP_TOKEN,
  POOLS_LIST,
  STAKING_DOPPLE_LP_TOKENS_LIST,
} from "../../constants";
import { UserShareType } from "../../hooks/usePoolData";
import { commify } from "@ethersproject/units";
import { useTranslation } from "react-i18next";
import Image from "next/image";
import Skeleton from "react-loading-skeleton";
import { useTokenContract } from "../../hooks/useContract";
import { useActiveWeb3React } from "../../hooks";
import { BigNumber } from "@ethersproject/bignumber";

function MyShareCard({ data, transactionData, depositPage, pool_name }) {
  let formattedData;
  let totalReceiveDopLp = 0;
  const { account, chainId, library } = useActiveWeb3React();
  const [totalDopLp, setTotalDopLp] = useState(BigNumber.from("0"));

  if (!depositPage) {
    const stakingContract = useTokenContract(
      STAKING_DOPPLE_LP_TOKENS_LIST[pool_name]
    );

    useEffect(() => {
      (async () => {
        if (stakingContract && account) {
          const dopLpBalance = await stakingContract.balanceOf(account);
          setTotalDopLp(dopLpBalance);
        }
      })();
    }, []);
  }

  try {
    formattedData = {
      share: formatBNToPercentString(data.share, 18),
      value: commify(formatBNToString(data.value, 18, 4)),
      tokens: data.tokens.map((coin) => {
        const token = TOKENS_MAP[coin.symbol];
        return {
          symbol: token.symbol,
          name: token.name,
          value: commify(formatBNToString(coin.value, 18, 4)),
        };
      }),
    };

    if (depositPage) {
      totalReceiveDopLp = commify(
        formatBNToString(
          transactionData.to.item.amount,
          transactionData.to.item.token.decimals
        )
      );
    }
  } catch (error) {
    formattedData = {
      share: "",
      value: "",
      tokens: [{ symbol: "" }, { symbol: "" }, { symbol: "" }, { symbol: "" }],
    };
  }

  return (
    <div className="myShareCard ">
      {depositPage && (
        <div className="absolute -ml-6 mt-14 hidden sm:block">
          <Image
            src="/images/icons/deposit-arrow-next.svg"
            width="50"
            height="50"
          />
        </div>
      )}
      <div className="dark:text-white dark:border-b-dark-500 flex items-center border-b p-6 pb-2 lg:px-12 ">
        <div className="pb-2 mr-4">
          <Image
            src="/images/icons/bank-account.svg"
            width="100"
            height="100"
          />
        </div>
        <div>
          {depositPage && <div className="text-sm mb-1">You will receive</div>}
          <div className="dark:text-white text-xl font-bold mb-1">
            Total {POOLS_LIST.find((x) => x.name == data?.name)?.title}{" "}
          </div>
          <div className="text-2xl text-blue-400 font-bold">
            {depositPage
              ? totalReceiveDopLp
              : formatBNToString(totalDopLp, DOPPLE_LP_TOKEN.decimals, 2)}
          </div>
        </div>
      </div>
      <div className="dark:bg-dark-700 dark:text-white dark:border-b-dark-500 items-center border-b p-6 text-sm">
        <div className="grid grid-cols-2 mb-3 ">
          <span className="text-steel-300 dark:text-white">Total : &nbsp;</span>
          <span className="font-bold text-right">
            {formattedData.share == "" ? (
              <Skeleton count={1} />
            ) : (
              formattedData.share + " of pool"
            )}
          </span>
        </div>
        <div className="grid grid-cols-2 ">
          <span className="text-steel-300 dark:text-white">
            Total amount : &nbsp;{" "}
          </span>
          <span className="font-bold text-right">
            {formattedData.share == "" ? (
              <Skeleton count={1} />
            ) : (
              formattedData.value
            )}
          </span>
        </div>
      </div>
      <div className="dark:text-white items-center p-6 pb-3 text-sm">
        {formattedData.tokens.map((coin, index) => (
          <div key={index} className="grid grid-cols-2 mb-4">
            {coin.symbol ? (
              <div className="flex">
                <Image
                  src={`/images/icons/` + coin.symbol.toLowerCase() + ".svg"}
                  width="20"
                  height="20"
                />
                <div className="text-steel-300 ml-2 dark:text-white">
                  {coin.symbol} : &nbsp;
                </div>
              </div>
            ) : (
              <div className="grid grid-cols-2">
                <Skeleton count={1} />
              </div>
            )}

            <span className="font-bold text-right">
              {coin.value ? coin.value : <Skeleton count={1} />}
            </span>
          </div>
        ))}
      </div>
    </div>
  );
}

export default MyShareCard;
