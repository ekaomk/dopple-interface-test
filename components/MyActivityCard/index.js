import React, { ReactElement } from "react";

import { HistoricalPoolDataType } from "../../hooks/useHistoricalPoolData";
import { commify } from "@ethersproject/units";
import { formatBNToString } from "../../utils";
import { useTranslation } from "react-i18next";

function MyActivityCard({ historicalPoolData }) {
  if (!historicalPoolData) return null;

  const historicalFormattedData = historicalPoolData
    ? {
        totalDepositsBTC: commify(
          formatBNToString(historicalPoolData.totalDepositsBTC, 36, 6)
        ),
        totalWithdrawalsBTC: commify(
          formatBNToString(historicalPoolData.totalWithdrawalsBTC, 36, 6)
        ),
        totalProfitBTC: commify(
          formatBNToString(historicalPoolData.totalProfitBTC, 36, 6)
        ),
        totalDepositsUSD: commify(
          formatBNToString(historicalPoolData.totalDepositsUSD, 36)
        ),
        totalWithdrawalsUSD: commify(
          formatBNToString(historicalPoolData.totalWithdrawalsUSD, 36)
        ),
        totalProfitUSD: commify(
          formatBNToString(historicalPoolData.totalProfitUSD, 36)
        ),
      }
    : null;

  return (
    <div className="myActivityCard">
      <h4>myActivity</h4>
      {historicalFormattedData ? (
        <div className="activityTable">
          <div key="deposits-btc">
            <span className="label">BTC deposit</span>
            <span>{historicalFormattedData.totalDepositsBTC}</span>
          </div>
          <div key="deposits-usd">
            <span className="label">USD deposit</span>
            <span>{`$${historicalFormattedData.totalDepositsUSD}`}</span>
          </div>
          <div key="withdrawals-btc">
            <span className="label">BTC withdrawal</span>
            <span>{historicalFormattedData.totalWithdrawalsBTC}</span>
          </div>
          <div key="withdrawals-usd">
            <span className="label">USD withdrawal</span>
            <span>{`$${historicalFormattedData.totalWithdrawalsUSD}`}</span>
          </div>
          <div key="profit-btc">
            <span className="label">BTC profit</span>
            <span>{historicalFormattedData.totalProfitBTC}</span>
          </div>
          <div key="profit-usd">
            <span className="label">USD profit</span>
            <span>{`$${historicalFormattedData.totalProfitUSD}`}</span>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default MyActivityCard;
