import React, { ReactElement } from "react"

import classNames from "classnames"

export default function Button(
    props
) {
    const { kind = "primary", size = "large", ...buttonProps } = props
    return (
        <button className={classNames("button", kind, size)} {...buttonProps} />
    )
}
