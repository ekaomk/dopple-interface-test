import Image from "next/image";
import WhitePanel from "../../components/layouts/WhitePanel";

import React, { useState, useEffect, useCallback } from "react";
import { formatBNToString } from "../../utils/index";

import { useActiveWeb3React } from "../../hooks";
import {
  useFairLaunchContract,
  useTokenContract,
  useDiamondHandTokenContract,
} from "../../hooks/useContract";
import { DOPPLE, DOP_DIAMONDHAND, TWIN_DIAMONDHAND } from "../../constants";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";

import { useStakingReward } from "../../hooks/diamounhand";
import { useDispatch, useSelector } from "react-redux";

export default function Reward({
  sumOfUnClaimReward,
  flagFromStake,
  flagUpdateWalletBalance,
  updateStakingBalance,
}) {
  const { account, chainId, error: web3Error } = useActiveWeb3React();
  const fairLaunchContract = useFairLaunchContract();
  const doppleTokenContract = useTokenContract(DOPPLE);
  const dopDiamondHandContract = useDiamondHandTokenContract(DOP_DIAMONDHAND);
  const twinDiamondHandContract = useDiamondHandTokenContract(TWIN_DIAMONDHAND);

  const [totalDoppleBalanceOf, setTotalDoppleBalanceOf] = useState("0");
  const [totalUnClaimReward, setTotalUnClaimReward] = useState("0");
  const [totalLockedDoppleBalanceOf, setTotalLockedDoppleBalanceOf] =
    useState("0");
  const [loadingClaimUnlockReward, setLoadingClaimUnlockReward] =
    useState(false);
  const [totalTvl, setTotalTvl] = useState(0);

  const { stakingRewardResult, update: updateStakingReward } =
    useStakingReward();

  const dispatch = useDispatch();
  const { apiData } = useSelector((state) => state.user);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  let fairLaunchDecimals;

  async function getTVL() {
    setTotalTvl(apiData?.doppleData?.totalTvl);
  }

  const updatePendingDopple = async (account) => {
    // updateDopStats(dataApi.dopple_data);

    if (!fairLaunchContract || !account) return;

    // dop pair
    const pendingDopBnb = await fairLaunchContract.pendingDopple(16, account);
    const pendingDopDolly = await fairLaunchContract.pendingDopple(15, account);
    const pendingDopBusdLp = await fairLaunchContract.pendingDopple(
      17,
      account
    );
    const pendingDopLp = await fairLaunchContract.pendingDopple(2, account);
    const pendingDopDaiLp = await fairLaunchContract.pendingDopple(18, account);
    const pendingDopUsdtLp = await fairLaunchContract.pendingDopple(
      19,
      account
    );

    // stable coin
    const pendingDollyLP = await fairLaunchContract.pendingDopple(12, account);
    const pendingUstLP = await fairLaunchContract.pendingDopple(4, account);
    const pending2PoolLp = await fairLaunchContract.pendingDopple(5, account);
    const pendingStableCoinDopple = await fairLaunchContract.pendingDopple(
      1,
      account
    );

    // diamondhand
    const pendingDopDiamondHand = await dopDiamondHandContract.pendingReward(
      account
    );
    const pendingTwinDiamondHand = await twinDiamondHandContract.pendingReward(
      account
    );

    setTotalUnClaimReward(
      formatBNToString(
        pendingDopBnb
          .add(pendingDopDolly)
          .add(pendingDollyLP)
          .add(pendingUstLP)
          .add(pending2PoolLp)
          .add(pendingDopBusdLp)
          .add(pendingDopDaiLp)
          .add(pendingDopUsdtLp)
          .add(pendingDopLp)
          .add(pendingStableCoinDopple)
          .add(pendingDopDiamondHand)
          .add(pendingTwinDiamondHand),
        fairLaunchDecimals,
        2
      )
    );
  };

  const unlockLockedReward = async (account) => {
    if (!doppleTokenContract || !account) return;
    const claimUnlockReward = await doppleTokenContract.unlock();
    setLoadingClaimUnlockReward(true);
    await claimUnlockReward.wait();
    setLoadingClaimUnlockReward(false);
  };

  const updateBalance = async (account) => {
    if (!doppleTokenContract || !account) return;
    const totalDoppleInWallet = await doppleTokenContract.balanceOf(account);
    setTotalDoppleBalanceOf(
      formatBNToString(totalDoppleInWallet, fairLaunchDecimals)
    );
  };

  const updateLockOfBalance = async (account) => {
    if (!doppleTokenContract || !account) return;
    const totalLockedDoppleInWallet = await doppleTokenContract.lockOf(account);
    setTotalLockedDoppleBalanceOf(
      formatBNToString(totalLockedDoppleInWallet, fairLaunchDecimals)
    );
  };

  useEffect(() => {
    getTVL();
  }, []);

  useEffect(() => {
    (async () => {
      await updateBalance(account);
      await updateLockOfBalance(account);
      await updatePendingDopple(account);
    })();
  }, [account, flagUpdateWalletBalance]);

  if (flagFromStake || !account) {
    updatePendingDopple(account);
  }

  return (
    <>
      <div className="grid grid-cols-2 gap-4 py-2">
        <div className="col-span-2 sm:col-span-1 rounded-3xl bg-gradient-primary">
          <div className="reward-panel flex items-center p-8 py-24">
            <div>
              <div className="text-xl font-bold text-white mb-3">
                Dopple earned :
              </div>
              <div className="text-4xl font-bold text-white mb-2 my-4">
                {flagFromStake
                  ? numberWithCommas(sumOfUnClaimReward) ?? "0"
                  : numberWithCommas(totalUnClaimReward)}
              </div>
              <div className="text-sm font-bold text-white mt-3">
                {flagFromStake ? (
                  <>
                    {numberWithCommas(sumOfUnClaimReward)
                      ? `≈ $${numberWithCommas(
                          parseFloat(sumOfUnClaimReward) * dopPrice
                        )}`
                      : "0"}
                  </>
                ) : (
                  <>
                    {numberWithCommas(totalUnClaimReward)
                      ? `≈ $${numberWithCommas(
                          parseFloat(totalUnClaimReward) * dopPrice
                        )}`
                      : "0"}
                  </>
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="col-span-2 sm:col-span-1">
          <div className="dark:text-white dark:bg-dark-700 bg-white rounded-3xl shadow-md p-6 mb-3">
            <div className="sm:text-md text-md flex sm:block items-center pt-2">
              <div className="inline sm:block">Your Dopple wallet balance</div>
            </div>
            <div className=" ">
              <div className="text-2xl mt-4 mb-2 font-bold text-blue-500">
                {numberWithCommas(totalDoppleBalanceOf)}
              </div>
              <div className="text-sm mb-2 text-steel-300">
                {dopPrice ? (
                  <>
                    ≈ $
                    {numberWithCommas(
                      parseFloat(totalDoppleBalanceOf) * dopPrice
                    )}
                  </>
                ) : null}
              </div>
            </div>
          </div>

          <div className="dark:text-white dark:bg-dark-700 bg-white rounded-3xl shadow-md p-6 ">
            <div className="sm:text-md text-md  flex items-center pt-1 ">
              <div className="sm:flex">Total Value Locked </div>
            </div>
            <div className="">
              <div className="text-2xl mt-4 mb-2 font-bold text-blue-500">
                {numberWithCommas(totalTvl)}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const LoadingIcon = () => (
  <svg
    className="animate-spin mr-1 h-4 w-4 text-white"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
  >
    <circle
      className="opacity-25"
      cx="12"
      cy="12"
      r="10"
      stroke="currentColor"
      strokeWidth="4"
    ></circle>
    <path
      className="opacity-75"
      fill="currentColor"
      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
    ></path>
  </svg>
);

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
