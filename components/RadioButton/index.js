
import React, { ReactElement } from "react"


export default function RadioButton({
    checked,
    onChange,
    label,
}) {
    return (
        <div className="radio">
            <label className="radio_wrapper">
                <span className="radio_input">
                    <input type="radio" checked={checked} onChange={onChange} />
                    <span className="radio_control"></span>
                </span>
                <span className="label">{label}</span>
            </label>
        </div>
    )
}