import React, { ReactElement, useState } from "react";
import { formatBNToString, formatDeadlineToNumber } from "../../utils";

import { AppState } from "../../state/index";
import { BigNumber } from "@ethersproject/bignumber";
import Button from "../Button";
import HighPriceImpactConfirmation from "../HighPriceImpactConfirmation";
import { TOKENS_MAP } from "../../constants";
import { formatGasToString } from "../../utils/gas";
import { formatSlippageToString } from "../../utils/slippage";
import { isHighPriceImpact } from "../../utils/priceImpact";
import { useSelector } from "react-redux";
import Image from "next/image";

function ReviewSwap({ onClose, onConfirm, data }) {
  const {
    slippageCustom,
    slippageSelected,
    gasPriceSelected,
    gasCustom,
    transactionDeadlineSelected,
    transactionDeadlineCustom,
  } = useSelector((state) => state.user);
  const { gasStandard, gasFast, gasInstant } = useSelector(
    (state) => state.application
  );
  const [
    hasConfirmedHighPriceImpact,
    setHasConfirmedHighPriceImpact,
  ] = useState(false);
  const fromToken = TOKENS_MAP[data.from.symbol];
  const toToken = TOKENS_MAP[data.to.symbol];
  const isHighPriceImpactTxn = isHighPriceImpact(
    data.exchangeRateInfo.priceImpact
  );
  const deadline = formatDeadlineToNumber(
    transactionDeadlineSelected,
    transactionDeadlineCustom
  );

  return (
    <>
      {/* title */}
      <div className="flex justify-between items-center pb-4">
        <div className="text-xl font-bold">
          Swap{" "}
          <span className="text-blue-400"> {data.exchangeRateInfo.pair}</span>
        </div>
        <div
          className="text-2xl cursor-pointer"
          onClick={(e) => {
            e.stopPropagation();
            onClose();
          }}
        >
          &times;
        </div>
      </div>
      {/* body */}
      <div className="dark:border-b-dark-500 border-b border-blue-300 mb-4 pb-5 pt-2">
        <div className="dark:bg-dark-600 grid grid-cols-2 items-center bg-blue-50 p-4 rounded-lg">
          <div className="flex items-center ">
            <img className="tokenIcon h-8" src={fromToken.icon} alt="icon" />
            <div className="ml-2">{data.from.symbol}</div>
          </div>
          <div className="">{parseFloat(data.from.value).toFixed(4)}</div>
        </div>

        <div className="transform rotate-90 text-blue-400 flex justify-center mt-3 mb-3">
          <Image
            src="/images/icons/swap/arrow-toggle.svg"
            width="28px"
            height="28px"
          />
        </div>

        <div className="dark:bg-dark-600 grid grid-cols-2 items-center bg-blue-50 p-4 rounded-lg">
          <div className="flex items-center ">
            <img className="tokenIcon h-8" src={toToken.icon} alt="icon" />
            <div className="ml-2">{data.to.symbol}</div>
          </div>
          <div className="">{parseFloat(data.to.value).toFixed(4)}</div>
        </div>
      </div>
      <div className="dark:border-b-dark-500 border-b border-blue-300 pb-5">
        <div className="grid grid-cols-2 gap-3 text-sm items-center ">
          <div className="col-span-1">Price {data.exchangeRateInfo.pair}</div>
          <div className="col-span-1">
            {formatBNToString(data.exchangeRateInfo.exchangeRate, 18, 4)}
          </div>
          <div className="col-span-1">Gas </div>
          <div className="col-span-1">
            {formatGasToString(
              { gasStandard, gasFast, gasInstant },
              gasPriceSelected,
              gasCustom
            )}{" "}
            GWEI
          </div>
          <div className="col-span-1">Max Slippage </div>
          <div className="col-span-1">
            {formatSlippageToString(slippageSelected, slippageCustom)}%
          </div>
          <div className="col-span-1">Deadline </div>
          <div className="col-span-1">{deadline} minutes</div>
          <div className="col-span-2">
            {isHighPriceImpactTxn && (
              <HighPriceImpactConfirmation
                checked={hasConfirmedHighPriceImpact}
                onCheck={() =>
                  setHasConfirmedHighPriceImpact((prevState) => !prevState)
                }
              />
            )}
          </div>
        </div>
      </div>
      {/* footer */}
      <div className="flex justify-between mt-4 text-sm py-2">
        <Button
          className="rounded-md bg-blue-400 text-white px-4 py-2 focus:outline-none"
          onClick={onConfirm}
          kind="primary"
          disabled={isHighPriceImpactTxn}
        >
          Confirm Swap
        </Button>
        <Button
          className="dark:bg-dark-600 rounded-sm bg-gray-200 rounded-md px-4 py-2 focus:outline-none"
          onClick={onClose}
          kind="secondary"
        >
          Cancel
        </Button>
      </div>
    </>
  );
}

export default ReviewSwap;
