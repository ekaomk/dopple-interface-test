import React, { ReactElement, useState, useEffect } from "react";

import ConnectWallet from "../ConnectWallet";
import Modal from "../Modal";
import { useWeb3React } from "@web3-react/core";
import Jazzicon, { jsNumberForAddress } from "react-jazzicon";
import { SignOutIcon, XIcon, ChevronDownIcon } from "@primer/octicons-react";
import Image from "next/image";
import Link from "next/link";
import { CONNECTOR_STORAGE_KEY, DOPPLE } from "../../constants";
import { useActiveWeb3React } from "../../hooks";
import {
  useTokenContract,
  useFairLaunchContract,
} from "../../hooks/useContract";
import { formatBNToString } from "../../utils/index";
import { wallets } from "../../constants/wallets";
import { useSelector } from "react-redux";

const Web3Status = ({ auth }) => {
  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
  } = useActiveWeb3React();
  const doppleTokenContract = useTokenContract(DOPPLE);
  const fairLaunchContract = useFairLaunchContract();

  let fairLaunchDecimals;
  const [modalOpen, setModalLoginOpen] = useState(false);
  const [modalUserInfoOpen, setModalUserInfoOpen] = useState(false);
  const [totalDoppleBalanceOf, setTotalDoppleBalanceOf] = useState("0");
  const [totalUnClaimDoppleInWallet, setTotalUnClaimDoppleInWallet] =
    useState("0");

  const { apiData } = useSelector((state) => state.user);
  const dopPrice = apiData?.twindexPrice?.dopPrice;

  const updatePendingDopple = async (account) => {
    if (!fairLaunchContract || !account) return;
    const pendingDopBnb = await fairLaunchContract.pendingDopple(8, account);
    const pendingDopDolly = await fairLaunchContract.pendingDopple(13, account);
    const pendingDollyLP = await fairLaunchContract.pendingDopple(12, account);
    const pendingUstLP = await fairLaunchContract.pendingDopple(4, account);
    const pending2PoolLp = await fairLaunchContract.pendingDopple(5, account);
    const pendingDopBusdLp = await fairLaunchContract.pendingDopple(9, account);
    const pendingDopLp = await fairLaunchContract.pendingDopple(2, account);
    const pendingStableCoinDopple = await fairLaunchContract.pendingDopple(
      1,
      account
    );

    setTotalUnClaimDoppleInWallet(
      formatBNToString(
        pendingDopBnb
          .add(pendingDopDolly)
          .add(pendingDollyLP)
          .add(pendingUstLP)
          .add(pending2PoolLp)
          .add(pendingDopBusdLp)
          .add(pendingDopLp)
          .add(pendingStableCoinDopple),
        fairLaunchDecimals,
        2
      )
    );
  };

  const getBalance = async (account) => {
    if (!doppleTokenContract || !account) return;
    const totalDoppleInWallet = await doppleTokenContract.balanceOf(account);
    setTotalDoppleBalanceOf(
      formatBNToString(totalDoppleInWallet, fairLaunchDecimals)
    );
  };

  useEffect(() => {
    //
    const connectorName = window.localStorage.getItem(CONNECTOR_STORAGE_KEY);
    if (connectorName && !account) {
      const wallet = wallets.find((w) => {
        return w.name == connectorName;
      });
      if (wallet) auth?.login(wallet.connector, wallet.name);
    } else {
      (async () => {
        await getBalance(account);
        await updatePendingDopple(account);
      })();
    }
  }, [account, chainId, dopPrice]);

  return (
    <>
      <button
        className={`bg-white text-black active:bg-pink-600 border dark:border-dark-700 font-bold uppercase text-xs py-2 px-3 pr-2 sm:pr-3 py-1 rounded outline-none focus:outline-none items-center justify-center flex dark:text-white dark:bg-dark-700`}
        type="button"
        onClick={() =>
          account?.length > 1
            ? setModalUserInfoOpen(true)
            : setModalLoginOpen(true)
        }
      >
        {account && (
          <div className="mx-1 items-center justify-center sm:flex hidden">
            <Jazzicon diameter={22} seed={jsNumberForAddress(account)} />
          </div>
        )}

        {account ? (
          <>
            <span className="mr-1">
              {`${account?.substring(0, 6)}...${account?.substring(
                account?.length - 4,
                account?.length
              )}`}
            </span>
            <ChevronDownIcon size={16} />
          </>
        ) : (
          <div className="py-1">Connect Wallet</div>
        )}
      </button>
      {/* modal connect wallet */}
      <Modal isOpen={modalOpen} onClose={() => setModalLoginOpen(false)}>
        <ConnectWallet onClose={() => setModalLoginOpen(false)} />
      </Modal>
      {/*  */}

      {/* modal user info */}
      <Modal
        isOpen={modalUserInfoOpen}
        onClose={() => setModalUserInfoOpen(false)}
      >
        <div className="px-3">
          {/* header */}
          <div className="flex justify-between">
            <div className="w-6"></div>
            <div className="w-full text-center font-bold text-xl">
              Your Account
            </div>
            <div className="w-6 justify-end flex items-center">
              <div
                className="cursor-pointer"
                onClick={() => setModalUserInfoOpen(false)}
              >
                <XIcon size={24} />
              </div>
            </div>
          </div>
          {/*  */}
          <div className="dark:bg-dark-600 dark:border-0 border rounded-lg text-sm my-4 p-7 flex justify-center items-center">
            <div className="text-center">
              {/* profile icon */}
              {account && (
                <Jazzicon diameter={44} seed={jsNumberForAddress(account)} />
              )}

              <div className="dark:text-white flex justify-center items-center font-bold text-steel-400 text-xl mt-2">
                {account?.substring(0, 6) +
                  " ... " +
                  account?.substring(account.length - 6, account.length)}
              </div>

              <div className="flex justify-center text-blue-400 mt-2 mb-4">
                is Connected
              </div>

              <div className="flex justify-center items-center gap-6 py-2">
                {/* btn bsc scan */}
                <Link href={`https://bscscan.com/address/` + account}>
                  <a target="_blank">
                    <button
                      className={`w-32 dark:bg-dark-500 dark:text-white bg-steel-100 text-blue-400 font-bold px-4 py-2 rounded outline-none focus:outline-none items-center justify-center flex dark:box-shadow`}
                      type="button"
                    >
                      <div className="dark:flex hidden transform rotate-180 flex items-center ">
                        <Image
                          src="/images/icons/bsc-white-icon.svg"
                          height="16px"
                          width="16px"
                        />
                      </div>

                      <div className="dark:hidden flex  transform rotate-180  items-center ">
                        <Image
                          src="/images/icons/bsc-blue-icon.svg"
                          height="16px"
                          width="16px"
                        />
                      </div>

                      <div className="dark:text-white font-bold text-steel-400 ml-1 ">
                        BSC Scan
                      </div>
                    </button>
                  </a>
                </Link>
                {/*  */}

                {/* btn logout */}
                <button
                  className={`w-32 dark:bg-dark-500 dark:text-white bg-steel-100 text-blue-400 font-bold px-4 py-2 rounded outline-none focus:outline-none items-center justify-center flex dark:box-shadow`}
                  type="button"
                  onClick={() => {
                    auth?.logout();
                    setModalUserInfoOpen(false);
                  }}
                >
                  <div className="transform rotate-180 flex items-center ">
                    <SignOutIcon size={16} />
                  </div>
                  <div className="dark:text-white font-bold text-steel-400 ml-1 ">
                    Logout
                  </div>
                </button>
                {/*  */}
              </div>
            </div>
          </div>

          <div className="py-4 pt-2">
            <div className="flex justify-center items-center mb-2">
              <Image src="/images/logo.svg" width="68" height="68" />
            </div>
            <div className="dark:text-white font-bold text-steel-400 text-center mb-4 text-2xl">
              {numberWithCommas(
                (
                  parseFloat(totalDoppleBalanceOf) +
                  parseFloat(totalUnClaimDoppleInWallet)
                ).toFixed(2)
              )}{" "}
              DOP
            </div>
            <div className="flex justify-between text-sm mb-4">
              <div>Balance :</div>
              <div className="dark:text-white font-bold text-steel-400">
                {numberWithCommas(parseFloat(totalDoppleBalanceOf).toFixed(2))}{" "}
                DOP
              </div>
            </div>
            <div className="flex justify-between text-sm mb-4">
              <div>Unclaim Reward :</div>
              <div className="dark:text-white font-bold text-steel-400">
                {numberWithCommas(
                  parseFloat(totalUnClaimDoppleInWallet).toFixed(2)
                )}{" "}
                DOP
              </div>
            </div>

            <div className="flex justify-between text-sm">
              <div>DOP Price :</div>
              <div className="dark:text-white font-bold text-steel-400">
                ${parseFloat(dopPrice).toFixed(4)}
              </div>
            </div>
          </div>

          {/* btn buydop */}
          <Link href="https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28">
            <a target="_blank">
              <div className="w-full items-center justify-center py-4">
                <button className="focus:outline-none w-full justify-center px-4 py-2 text-white flex items-center rounded-lg shadow-sm  bg-gradient-primary  ">
                  <div className="mr-2 flex items-center">
                    <Image
                      src="/images/logo_transparent.svg"
                      width="20"
                      height="20"
                    />
                  </div>
                  <div>Buy DOP</div>
                </button>
              </div>
            </a>
          </Link>
        </div>
      </Modal>
      {/*  */}
    </>
  );
};

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default Web3Status;
