import React from "react";
import classNames from "classnames";
import Image from "next/image";

function getIconForType(type) {
    switch (type) {
        case "error":
            return "/images/icons/iconCancelCircle.svg";
        case "success":
            return "/images/icons/iconCheckCircle.svg";
        case "pending":
            return "/images/icons/iconWaitingCircle.svg";
    }
}
export default function Toast({ title, type, onClick, }) {
    return (
        <div className={classNames("toast", `toast-${type}`, "absolute bottom-10 right-10 p-6 rounded-lg bg-blue-200 text-black")} onClick={onClick}>
            <div className="title flex items-center text-md">
                <Image src={getIconForType(type)} alt="A notification icon" width="20px" height="20px" />
                <span className="ml-2">{title}</span>
            </div>
        </div>
    )
}