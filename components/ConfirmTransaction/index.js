import React, { ReactElement } from "react";
import Image from "next/image";
function ConfirmTransaction() {
  return (
    <>
      <div className="confirmTransaction dark:bg-dark-700  grid grid-cols-1 items-center py-12 text-center">
        <div className="mb-14 dark:hidden flex items-center justify-center">
          <Image src="/images/loading.gif" width="88px" height="88px" />
        </div>
        <div className="mb-12 dark:flex hidden items-center justify-center animate-bounce">
          <Image src="/images/logo.svg" height="80px" width="80px" />
        </div>
        <div className="dark:text-white text-lg text-center w-full">
          Waiting transaction confirm ...
        </div>
      </div>
    </>
  );
}

export default ConfirmTransaction;
