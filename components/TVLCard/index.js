import usePoller from "../../hooks/usePoller";
import { useEffect, useState } from "react";
import { useCountUp } from "react-countup";
import { useSelector } from "react-redux";

export default function TVLCard() {
  const [totalValueLock, setTotalValueLock] = useState("loading...");

  const { apiData } = useSelector((state) => state.user);
  const dopPrice = apiData?.twindexPrice?.dopPrice;
  // second
  const autoReloadTime = 10;
  const { countUp, start, pauseResume, reset, update } = useCountUp({
    start: 0,
    end: 0,
    delay: 1000,
    duration: 2,
  });

  async function updateTotalValueLock() {
    setTotalValueLock(apiData.doppleData.totalTvl);
    update(apiData.doppleData.totalTvl);
  }

  useEffect(() => {
    updateTotalValueLock();
  }, [totalValueLock, dopPrice]);

  usePoller(async () => {
    updateTotalValueLock();
  }, autoReloadTime * 1000);

  return (
    <>
      <div className="">
        <div className=" bg-gradient-primary  rounded-2xl shadow-md mb-6">
          <div className="info-tvl-panel p-6 lg:py-8 text-white">
            <div className="text-center">
              <div className="text-2xl sm:text-3xl mb-1 font-bold">
                Total Value Locked
              </div>
              <div className="text-4xl sm:text-7xl font-bold my-10">
                {countUp == 0 ? (
                  "loading..."
                ) : (
                  <>
                    <span className="sm:mr-3 mr-0">$</span>
                    {numberWithCommas(countUp)}
                  </>
                )}
              </div>
            </div>
            <div className="text-center">
              See more information on{" "}
              <a
                target="_blank"
                className=""
                href="https://bscscan.com/address/0x5162f992EDF7101637446ecCcD5943A9dcC63A8A#code"
              >
                {" "}
                <u>BSCScan</u>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

function numberWithCommas(x) {
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
