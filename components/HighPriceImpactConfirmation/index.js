import React, { ReactElement } from "react";

import CheckboxInput from "../CheckboxInput";
import { useTranslation } from "react-i18next";

export default function HighPriceImpactConfirmation({ checked, onCheck }) {
  return (
    <div className="highPriceImpactConfirmation pt-2">
      <span className="font-bold">High Price Impact Confirmation</span>
      <div className="confirmationBox flex items-center mt-1">
        <CheckboxInput
          id="inputHighPriceImpact"
          checked={checked}
          onChange={onCheck}
        />
        <div>
          <label className="ml-2 cursor-pointer" htmlFor="inputHighPriceImpact">
            Confirm
          </label>{" "}
        </div>
      </div>
    </div>
  );
}
