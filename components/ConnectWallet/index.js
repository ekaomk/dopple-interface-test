import React, { useEffect } from "react";
import { UnsupportedChainIdError, useWeb3React } from "@web3-react/core";
import useAuth from "../../hooks/useAuth";
import { wallets } from "../../constants/wallets";

function ConnectWallet({ onClose }) {
  const { activate } = useWeb3React();
  const auth = useAuth();

  return (
    <>
      <div className="flex justify-between items-center pb-4">
        <div className="text-xl ">Connect Wallet</div>
        <div
          className="text-2xl cursor-pointer"
          onClick={(e) => {
            e.stopPropagation();
            onClose();
          }}
        >
          &times;
        </div>
      </div>
      <div className="walletList">
        <div className="flex flex-wrap overflow-hidden sm:-mx-1">
          {wallets.map((wallet, index) => (
            <div className="w-full overflow-hidden my-1 px-1" key={index}>
              <button
                className="dark:bg-dark-500 focus:outline-none bg-gray-100 hover:bg-gray-200 w-full h-20 rounded-lg px-6 "
                onClick={() => {
                  auth.login(wallet.connector, wallet.name);
                  onClose();
                }}
              >
                <div className="flex items-center text-center mx-auto">
                  <img
                    src={wallet.icon}
                    alt="icon"
                    className="icon w-11 mr-4"
                  />
                  <span className="text-black dark:text-white">
                    {wallet.name}
                  </span>
                </div>
              </button>
            </div>
          ))}
        </div>
      </div>

      <div className="text-sm mt-5 mb-2">
        <span>Don't have a wallet? </span>
        <a
          href="https://ethereum.org/en/wallets/"
          target="blank"
          className="text-blue-400 font-bold"
        >
          &nbsp;Get a wallet !
        </a>
      </div>
    </>
  );
}
export default ConnectWallet;
