import Link from "next/link";
import React, { useState } from "react";
import { useRouter } from "next/router";

export default function Sidebar({ href }) {
  const router = useRouter();
  const [currentPage, setCurrentPage] = useState(
    router.pathname.substr(1).toLowerCase()
  );

  return (
    <>
      <aside className="transform flex top-44 left-0 w-60 bg-transparent fixed h-full z-30 ">
        <div className="mx-auto">
          <ul className="unstyled text-2xl dark:text-white text-gray-700">
            <Link href="/Swap">
              <a>
                <li
                  onClick={() => setCurrentPage("swap")}
                  className={`${
                    currentPage == "swap" && "text-blue-400"
                  } font-bold py-2 `}
                >
                  Swap
                </li>
              </a>
            </Link>

            <Link href="/Deposit">
              <a>
                <li
                  onClick={() => setCurrentPage("deposit")}
                  className={`${
                    currentPage == "deposit" && "text-blue-400"
                  } font-bold py-2 `}
                >
                  Deposit
                </li>
              </a>
            </Link>
            <Link href="/Withdraw">
              <a>
                <li
                  onClick={() => setCurrentPage("withdraw")}
                  className={`${
                    currentPage == "withdraw" && "text-blue-400"
                  } font-bold py-2 `}
                >
                  Withdraw
                </li>
              </a>
            </Link>
            <Link href="/Dolly">
              <a>
                <li
                  onClick={() => setCurrentPage("dolly")}
                  className={`${
                    currentPage == "dolly" && "text-blue-400"
                  } font-bold py-2 `}
                >
                  Dolly
                </li>
              </a>
            </Link>
            <Link href="/Stake">
              <a>
                <li
                  onClick={() => setCurrentPage("stake")}
                  className={`${
                    currentPage == "stake" && "text-blue-400"
                  } font-bold py-2 `}
                >
                  Farm
                </li>
              </a>
            </Link>
          </ul>
        </div>
      </aside>
    </>
  );
}
