import React, { ReactElement } from "react"
import { useDispatch, useSelector } from "react-redux"

import CheckboxInput from "../CheckboxInput"

import ToolTip from "../ToolTip"
import { updateInfiniteApproval } from "../../state/user"

export default function InfiniteApprovalField() {
    const dispatch = useDispatch()
    const { infiniteApproval } = useSelector((state) => state.user)
    return (
        <div className="infiniteApproval">
            <CheckboxInput
                checked={infiniteApproval}
                onChange={() =>
                    dispatch(updateInfiniteApproval(!infiniteApproval))
                }
            />
            <ToolTip content="infiniteApprovalTooltip">
                <span className="label">infiniteApproval</span>
            </ToolTip>
        </div>
    )
}
