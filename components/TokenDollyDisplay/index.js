import React, { useEffect, useState } from "react";
import Button from "../Button";
import { TOKENS_MAP, NETWORK } from "../../constants";
import classNames from "classnames";
import Image from "next/image";
import { useTokenContract, useSwapContract } from "../../hooks/useContract";
import { useActiveWeb3React } from "../../hooks";
import { BigNumber } from "@ethersproject/bignumber";
import { useApproveForDeposit } from "../../hooks/useApproveAndDeposit";
import { Zero, MaxUint256 } from "@ethersproject/constants";
import ConfirmTransaction from "../ConfirmTransaction";
import { commify } from "@ethersproject/units";
import { formatBNToPercentString, formatBNToString } from "../../utils";
import Skeleton from "react-loading-skeleton";

function TokenDollyDisplay({ symbol, icon, inputValue, myShareData }) {
  const [balance, setBalance] = useState("");

  try {
    if (inputValue) {
      inputValue = Math.floor(parseFloat(inputValue) * 10000) / 10000;
    }
  } catch (error) {
    inputValue = "";
  }

  useEffect(() => {
    if (myShareData) {
      myShareData.tokens.map((coin) => {
        if (coin.symbol == symbol) {
          setBalance(commify(formatBNToString(coin.value, 18, 4)));
        }
      });
    }
  }, [myShareData, balance]);

  return (
    <>
      <div className="flex justify-between w-full mt-3">
        <div className="flex items-center">
          <Image src={icon} width="22" height="22" />
          <div className="dark:text-white text-steel-300 text-sm ml-2">
            {symbol}
          </div>
        </div>
        <div className="dark:text-white text-steel-400 text-sm font-bold">
          {inputValue} {symbol}
        </div>
      </div>
    </>
  );
}

export { TokenDollyDisplay };
