import Image from "next/image";
import React, { useEffect, useState } from "react";
import Web3Status from "../Web3Status";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import { useActiveWeb3React } from "../../hooks";
import { useTokenContract } from "../../hooks/useContract";
import { CONNECTOR_STORAGE_KEY, DOPPLE } from "../../constants";
import { formatBNToString } from "../../utils/index";
import useAuth from "../../hooks/useAuth";
import { wallets } from "../../constants/wallets";
import { ThreeBarsIcon } from "@primer/octicons-react";
import { useSelector } from "react-redux";

export default function Navbar() {
  const [modalOpen, setModalOpen] = useState(false);
  const [showMobileMenu, setShowMobileMenu] = useState(false);
  const [totalDoppleBalanceOf, setTotalDoppleBalanceOf] = useState("0");
  const doppleTokenContract = useTokenContract(DOPPLE);
  const [darkMode, setDarkMode] = useState(false);
  const auth = useAuth();
  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
  } = useActiveWeb3React();

  let fairLaunchDecimals;

  const getBalance = async (account) => {
    if (!doppleTokenContract || !account) return;
    const totalDoppleInWallet = await doppleTokenContract.balanceOf(account);
    setTotalDoppleBalanceOf(
      formatBNToString(totalDoppleInWallet, fairLaunchDecimals)
    );
  };

  const { apiData } = useSelector((state) => state.user);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  useEffect(() => {
    //
    const connectorName = window.localStorage.getItem(CONNECTOR_STORAGE_KEY);
    if (connectorName && !account) {
      const wallet = wallets.find((w) => {
        return w.name == connectorName;
      });
      if (wallet) auth.login(wallet.connector, wallet.name);
    } else {
      (async () => {
        await getBalance(account);
      })();
    }
  }, [account, chainId]);

  // toggle darkmode
  const toggleDarkmode = () => {
    let htmlClasses = document.querySelector("html").classList;
    if (localStorage.theme == "dark") {
      htmlClasses.remove("dark");
      localStorage.removeItem("theme");
      setDarkMode(false);
    } else {
      htmlClasses.add("dark");
      localStorage.theme = "dark";
      setDarkMode(true);
    }
  };

  // remember darkmode
  useEffect(() => {
    if (
      localStorage.theme === "dark" ||
      (!"theme" in localStorage &&
        window.matchMedia("(prefers-color-scheme: dark)").matches)
    ) {
      document.querySelector("html").classList.add("dark");
      setDarkMode(true);
    } else if (localStorage.theme === "dark") {
      document.querySelector("html").classList.add("dark");
      setDarkMode(false);
    }
  }, []);

  return (
    <>
      <div className="main-nav container">
        <div className="py-3 pb-0 sm:pb-5 sm:py-5 sm:mb-2 flex justify-between items-center">
          <Link href="/">
            <a>
              <div className="flex items-center cursor-pointer">
                {/* dark */}
                <div className="dark:flex hidden">
                  <div className="lg:flex hidden">
                    <Image
                      src="/images/logo_with_text.svg"
                      height="68"
                      width="184px"
                    />
                  </div>
                  <div className="lg:hidden flex">
                    <Image
                      src="/images/logo_with_text.svg"
                      height="54px"
                      width="120px"
                    />
                  </div>
                </div>

                {/* light */}
                <div className="dark:hidden flex">
                  <div className="lg:flex hidden">
                    <Image
                      src="/images/logo_with_text_light.svg"
                      height="68"
                      width="184px"
                    />
                  </div>
                  <div className="lg:hidden flex">
                    <Image
                      src="/images/logo_with_text_light.svg"
                      height="54px"
                      width="120px"
                    />
                  </div>
                </div>
              </div>
            </a>
          </Link>
          <div className=" flex">
            <div className="sm:flex hidden justify-between">
              <Link href="https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28">
                <a target="_blank">
                  <div className="px-4 py-2 text-white flex items-center rounded-lg shadow-sm  bg-gradient-primary  mr-2  badge-price">
                    <Image
                      src="/images/logo_transparent.svg"
                      width="20"
                      height="20"
                    />
                    <div className="ml-2 text-sm">
                      {!dopPrice ? "loading..." : "$" + dopPrice.toFixed(4)}
                    </div>
                  </div>
                </a>
              </Link>
              <div className="dark:text-white bg-white flex items-center bg-blue-400 text-steel-400 rounded-lg pl-3 ">
                <div className="grid grid-cols-1">
                  <div className="text-white text-sm font-bold">
                    <span>{numberWithCommas(totalDoppleBalanceOf)}</span>
                    <span className="ml-1 mr-3">DOP</span>
                  </div>
                  <div className="text-xs text-white">
                    ≈ $
                    {numberWithCommas(
                      parseFloat(totalDoppleBalanceOf) * dopPrice
                    )}
                  </div>
                </div>
                <div>
                  <Web3Status auth={auth} onClose={() => setModalOpen(false)} />
                </div>
              </div>
            </div>
            {/* lightmode darkmode */}
            <div
              onClick={() => toggleDarkmode()}
              className="mobile-menu-button  w-10 sm:px-2 py-2 ml-2 dark:bg-dark-700 bg-white rounded-lg cursor-pointer dark:bg-dark-700 dark:border-dark-700 border flex justify-center items-center"
            >
              {darkMode ? (
                <Image
                  src="/images/icons/darkmode-icon.svg"
                  width="20px"
                  height="20px"
                />
              ) : (
                <Image
                  src="/images/icons/lightmode-icon.svg"
                  width="20px"
                  height="20px"
                />
              )}
            </div>

            {/* hamburger */}
            <div
              onClick={() => setShowMobileMenu(!showMobileMenu)}
              className="dark:bg-dark-700 text-white mobile-menu-button hidden w-10 sm:w-auto px-2 justify-center sm:px-2 py-2 items-center ml-4 bg-blue-400 rounded-lg cursor-pointer "
            >
              <ThreeBarsIcon size={20} />
            </div>
          </div>
        </div>

        {/* start mobile */}
        <div className="sm:hidden flex my-4 justify-between">
          <Link href="https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28">
            <a target="_blank">
              <div className="px-4 py-2 text-white flex items-center rounded-lg shadow-sm bg-gradient-primary mr-2 badge-price">
                <Image
                  src="/images/logo_transparent.svg"
                  width="20"
                  height="20"
                />
                <div className="ml-2 text-sm">
                  {!dopPrice ? "loading..." : "$" + dopPrice.toFixed(4)}
                </div>
              </div>
            </a>
          </Link>
          <div className="bg-white flex items-center bg-steel-100 dark:text-white dark:bg-dark-700 text-steel-400 rounded-lg pl-3">
            <div className="grid grid-cols-1">
              <div className="text-steel-400 text-sm font-bold dark:text-white">
                <span>{numberWithCommas(totalDoppleBalanceOf)}</span>
                <span className="ml-1 mr-3">DOP</span>
              </div>
              <div className="text-xs text-blue-400">
                ≈ $
                {numberWithCommas(parseFloat(totalDoppleBalanceOf) * dopPrice)}
              </div>
            </div>

            <div>
              <Web3Status
                auth={auth}
                dopPrice={dopPrice}
                balance={numberWithCommas(totalDoppleBalanceOf)}
                onClose={() => setModalOpen(false)}
              />
            </div>
          </div>
        </div>
        {/* end mobile */}
      </div>
      <div
        className={`${
          showMobileMenu ? "active" : ""
        } mobile-menu z-40 relative `}
      >
        <div className="container pb-4 pt-2 px-4">
          <div className="grid grid-cols-1 text-2xl gap-y-4">
            <div onClick={() => setShowMobileMenu(false)}>
              <Link href="/Swap">
                <a>Swap</a>
              </Link>
            </div>
            <div onClick={() => setShowMobileMenu(false)}>
              <Link href="/Deposit">
                <a>Deposit</a>
              </Link>
            </div>
            <div onClick={() => setShowMobileMenu(false)}>
              <Link href="/Withdraw">
                <a>Withdraw</a>
              </Link>
            </div>
            <div onClick={() => setShowMobileMenu(false)}>
              <Link href="/Stake">
                <a>Farm</a>
              </Link>
            </div>
          </div>
        </div>
      </div>

      <div
        onClick={() => setShowMobileMenu(false)}
        className={`${
          showMobileMenu ? "active" : ""
        } mobile-menu-layer hidden absolute h-full w-full bg-black opacity-60`}
      ></div>
    </>
  );
}

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function formatDecimalPlace(value, place) {
  try {
    return Math.floor(parseFloat(value) * (place * 10)) / (place * 10);
  } catch (error) {
    return;
  }
}
