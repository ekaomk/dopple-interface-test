import React, { ReactElement, useState, useCallback } from "react";
import { formatBNToPercentString, formatBNToString } from "../../utils";
import { useDispatch, useSelector } from "react-redux";

import { AppDispatch } from "../../state";
import { AppState } from "../../state/index";
import { BigNumber } from "@ethersproject/bignumber";
import ConfirmTransaction from "../ConfirmTransaction";
import DeadlineField from "../DeadlineField";
import GasField from "../GasField";
import InfiniteApprovalField from "../InfiniteApprovalField";
import Modal from "../Modal";
import { PayloadAction } from "@reduxjs/toolkit";
import ReviewSwap from "../ReviewSwap";
import SlippageField from "../SlippageField";
import SwapForm from "../SwapForm";
import classNames from "classnames";
import { isHighPriceImpact } from "../../utils/priceImpact";
import { updateSwapAdvancedMode } from "../../state/user";
import { useActiveWeb3React } from "../../hooks";

import WhitePanel from "../../components/layouts/WhitePanel";
import Reward from "../../components/Reward/Reward";
import Image from "next/image";
import Link from "next/link";

import usePoller from "../../hooks/usePoller";
import fetchGasPrices from "../../utils/updateGasPrices";
import {
  BLOCK_TIME,
  NETWORK,
  SWAP_EXCHANGE,
  POOL_LIST,
  TOKEN_LIST,
  TOKENS_MAP,
} from "../../constants";
import { useSwapExchange } from "../../hooks/useSwapExchange";
import { formatUnits } from "@ethersproject/units";
import { useEffect } from "react";
import { useSwapContract, useTokenContract } from "../../hooks/useContract";
import { Zero, MaxUint256 } from "@ethersproject/constants";

export default function SwapPage(props) {
  const { account, chainId, error: web3Error } = useActiveWeb3React();
  const {
    tokenFrom,
    tokenTo,
    exchangeRateInfo,
    error,
    fromState,
    toState,
    onChangeFromToken,
    onChangeFromAmount,
    onChangeToToken,
    onConfirmTransaction,
    onClickReverseExchangeDirection,
  } = props;
  // const [mostValueCompare, setMostValueCompare] = useState(0);
  const poolList = POOL_LIST;
  let fairLaunchDecimals;
  const [isAllowance, setIsAllowance] = useState(true);
  const [currentModal, setCurrentModal] = useState();
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const fetchAndUpdateGasPrice = useCallback(() => {
    fetchGasPrices(dispatch);
  }, [dispatch]);

  usePoller(fetchAndUpdateGasPrice, BLOCK_TIME);

  const swapContractList = {};
  const tokenContractList = {};

  poolList.map((pool_name, index) => {
    swapContractList[pool_name] = useSwapContract(pool_name);
  });

  Object.keys(TOKENS_MAP).map((tokenName, index) => {
    tokenContractList[tokenName] = useTokenContract(TOKEN_LIST[tokenName]);
  });

  const checkAllowance = async ({ tokenName, poolName }) => {
    if (poolName == "") return setIsAllowance(true);
    if (fromState.value == "") return setIsAllowance(true);

    if (!account) return;

    setLoading(true);
    try {
      const result = await tokenContractList[tokenName].allowance(
        account,
        swapContractList[poolName]?.address
      );
      setIsAllowance(!result.isZero());
    } catch (error) {}

    setLoading(false);
  };

  useEffect(async () => {
    setIsAllowance(true);
    await checkAllowance({
      tokenName: fromState.symbol,
      poolName: exchangeRateInfo.tradingRoute,
    });
  }, [fromState.symbol, exchangeRateInfo.tradingRoute]);

  const approveSwap = async ({ tokenName, poolName }) => {
    if (poolName == "") return setIsAllowance(true);
    if (!account) return;

    setCurrentModal("confirm");
    try {
      const result = await tokenContractList[tokenName].approve(
        swapContractList[poolName].address,
        MaxUint256
      );
      await result.wait();
      setCurrentModal(null);
      checkAllowance({
        tokenName: fromState.symbol,
        poolName: exchangeRateInfo.tradingRoute,
      });
    } catch (error) {
      setCurrentModal(null);
    }
  };

  return (
    <>
      <div className="container">
        <WhitePanel>
          <div className="container">
            <div className="dark:text-white text-lg sm:text-4xl text-center font-bold pb-4 sm:pb-10 mb-5">
              <span>Swap your</span>
              <span className="text-blue-400"> Stablecoins </span>
              <span>at the</span>
              <span className="text-blue-400"> Best Rate</span>
              <div className="dark:text-white text-steel-300 text-xs sm:text-sm mt-4 sm:px-20 font-normal">
                We use an algorithm that is designed for swapping stablecoins
                the most efficient way. Our transaction fees are 4x lower than
                on PancakeSwap.
              </div>
            </div>
          </div>
          <div className="grid grid-cols-11 gap-3 mt-6">
            <div className="col-span-11 sm:col-span-5">
              <SwapForm
                isSwapFrom={true}
                tokens={tokenFrom}
                onChangeSelected={onChangeFromToken}
                onChangeAmount={onChangeFromAmount}
                selected={fromState.symbol}
                inputValue={fromState.value}
                // inputValue={parseFloat(fromState.value).toFixed(2)}
              />
              <div className="spacer" />
            </div>
            <div className="col-span-11 sm:col-span-1 flex items-center">
              <div className="mx-auto mt-2 sm:mt-0 sm:mb-4 transform rotate-90 sm:rotate-0">
                <button
                  className="focus:outline-none"
                  onClick={onClickReverseExchangeDirection}
                >
                  <Image
                    src="/images/icons/swap/arrow-toggle.svg"
                    width="28px"
                    height="28px"
                  />
                </button>
              </div>
            </div>
            <div className="col-span-11 sm:col-span-5">
              <SwapForm
                isSwapFrom={false}
                tokens={tokenTo}
                onChangeSelected={onChangeToToken}
                selected={toState.symbol}
                // inputValue={toState.value}
                inputValue={
                  toState.value == 0 ? "" : parseFloat(toState.value).toFixed(4)
                }
              />
            </div>
          </div>

          <div className="flex justify-center pt-6">
            {fromState.value && NETWORK === chainId && (
              <div className="dark:bg-dark-600 dark:text-white dark:border-dark-700 border border-steel-200 rounded-md w-full sm:w-3/4 px-4 sm:px-6 py-3 pb-4 sm:pb-6">
                <SwapRateDopple
                  fromInputValue={fromState.value}
                  toInputValue={toState.value}
                  onChangeFromAmount={onChangeFromAmount}
                />

                {SWAP_EXCHANGE.map((swap, index) => (
                  <SwapRate
                    key={index}
                    swap={swap}
                    tokens={tokenFrom}
                    fromSelected={fromState.symbol}
                    toSelected={toState.symbol}
                    fromInputValue={fromState.value}
                    toInputValue={toState.value}
                  />
                ))}
              </div>
            )}
          </div>

          <div className="flex pt-4 min-h-24">
            <div className="w-full sm:w-auto sm:mx-auto mt-4 mb-8">
              {/* if allowance == true*/}
              {loading ? (
                <div className="py-3 px-32 invisible">Loading</div>
              ) : (
                <>
                  {isAllowance ? (
                    <button
                      type="button"
                      className={`py-3 px-32 rounded-lg ${
                        NETWORK !== chainId || error
                          ? "bg-red-400"
                          : " bg-gradient-primary "
                      } text-white font-bold focus:outline-none`}
                      onClick={() => {
                        setCurrentModal("review");
                      }}
                      disabled={
                        !!error || +toState.value <= 0 || NETWORK !== chainId
                      }
                    >
                      {NETWORK !== chainId
                        ? "Invalid network"
                        : error
                        ? error
                        : "Swap"}
                    </button>
                  ) : (
                    <div className="flex gap-2">
                      <button
                        type="button"
                        className={`focus:outline-none py-3 w-44 rounded-lg  bg-gradient-primary  text-white`}
                        onClick={() => {
                          approveSwap({
                            tokenName: fromState.symbol,
                            poolName: exchangeRateInfo.tradingRoute,
                          });
                        }}
                      >
                        Approve {fromState.symbol}
                      </button>

                      <button
                        type="button"
                        className={`focus:outline-none py-3 w-44 rounded-lg bg-gray-400 text-white`}
                      >
                        Swap
                      </button>
                    </div>
                  )}
                </>
              )}

              {/* ------ */}
            </div>
          </div>
          <div className="dark:text-white text-center text-gray-500 pt-2 mb-4">
            <div className="text-sm">
              Exchange Rate {fromState.symbol}/{toState.symbol} (including
              fees):&nbsp;
              <span className="font-bold text-blue-400">
                {"1 : " +
                  formatBNToString(
                    exchangeRateInfo.exchangeRate,
                    fairLaunchDecimals,
                    4
                  )}
              </span>
            </div>
            <div className="text-sm">
              Trading routed through:&nbsp;
              <span className="font-bold text-blue-400">
                {exchangeRateInfo.tradingRoute == ""
                  ? "Dopple Pool"
                  : exchangeRateInfo.tradingRoute}
              </span>
            </div>
            <div className="text-sm">
              Max slippage:&nbsp;
              <span className="font-bold text-blue-400">1%</span>
            </div>
          </div>

          {/*  */}
          <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
            {currentModal === "review" ? (
              <ReviewSwap
                onClose={() => setCurrentModal(null)}
                onConfirm={async () => {
                  setCurrentModal("confirm");
                  await onConfirmTransaction?.();
                  setCurrentModal(null);
                }}
                data={{
                  from: fromState,
                  to: toState,
                  exchangeRateInfo,
                }}
              />
            ) : null}
            {currentModal === "confirm" ? <ConfirmTransaction /> : null}
          </Modal>
          {/*  */}
        </WhitePanel>
        {/* Banner component */}
        <TradingVolume />
        {/* Reward component */}
        <Reward />
      </div>
    </>
  );
}

const Banner = () => {
  return (
    <>
      <div className="flex mb-4">
        <a href="/Info">
          <div className="w-full cursor-pointer">
            <div className="relative -ml-2 -mb-14 sm:-mb-20 z-30 w-14 sm:w-20 sm:block hidden">
              <Image
                src="/images/icons/icon-new.png"
                width="500"
                height="500"
              />
            </div>
            <Image
              src="/images/banner/banner-10m@2x.png"
              width="2000"
              height="554"
              objectFit="contain"
            />
          </div>
        </a>
      </div>
    </>
  );
};

const TradingVolume = () => {
  const [tradingVolume, setTradingVolume] = useState(0);

  const { apiData } = useSelector((state) => state.user);

  async function updateTradingVolume() {
    setTradingVolume(apiData?.dopple_data?.total_trading_volume);
  }

  useEffect(() => {
    updateTradingVolume();
  }, []);

  return (
    <>
      <div className=" bg-gradient-primary  rounded-3xl shadow-md mb-6">
        <div className="info-tvl-panel p-6 sm:py-16 text-white">
          <div className="text-center">
            <div className="sm:text-3xl mb-1 font-bold">Trading Volume</div>
            <div className="text-4xl sm:text-7xl font-bold sm:mt-10">
              {tradingVolume == 0 ? (
                "loading..."
              ) : (
                <>
                  <span className="sm:mr-3 mr-0">$</span>
                  {numberWithCommasFixedZero(parseFloat(tradingVolume))}
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const SwapRate = ({
  swap,
  tokens,
  fromSelected,
  toSelected,
  fromInputValue,
  toInputValue,
}) => {
  const tokenForm = tokens.find((t) => t.symbol === fromSelected);
  const tokenTo = tokens.find((t) => t.symbol === toSelected);

  if (tokenForm.symbol == "DOLLY" || tokenTo.symbol == "DOLLY") {
    return null;
  }

  const swapExchange = useSwapExchange(
    swap,
    tokenForm,
    tokenTo,
    fromInputValue,
    toInputValue
  );

  const priceImpact = 0;

  return (
    <>
      <div className="grid grid-cols-12 items-center h-10 sm:text-base text-xs">
        <div className="col-span-1 p-1 sm:p-4 flex items-center">
          <Image src={swap.icon} width="500" height="500" objectFit="contain" />
        </div>
        <div className="dark:text-white pl-1 sm:pl-0 sm:col-span-2 col-span-3 text-steel-300">
          {swap.name}
        </div>
        <div className="col-span-3 text-center font-bold hidden sm:block  overflow-auto">
          {parseFloat(fromInputValue).toFixed(4)}
        </div>
        <div className="col-span-1 text-center flex items-center justify-center">
          <Image
            src={`/images/icons/swap/arrow-right.svg`}
            width="18"
            height="18"
            objectFit="contain"
          />
        </div>
        <div className="sm:col-span-3 col-span-5 text-center font-bold overflow-auto text-red-400 line-through">
          {swapExchange.pairPrice ? swapExchange.pairPrice : ""}
        </div>
        {/* <div
          className={`col-span-2 flex items-center justify-end ${parseFloat(priceImpact) < 0 ? "text-pink-500" : "text-green-500"
            }`}
        >
          <div>{parseFloat(priceImpact).toFixed(2)}%</div>
        </div> */}
      </div>
    </>
  );
};

const SwapRateDopple = ({
  fromInputValue,
  toInputValue,
  onChangeFromAmount,
}) => {
  const priceImpact = 0;

  return (
    <>
      <div className="grid grid-cols-12 items-center h-10 sm:text-base text-xs">
        <div className="col-span-1 p-1 sm:p-4 flex items-center">
          <Image
            src={`/images/logo.svg`}
            width="500"
            height="500"
            objectFit="contain"
          />
        </div>
        <div className="dark:text-white pl-1 sm:pl-0 sm:col-span-2 col-span-3 text-steel-300">
          Dopple
        </div>
        <div className="col-span-3 text-center font-bold hidden sm:block  overflow-auto">
          {parseFloat(fromInputValue).toFixed(4)}
        </div>
        <div className="col-span-1 text-center flex items-center justify-center">
          <Image
            src={`/images/icons/swap/arrow-right.svg`}
            width="18"
            height="18"
            objectFit="contain"
          />
        </div>
        <div className="sm:col-span-3 col-span-5 text-center font-bold overflow-auto">
          {toInputValue == 0 ? "" : parseFloat(toInputValue).toFixed(4)}
        </div>
        {/* <div
          className={`col-span-2 flex items-center justify-end ${parseFloat(priceImpact) < 0 ? "text-pink-500" : "text-green-500"
            }`}
        >
          <div>{parseFloat(priceImpact).toFixed(2)}%</div>
        </div> */}
      </div>
    </>
  );
};

function numberWithCommasFixedZero(x) {
  var x = Number.parseFloat(x).toFixed(0);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
