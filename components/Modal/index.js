import React, { ReactElement, ReactNode } from "react";

function Modal({ isOpen, onClose, children }) {
  if (!isOpen) return null;
  else
    return (
      // Modal container, provide the dark background
      <div
        className="modal"
        onClick={(e) => {
          if (e.target === e.currentTarget) {
            onClose();
          }
        }}
      >
        {/* Modal content */}
        <div
          className="modal-body flex flex-wrap overflow-hidden -mx-1"
          onClick={(e) => {
            if (e.target === e.currentTarget) {
              onClose();
            }
          }}
        >
          <div className="relative z-50 mx-auto sm:max-w-30 overflow-hidden my-1 px-1">
            <div className="overflow-hidden">
              <div className="modal-panel dark:text-white dark:bg-dark-700 bg-white  p-6 rounded-lg">
                {children}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
}

export default Modal;
