import React, { ReactElement } from "react"
import Image from "next/image";

function NoShareContent() {
    return (
        <div className="no-share">
            <Image src="/images/assets/deposit_graph.svg" height="64px" width="64px" alt="put tokens in pool" />
            <h2>
                noDepositTitle
        <br />
        noDepositTitle2
      </h2>
            <p>noDeposit2</p>
            <a href="/#/deposit">
                <button className="actionBtn">deposit</button>
            </a>
        </div>
    )
}

export default NoShareContent