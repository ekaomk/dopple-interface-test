import { useWeb3React } from "@web3-react/core";
import React, { useEffect, useState } from "react";
import { Contract } from "@ethersproject/contracts";
import ERC20ABI from "../constants/abis/ERC20.abi.json";
import { formatUnits } from "@ethersproject/units";
import { NetworkContextName } from "../constants"

export const TokenBalance = ({ symbol, address, decimals }) => {
    const { account, library } = useWeb3React(NetworkContextName)
    const [balance, setBalance] = useState()

    useEffect(() => {
        // listen for changes on an Ethereum address
        console.log(`listening for Transfer...`)
        const contract = new Contract(address, ERC20ABI, library.getSigner())
        contract.functions.balanceOf(account).then(({ balance }) => {
            setBalance(balance)
        })
    }, [])

    if (!balance) {
        return <div>...</div>
    }
    return (
        <div>
            BALANCE {parseFloat(formatUnits(balance, decimals)).toPrecision(4)} {symbol}
        </div>
    )
}