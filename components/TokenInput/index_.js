import React, { useEffect, useState } from "react";
import ERC20ABI from "../../constants/abis/ERC20.abi.json";
import { Contract } from "@ethersproject/contracts";
import { formatUnits } from "@ethersproject/units";

export default function TokenInput({ token, library, account }) {
  const [balance, setBalance] = useState();

  useEffect(() => {
    // listen for changes on an Ethereum address
    if (!token || !library) return;
    const contract = new Contract(token.address, ERC20ABI, library.getSigner());
    contract.functions.balanceOf(account).then(({ balance }) => {
      setBalance(balance);
    });
  }, []);
  return (
    <>
      <div className="my-auto mx-auto w-full ">
        <div className="flex items-center justify-start gap-3 py-2 mx-3 border-b">
          {token && <img className="w-12 h-12 rounded-md" src={token.icon} />}

          <div className="flex flex-grow  ">
            <div className="flex w-8/12">
              <input
                type="text"
                placeholder="0.0"
                className="
                  bg-white
                  text-sm
                  text-gray-900
                  text-center
                  focus:outline-none
                  border
                  border-gray-200
                  focus:border-gray-200
                  rounded-l-md w-full"
              />
            </div>
            <div className="flex flex-col w-4/12">
              <button
                className="
                text-white
                text-center
                text-sm
                rounded-r-md
                px-1 
                bg-gray-400
                border
                border-gray-400
                focus:outline-none
                "
              >
                Balance <br />
                {balance &&
                  parseFloat(formatUnits(balance, token.decimals)).toPrecision(
                    4
                  )}
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
