import React, { useEffect, useState } from "react";
import Button from "../Button";
import { TOKENS_MAP, NETWORK } from "../../constants";
import classNames from "classnames";
import Image from "next/image";
import { useTokenContract, useSwapContract } from "../../hooks/useContract";
import { useActiveWeb3React } from "../../hooks";
import { BigNumber } from "@ethersproject/bignumber";
import { useApproveForDeposit } from "../../hooks/useApproveAndDeposit";
import { Zero, MaxUint256 } from "@ethersproject/constants";
import ConfirmTransaction from "../ConfirmTransaction";
import { commify } from "@ethersproject/units";
import { formatBNToPercentString, formatBNToString } from "../../utils";
import Skeleton from "react-loading-skeleton";

function TokenInputDeposit({
  symbol,
  icon,
  max,
  inputValue,
  onChange,
  disabled,
  borderBottom,
  address,
  setCurrentModal,
  myShareData,
  maxBalance,
  poolName,
}) {
  const { account, chainId, error: web3Error } = useActiveWeb3React();
  max = parseFloat(Math.floor(max)).toFixed(2);
  const [allowance, setAllowance] = useState(BigNumber.from("0"));
  const [balance, setBalance] = useState("");

  const approveForDepositToken = useApproveForDeposit(
    poolName,
    TOKENS_MAP[symbol]
  );
  const swapContract = useSwapContract(poolName);
  const tokenContract = useTokenContract(TOKENS_MAP[symbol]);

  const approveForDeposit = async () => {
    if (NETWORK != chainId) return;
    setCurrentModal("confirm");
    await approveForDepositToken();
    await updateAllowance();
    setCurrentModal(null);
  };

  const updateAllowance = async () => {
    if (!tokenContract || !swapContract) return;
    const existingAllowance = await tokenContract.allowance(
      account,
      swapContract.address
    );
    setAllowance(existingAllowance);
  };

  useEffect(() => {
    (async () => {
      updateAllowance();
    })();

    setBalance(maxBalance);
  }, [tokenContract, swapContract, chainId, myShareData, balance]);

  function onClickMax(e) {
    e.preventDefault();
    onChange(String(max));
  }
  function onChangeInput(e) {
    const { decimals } = TOKENS_MAP[symbol];
    const parsedValue = parseFloat("0" + e.target.value);
    const periodIndex = e.target.value.indexOf(".");
    const isValidInput = e.target.value === "" || !isNaN(parsedValue);
    const isValidPrecision =
      periodIndex === -1 || e.target.value.length - 1 - periodIndex <= decimals;
    if (isValidInput && isValidPrecision) {
      // don't allow input longer than the token allows
      onChange(e.target.value);
    }
  }

  return (
    <>
      <div className="overflow-hidden relative">
        {/* balance text */}
        <div className="flex justify-between pb-1">
          <div className="pl-1 text-sm flex">
            <div className="dark:text-steel-300">Balance :&nbsp;</div>
            <span className="text-blue-400 font-bold">
              {balance && balance + " " + symbol}
            </span>
          </div>
        </div>
        {/* input */}
        <div className="pb-8 ">
          {allowance.eq(0) && (
            <AllowanceButton
              icon={icon}
              symbol={symbol}
              approve={approveForDeposit}
            />
          )}
          <input
            type="number"
            className="dark:text-white border rounded-md focus:outline-none dark:bg-dark-600 dark:border-0 bg-steel-100 p-2 pl-32 h-12 w-full"
            placeholder="0.0"
            disabled={disabled ? true : false}
            value={inputValue == 0 ? "" : inputValue}
            onChange={onChangeInput}
            placeholder="0.0"
            onFocus={(e) => e.target.select()}
          />

          <div className="flex justify-between ">
            {/* icon  */}
            <div className="dark:border-0 dark:text-white dark:bg-dark-500 border rounded-md bg-white w-28 flex items-center p-2 -mt-12 z-10 relative">
              <Image src={icon} width="30px" height="30px" />
              <span className="font-bold ml-2">{symbol}</span>
            </div>
            {/* max btn */}
            <div className="p-1 -mt-12 z-10 relative">
              <button
                className={`focus:outline-none px-4 py-2 bg-blue-400 rounded-md text-white${
                  allowance.eq(0) ? " hidden" : ""
                }`}
                onClick={onClickMax}
              >
                MAX
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

function TokenInputWithdraw({
  symbol,
  icon,
  inputValue,
  onChange,
  disabled,
  borderBottom,
  myShareData,
  radioActive,
  setRadioActive,
}) {
  const [balance, setBalance] = useState("");

  try {
    if (inputValue) {
      inputValue = Math.floor(parseFloat(inputValue) * 10000) / 10000;
    }
  } catch (error) {
    inputValue = "";
  }

  function onChangeInput(e) {
    const { decimals } = TOKENS_MAP[symbol];
    const parsedValue = parseFloat("0" + e.target.value);
    const periodIndex = e.target.value.indexOf(".");
    const isValidInput = e.target.value === "" || !isNaN(parsedValue);
    const isValidPrecision =
      periodIndex === -1 || e.target.value.length - 1 - periodIndex <= decimals;
    if (isValidInput && isValidPrecision) {
      // don't allow input longer than the token allows
      onChange(e.target.value);
    }
  }

  useEffect(() => {
    if (myShareData) {
      myShareData.tokens.map((coin) => {
        if (coin.symbol == symbol) {
          setBalance(commify(formatBNToString(coin.value, 18, 4)));
        }
      });
    }
  }, [myShareData, balance]);

  return (
    <>
      <div className="overflow-hidden relative">
        {/* input */}
        <div className="pb-8 flex justify-center items-center">
          <div>
            <div
              className={`rounded-full dark:bg-dark-500 radio-padding-outline radio-outline cursor-pointer ${
                radioActive == symbol || radioActive == "ALL" ? "active" : ""
              }`}
              onClick={() => setRadioActive(symbol)}
            >
              <div className="rounded-full bg-white radio-padding"></div>
            </div>
          </div>
          <div className="ml-4 w-full">
            <input
              type="number"
              className="dark:bg-dark-600 dark:border-0 border rounded-md focus:outline-none bg-steel-100 p-2 pl-32 h-12 w-full dark:text-white"
              placeholder="0.0"
              disabled={disabled ? true : false}
              value={inputValue}
              onChange={onChangeInput}
              placeholder="0.0"
              onFocus={(e) => e.target.select()}
            />
            <div className="flex justify-between ">
              {/* icon  */}
              <div
                className="dark:bg-dark-500 dark:border-0 border rounded-md bg-white w-28 flex items-center p-2 -mt-12 z-10 relative cursor-pointer"
                onClick={() => setRadioActive(symbol)}
              >
                <Image src={icon} width="30px" height="30px" />
                <span className="font-bold ml-2">{symbol}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const AllowanceButton = ({ icon, symbol, approve }) => {
  return (
    <>
      <div className=" overflow-hidden">
        <div className=" dark:bg-dark-600 dark:border-0 bg-white absolute w-full h-18 z-40 bg-opacity-100 rounded backdrop-filter flex items-center justify-center">
          <button
            type="button"
            className={`focus:outline-none  bg-transparent text-center py-2  h-full w-full border rounded-md font-bold border-blue-500 text-blue-500`}
            onClick={(e) => {
              approve();
            }}
          >
            <div className="flex justify-between px-2 items-center">
              <div className="flex items-center">
                <Image src={icon} width="30px" height="30px" />
                <span className="dark:text-white text-black font-base ml-2">
                  {symbol}
                </span>
              </div>
              <span> Approve</span>
              <div className="flex items-center invisible">
                <Image src={icon} width="30px" height="30px" />
                <span className="text-black font-base ml-2">{symbol}</span>
              </div>
            </div>
          </button>
        </div>
      </div>
    </>
  );
};

export { TokenInputDeposit, TokenInputWithdraw };
