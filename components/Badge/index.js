import React, { ReactElement } from "react";

export default function Badge({ text, className }) {
  return (
    <span
      className={`${
        className && ""
      } rounded-lg text-white bg-pink-500 px-1 sm:px-2 ml-1 sm:px-3 sm:text-base text-xs`}
    >
      {text}
    </span>
  );
}
