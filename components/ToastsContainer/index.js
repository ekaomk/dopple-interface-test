import React from "react";
import Image from "next/image";
export default function ToastsContainer({ children, }) {
    return <div className={"toast-container"}>
        {children}
    </div>;
}