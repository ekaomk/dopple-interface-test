export default function PageNotFound() {
  return (
    <>
      <div className="container bg-white text-center py-20 rounded-lg mb-4">
        <div className="text-4xl text-blue-400 font-bold py-20">
          PAGE NOT FOUND
        </div>
      </div>
    </>
  );
}
