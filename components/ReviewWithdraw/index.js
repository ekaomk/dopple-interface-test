import React, { ReactElement, useState } from "react";

import { AppState } from "../../state/index";
import Button from "../Button";
import { GasPrices } from "../../state/user";
import HighPriceImpactConfirmation from "../HighPriceImpactConfirmation";
import { ReviewWithdrawData } from "../WithdrawPage";
import { formatBNToString, formatDeadlineToNumber } from "../../utils";
import { formatGasToString } from "../../utils/gas";
import { formatSlippageToString } from "../../utils/slippage";
import { isHighPriceImpact } from "../../utils/priceImpact";
import { useSelector } from "react-redux";

function ReviewWithdraw({ onClose, onConfirm, data }) {
  const {
    slippageCustom,
    slippageSelected,
    gasPriceSelected,
    gasCustom,
    transactionDeadlineSelected,
    transactionDeadlineCustom,
  } = useSelector((state) => state.user);
  const { gasStandard, gasFast, gasInstant } = useSelector(
    (state) => state.application
  );
  const [hasConfirmedHighPriceImpact, setHasConfirmedHighPriceImpact] =
    useState(false);
  const isHighSlippageTxn = isHighPriceImpact(data.priceImpact);
  const deadline = formatDeadlineToNumber(
    transactionDeadlineSelected,
    transactionDeadlineCustom
  );

  console.log("review withdraw : ", data.withdraw);

  return (
    <>
      {/* title */}
      <div className="dark:text-white flex justify-between items-center pb-4">
        <div className="text-xl font-bold">Review Withdraw</div>
        <div
          className="text-2xl cursor-pointer"
          onClick={(e) => {
            e.stopPropagation();
            onClose();
          }}
        >
          &times;
        </div>
      </div>

      {/* body */}
      <div className="dark:border-b-dark-500 dark:text-white border-b border-blue-300 mb-4 pb-5">
        <div className="text-sm  ">Withdraw list</div>
        {data.withdraw.map((token, index) => (
          <div className="grid grid-cols-2 gap-4 text-sm mt-3" key={index}>
            <div className="col-span-1 flex items-center ">
              <img src={token.icon} className="h-5" alt="icon" />
              <span className="ml-2">{token.name}</span>
            </div>
            <div className="col-span-1 flex items-center">{token.value}</div>
          </div>
        ))}
      </div>
      <div className="dark:border-b-dark-500 dark:text-white border-b border-blue-300 pb-3">
        <div className="grid grid-cols-2 gap-3 text-sm items-center ">
          <div className="col-span-1">Gas </div>
          <div className="col-span-1">
            {formatGasToString(
              { gasStandard, gasFast, gasInstant },
              gasPriceSelected,
              gasCustom
            )}{" "}
            GWEI
          </div>
          <div className="col-span-1">Max Slippage </div>
          <div className="col-span-1">
            {formatSlippageToString(slippageSelected, slippageCustom)} %
          </div>
          <div className="col-span-1">Deadline </div>
          <div className="col-span-1">{deadline} minutes</div>
          <div className="col-span-1">Rates </div>
          <div className="col-span-1">
            {" "}
            {data.rates.map((rate, index) => (
              <span key={index}>
                1 {rate.name} = ${rate.rate}
              </span>
            ))}
          </div>
          <div className="col-span-2">
            {isHighSlippageTxn && (
              <HighPriceImpactConfirmation
                checked={hasConfirmedHighPriceImpact}
                onCheck={() =>
                  setHasConfirmedHighPriceImpact((prevState) => !prevState)
                }
              />
            )}
          </div>
        </div>
      </div>
      {/* footer */}
      <div className="flex justify-between mt-4 text-sm py-2">
        <Button
          className="rounded-md bg-blue-400 text-white px-4 py-2 focus:outline-none"
          onClick={onConfirm}
          kind="primary"
          disabled={isHighSlippageTxn && !hasConfirmedHighPriceImpact}
        >
          Confirm
        </Button>
        <Button
          className="rounded-sm bg-gray-200 rounded-md px-4 py-2 focus:outline-none dark:bg-dark-600"
          onClick={onClose}
          kind="secondary"
        >
          Cancel
        </Button>
      </div>
    </>
  );
}

export default ReviewWithdraw;

function formatDecimalPlace(value) {
  try {
    return Math.floor(parseFloat(value) * 10000) / 10000;
  } catch (error) {
    return;
  }
}
