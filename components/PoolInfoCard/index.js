import { POOL_FEE_PRECISION, TOKENS_MAP } from "../../constants";
import React, { ReactElement } from "react";
import { formatBNToPercentString, formatBNToString } from "../../utils";

import { PoolDataType } from "../../hooks/usePoolData";
import { commify } from "@ethersproject/units";

function PoolInfoCard({ data }) {
  const swapFee = data?.swapFee
    ? formatBNToPercentString(data.swapFee, POOL_FEE_PRECISION)
    : null;
  const adminFee = data?.adminFee
    ? formatBNToPercentString(data.adminFee, POOL_FEE_PRECISION)
    : null;
  const formattedData = {
    name: data?.name,
    swapFee,
    reserve: data?.reserve ? commify(formatBNToString(data.reserve, 18)) : "0",
    adminFee: swapFee && adminFee ? `${adminFee} of ${swapFee}` : null,
    tokens:
      data?.tokens.map((coin) => {
        const token = TOKENS_MAP[coin.symbol];
        return {
          symbol: token.symbol,
          name: token.name,
          icon: token.icon,
          percent: coin.percent,
          value: commify(formatBNToString(coin.value, 18, 4)),
        };
      }) || [],
  };

  return (
    <div className="poolInfoCard pt-2">
      {/* <h4>{formattedData.name}</h4> */}
      <div className="text-xl font-bold mb-1 mt-2">Detail</div>
      <div className="info text-steel-300  grid grid-cols-1 gap-y-1 pt-1">
        <div>
          <span>Fee : &nbsp; </span>
          <span className="text-blue-400 font-bold">
            {formattedData.swapFee || 0}
          </span>
        </div>
      </div>
    </div>
  );
}

export default PoolInfoCard;
// <div>
//     <span>Total locked : &nbsp; </span>
//     <span className="text-blue-400 font-bold">
//       {formattedData.reserve || 0}
//     </span>
//   </div>

//   <div>
//     <span>Admin Fee : &nbsp; </span>
//     <span className="text-blue-400 font-bold">
//       {formattedData.adminFee || 0}
//     </span>
//   </div>
