import React, { ReactElement } from "react"

export default function ToolTip({
    content,
    children,
}) {
    return (
        <div className="toolTip">
            {children}
            <div className="tooltipText">{content}</div>
        </div>
    )
}
