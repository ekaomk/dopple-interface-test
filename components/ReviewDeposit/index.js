import React, { ReactElement, useState } from "react";
import {
  formatBNToPercentString,
  formatBNToString,
  formatDeadlineToNumber,
} from "../../utils";

import Button from "../Button";
import HighPriceImpactConfirmation from "../HighPriceImpactConfirmation";
import { commify } from "@ethersproject/units";
import { formatGasToString } from "../../utils/gas";
import { formatSlippageToString } from "../../utils/slippage";
import { isHighPriceImpact } from "../../utils/priceImpact";
import { useSelector } from "react-redux";

function ReviewDeposit({ onClose, onConfirm, transactionData }) {
  const {
    slippageCustom,
    slippageSelected,
    gasPriceSelected,
    gasCustom,
    transactionDeadlineSelected,
    transactionDeadlineCustom,
  } = useSelector((state) => state.user);
  const { gasStandard, gasFast, gasInstant } = useSelector(
    (state) => state.application
  );
  const [hasConfirmedHighPriceImpact, setHasConfirmedHighPriceImpact] =
    useState(false);
  const isHighPriceImpactTxn = isHighPriceImpact(transactionData.priceImpact);
  const deadline = formatDeadlineToNumber(
    transactionDeadlineSelected,
    transactionDeadlineCustom
  );
  return (
    <>
      <div className="flex justify-between items-center pb-4 dark:text-white">
        <div className="text-xl font-bold">
          Review
          {transactionData.to.item.token.geckoId.toUpperCase() == "DOLLY"
            ? " Mint"
            : " Deposit"}
        </div>
        <div
          className="text-2xl cursor-pointer"
          onClick={(e) => {
            e.stopPropagation();
            onClose();
          }}
        >
          &times;
        </div>
      </div>
      {/* depositing */}
      <div className="dark:border-b-dark-500 border-b border-blue-300 pb-3 dark:text-white">
        <div className="mb-4 text-sm">
          {transactionData.to.item.token.geckoId.toUpperCase() == "DOLLY"
            ? "Minting"
            : "Depositing"}
        </div>
        {transactionData.from.items.map(({ token, amount }) => (
          <div className="grid grid-cols-2 gap-3 text-sm" key={token.symbol}>
            <div className="col-span-1 flex items-center mb-3">
              <img src={token.icon} className="h-8" alt="icon" />
              <span className="ml-2">{token.symbol}</span>
            </div>
            <div className="col-span-1 flex items-center">
              {commify(formatBNToString(amount, token.decimals))}
            </div>
          </div>
        ))}
        <div className="grid grid-cols-2 gap-3 pt-3 text-sm">
          <div className="col-span-1 font-bold">total</div>
          <div className="col-span-1 font-bold text-blue-400">
            {commify(formatBNToString(transactionData.from.totalAmount, 18))}
          </div>
        </div>
      </div>

      {/* receiving */}
      <div className="dark:border-b-dark-500 border-b border-blue-300 py-5 pb-6 dark:text-white">
        <div className="mb-4 text-sm">Receiving</div>
        <div
          className="grid grid-cols-2 gap-3 items-center text-sm"
          key={transactionData.to.item.token.symbol}
        >
          <div className="col-span-1 flex items-center">
            <img
              src={transactionData.to.item.token.icon}
              className="h-8"
              alt="icon"
            />
            <span className="ml-2">
              {transactionData.to.item.token.geckoId.toUpperCase() == "DOLLY"
                ? transactionData.to.item.token.geckoId.toUpperCase()
                : `${transactionData.to.item.token.geckoId.toUpperCase()}-LPs`}
            </span>
          </div>
          <div className="col-span-1 font-bold text-blue-400">
            {commify(
              formatBNToString(
                transactionData.to.item.amount,
                transactionData.to.item.token.decimals
              )
            )}
          </div>
        </div>
      </div>

      {/* detail */}
      <div className="dark:border-b-dark-500 border-b border-blue-400 py-5 dark:text-white">
        <div className="grid grid-cols-2 gap-3 gap-y-3 mt-1 text-sm">
          {transactionData.to.item.token.geckoId.toUpperCase() ==
          "DOLLY" ? null : (
            <>
              <div className="col-span-1">Share of Pool</div>
              <div className="col-span-1">
                {formatBNToPercentString(transactionData.shareOfPool, 18)}
              </div>
            </>
          )}

          <div className="col-span-1">Gas</div>
          <div className="col-span-1">
            {formatGasToString(
              { gasStandard, gasFast, gasInstant },
              gasPriceSelected,
              gasCustom
            )}{" "}
            GWEI
          </div>
          <div className="col-span-1">Max Slippage</div>
          <div className="col-span-1">
            {formatSlippageToString(slippageSelected, slippageCustom)}%
          </div>
          <div className="col-span-1">Deadline</div>
          <div className="col-span-1">{deadline} minutes</div>
          <div className="col-span-2">
            {isHighPriceImpactTxn && (
              <HighPriceImpactConfirmation
                checked={hasConfirmedHighPriceImpact}
                onCheck={() =>
                  setHasConfirmedHighPriceImpact((prevState) => !prevState)
                }
              />
            )}
          </div>
        </div>
      </div>
      {/* footer */}
      <div className="flex justify-between mt-4 text-sm py-2">
        <Button
          className="rounded-md bg-blue-400 text-white px-4 py-2 focus:outline-none"
          onClick={onConfirm}
          kind="primary"
          size="large"
          disabled={isHighPriceImpactTxn}
        >
          Confirm
        </Button>
        <Button
          className="rounded-sm bg-gray-200 rounded-md px-4 py-2 focus:outline-none dark:bg-dark-600"
          onClick={onClose}
          kind="secondary"
          size="large"
        >
          Cancel
        </Button>
      </div>
    </>
  );
}

export default ReviewDeposit;
