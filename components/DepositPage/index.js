import { PoolDataType, UserShareType } from "../../hooks/usePoolData";
import React, {
  ReactElement,
  useState,
  Suspense,
  useCallback,
  useEffect,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { POOL_FEE_PRECISION, POOLS_LIST } from "../../constants";

import ConfirmTransaction from "../ConfirmTransaction";
import DeadlineField from "../DeadlineField";
import GasField from "../GasField";
import { HistoricalPoolDataType } from "../../hooks/useHistoricalPoolData";
import InfiniteApprovalField from "../InfiniteApprovalField";
import LPStakingBanner from "../LPStakingBanner";
import Modal from "../Modal";
import MyActivityCard from "../MyActivityCard";
import MyShareCard from "../MyShareCard";
import { PayloadAction } from "@reduxjs/toolkit";
import PoolInfoCard from "../PoolInfoCard";
import ReviewDeposit from "../ReviewDeposit";
import SlippageField from "../SlippageField";
import { TokenInputDeposit } from "../TokenInput";
import classNames from "classnames";
import { formatBNToPercentString, formatBNToString } from "../../utils";
import { updatePoolAdvancedMode } from "../../state/user";
import { commify } from "@ethersproject/units";

import usePoller from "../../hooks/usePoller";

import fetchGasPrices from "../../utils/updateGasPrices";
import {
  BLOCK_TIME,
  NETWORK,
  STAKING_DOPPLE_LP_TOKENS_LIST,
} from "../../constants";
import { getContract } from "../../utils/index";
import LPTOKEN_ABI from "../../constants/abis/lpToken.abi.json";
import WhitePanel from "../../components/layouts/WhitePanel";
import Image from "next/image";
import { useActiveWeb3React } from "../../hooks";
import { BigNumber } from "@ethersproject/bignumber";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";

import {
  useFairLaunchContract,
  useTokenContract,
  useSwapContract,
} from "../../hooks/useContract";

/* eslint-enable @typescript-eslint/no-explicit-any */
const DepositPage = (props) => {
  const {
    title,
    tokens,
    exceedsWallet,
    poolData,
    historicalPoolData,
    myShareData,
    transactionData,
    onChangeTokenInputValue,
    onConfirmTransaction,
  } = props;

  const [currentModal, setCurrentModal] = useState(null);
  const [apy, setApy] = useState(null);
  const [totalValueLock, setTotalValueLock] = useState(null);
  const { account, chainId, error: web3Error, library } = useActiveWeb3React();

  const swapContract = useSwapContract(title);

  const dispatch = useDispatch();
  const { userPoolAdvancedMode: advanced, apiData } = useSelector(
    (state) => state.user
  );
  const validDepositAmount = transactionData.to.totalAmount.gt(0);
  const fairLaunchContract = useFairLaunchContract();

  const fetchAndUpdateGasPrice = useCallback(() => {
    fetchGasPrices(dispatch);
  }, [dispatch]);
  usePoller(fetchAndUpdateGasPrice, BLOCK_TIME);

  async function getTotalValueLock() {
    const lpTokenContract = getContract(
      STAKING_DOPPLE_LP_TOKENS_LIST[title].addresses[chainId],
      LPTOKEN_ABI,
      library,
      account !== null && account !== void 0 ? account : undefined
    );
    const [userLpTokenBalance, totalLpTokenBalance] = await Promise.all([
      lpTokenContract?.balanceOf(account || AddressZero),
      lpTokenContract?.totalSupply(),
    ]);
    if (totalLpTokenBalance) {
      setTotalValueLock(formatBNToString(totalLpTokenBalance, 18));
    }
  }

  useEffect(() => {
    getTotalValueLock();
    async function calcApy() {
      if (apiData) {
        let _apy =
          apiData?.staking[
            STAKING_DOPPLE_LP_TOKENS_LIST[title].addresses[chainId]
          ].apy;

        if (_apy < Infinity && _apy > 0) {
          setApy(parseInt(_apy));
        }
      }
    }

    calcApy();
  }, [apy, totalValueLock, apiData]);

  return (
    <>
      <div className="container">
        <div className="w-20">
          <Link href="/Deposit">
            <a>
              <div className="text-blue-400 mt-3 ">
                <div className="flex items-center w-auto">
                  <Image
                    src="/images/icons/arrow-left.svg"
                    width="14"
                    height="14"
                  />
                  <div className="font-bold ml-1">Back</div>
                </div>
              </div>
            </a>
          </Link>
        </div>
        <div className="flex dark:text-white  mb-6 mt-3">
          <div className="text-3xl font-bold">Deposit</div>
          <div className="ml-2 rounded-xl bg-green-50 border border-green-300 text-blue-400 flex items-center justify-center px-3">
            No deposit fee
          </div>
        </div>
        <div className="grid grid-cols-2 gap-7">
          <div className="col-span-full sm:col-span-1 w-full">
            <div
              className={` rounded-3xl shadow-md p-6 lg:p-10 py-4 dark:bg-dark-700 bg-white`}
            >
              <div className="grid grid-cols-5 gap-3 ">
                {/* group 1 */}
                <div className="col-span-full">
                  <div className="dark:text-white text-2xl font-bold mb-6">
                    Add Liquidity
                  </div>
                  {tokens.map((token, index) => (
                    <div key={index}>
                      <TokenInputDeposit
                        {...token}
                        onChange={(value) =>
                          onChangeTokenInputValue(token.symbol, value)
                        }
                        deposit={true}
                        myShareData={myShareData}
                        setCurrentModal={setCurrentModal}
                        maxBalance={token.max}
                        poolName={title}
                      />
                    </div>
                  ))}
                  <TextBonus transactionData={transactionData} />
                  <TextDepositBenefit />
                  <ButtonDeposit
                    NETWORK={NETWORK}
                    chainId={chainId}
                    exceedsWallet={exceedsWallet}
                    setCurrentModal={setCurrentModal}
                  />
                  <TextFooter
                    poolData={poolData}
                    exceedsWallet={exceedsWallet}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-full sm:col-span-1 w-full">
            <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md mb-6">
              <MyShareCard
                data={myShareData}
                transactionData={transactionData}
                depositPage={true}
              />
              {/* <MyActivityCard historicalPoolData={historicalPoolData} /> */}
            </div>
            <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md mb-6">
              <CardSuggestStake
                apy={apy}
                stakingData={POOLS_LIST.find((x) => x.name == title)}
              />
            </div>
          </div>
        </div>
        <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
          {currentModal === "review" ? (
            <ReviewDeposit
              transactionData={transactionData}
              onConfirm={async () => {
                setCurrentModal("confirm");
                await onConfirmTransaction?.();
                setCurrentModal(null);
              }}
              onClose={() => setCurrentModal(null)}
            />
          ) : null}
          {currentModal === "confirm" ? <ConfirmTransaction /> : null}
        </Modal>
      </div>
    </>
  );
};

const ButtonDeposit = ({
  NETWORK,
  chainId,
  exceedsWallet,
  setCurrentModal,
}) => {
  return (
    <div className="flex justify-center">
      <div className="w-full text-center pt-5 ">
        <button
          className={
            " focus:outline-none w-full  text-white  py-3 rounded-lg shadow-sm " +
            (NETWORK === chainId
              ? !exceedsWallet
                ? " bg-gradient-primary "
                : "bg-gray-400"
              : "bg-red-400")
          }
          onClick={() => {
            setCurrentModal("review");
          }}
          disabled={exceedsWallet && NETWORK !== chainId}
        >
          {NETWORK === chainId ? "Deposit" : "Invalid network"}
        </button>
      </div>
    </div>
  );
};

const TextDepositBenefit = () => {
  return (
    <div className="transactionInfoItem ">
      <div className="slippage text-steel-300 text-sm ">
        <span>Dopple and LPs will receive</span>
        <span className="text-blue-400 mx-1">0.045%</span>
        <span>of trading fees.</span>
      </div>
    </div>
  );
};

const TextBonus = ({ transactionData }) => {
  return (
    <div className="transactionInfoItem grid-cols-2 grid ">
      {transactionData.priceImpact.gte(0) ? (
        <span className="bonus text-steel-300 font-bold">Bonus </span>
      ) : (
        <span className="slippage text-steel-300 font-bold">Price impact</span>
      )}
      <span
        className={
          "value font-bold  text-right pl-1 " +
          (transactionData.priceImpact.gte(0)
            ? "text-blue-400"
            : "text-red-400")
        }
      >
        {transactionData.priceImpact ? (
          formatBNToPercentString(transactionData.priceImpact, 18, 4)
        ) : (
          <Skeleton count={1} />
        )}
      </span>
    </div>
  );
};

const TextFooter = ({ poolData, exceedsWallet }) => {
  return (
    <div className="mt-4 mb-3 text-steel-300 text-sm">
      <div className={classNames("transactionInfoContainer", "show")}>
        <div className="transactionInfo text-center">
          {poolData?.keepApr && (
            <div className="transactionInfoItem grid-cols-2 grid text-right mb-2">
              <a
                href="https://docs.dopple.finance/faq#what-are-dopples-liquidity-provider-rewards"
                target="_blank"
                rel="noopener noreferrer"
              >
                <span>{`Keep APR:`}</span>
              </a>{" "}
              <span className="value text-blue-400 font-bold text-left pl-1">
                {formatBNToPercentString(poolData.keepApr, 18)}
              </span>
            </div>
          )}

          {exceedsWallet ? (
            <div className="error text-red-400 font-bold">
              Insufficient Fund
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

const CardSuggestStake = ({ apy, stakingData }) => {
  return (
    <>
      <div className="myShareCard dark:text-white">
        <div className="flex w-full justify-center relative">
          <div className="absolute -mt-6 hidden sm:block transform rotate-90 ">
            <Image
              src="/images/icons/deposit-arrow-next.svg"
              width="50"
              height="50"
            />
          </div>
        </div>
        <div className="p-6  lg:px-8 ">
          <div className="text-2xl font-bold">Stakes</div>
          <div className="flex py-6 pb-3 justify-between">
            <div className="flex items-center">
              <Image src={stakingData.icon} width="44" height="44" />

              {stakingData.title == "BTC LPs" ? (
                <div className="ml-2">
                  <div className="font-bold text-lg">{stakingData.title}</div>
                  <div className="text-blue-400 text-sm font-bold ">
                    <div>APR : Coming Soon...</div>
                  </div>
                </div>
              ) : (
                <div className="ml-2">
                  <div className="font-bold text-lg">{stakingData.title}</div>
                  <div className="text-blue-400 text-sm font-bold ">
                    {apy ? <div>APR {apy}%</div> : <Skeleton count={1} />}
                  </div>
                </div>
              )}
            </div>
            <div>
              <a href="/Stake">
                <button className="focus:outline-none w-full  text-white  py-3 px-8 rounded-lg shadow-sm  bg-gradient-primary ">
                  {stakingData.title == "BTC LPs"
                    ? "Coming Soon..."
                    : "Stake Now"}
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default DepositPage;
