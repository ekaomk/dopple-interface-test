import React, { ReactElement, useCallback, useState, useEffect } from "react";

import { BigNumber } from "@ethersproject/bignumber";
import Button from "../Button";
import ToolTip from "../ToolTip";
import classNames from "classnames";
import { formatBNToString } from "../../utils";
import { formatUnits } from "@ethersproject/units";
import Image from "next/image";
import { createPopper } from "@popperjs/core";

function SwapForm({
  tokens,
  selected,
  inputValue,
  isSwapFrom,
  onChangeSelected,
  onChangeAmount,
}) {
  const [selectedToken, setSelectedToken] = useState();

  // console.log(inputValue);
  useEffect(() => {
    if (!selected) return;
    setSelectedToken(tokens.find((t) => t.symbol === selected));
  });

  const setValueFromPercent = (percent) => {
    const token = tokens.find((t) => t.symbol === selected);
    if (token && onChangeAmount) {
      onChangeAmount(
        formatUnits(token.value.mul(percent).div(100), token.decimals, false)
      );
    }
  };

  // console.log(selectedToken);

  return (
    <div className="swapForm dark:text-white">
      <div className="head">
        <div className="pl-1 text-sm"></div>
        {isSwapFrom ? (
          <div className="flex justify-between">
            <div className="pl-1 text-sm font-bold">From</div>
            <div className="pl-1 text-sm">
              Balance :{" "}
              <span className="text-blue-400 font-bold">
                {selectedToken &&
                  formatBNToString(
                    selectedToken.value,
                    selectedToken.decimals,
                    4
                  )}
              </span>
            </div>
          </div>
        ) : (
          <div className="flex justify-between">
            <div className="pl-1 text-sm font-bold">To</div>
            <div className="pl-1 text-sm">
              Balance :{" "}
              <span className="text-blue-400 font-bold">
                {selectedToken &&
                  formatBNToString(
                    selectedToken.value,
                    selectedToken.decimals,
                    4
                  )}
              </span>
            </div>
          </div>
        )}

        <div className="pl-1 text-sm"></div>

        <div className="inputField">
          {/*  */}
          {/*  */}
          <div className="text-right">
            {/* {selected && <TokenBalance key={selected.address} {...selected} />} */}
            <div className="flex pt-1">
              <div className="w-full -mt-6">
                <Dropdown
                  selectedToken={tokens.find((t) => t.symbol === selected)}
                  tokens={tokens}
                  onChangeSelected={onChangeSelected}
                  isSwapFrom={isSwapFrom}
                />
                <input
                  autoComplete="off"
                  autoCorrect="off"
                  type="text"
                  className={classNames(
                    { hasMaxButton: isSwapFrom },
                    "focus:outline-none dark:bg-dark-900 text-xl py-2 w-full bg-steel-100 shadow-sm rounded-md p-2 pl-32"
                  )}
                  value={inputValue}
                  placeholder="0.0"
                  spellCheck="false"
                  onChange={(e) => onChangeAmount?.(e.target.value, true)}
                  onFocus={(e) => {
                    if (isSwapFrom) {
                      e.target.select();
                    }
                  }}
                  readOnly={!isSwapFrom}
                  type="text"
                />
              </div>
            </div>
          </div>
        </div>

        {isSwapFrom ? (
          <div className="float-right flex mt-2 text-sm font-bold text-steel-300">
            <button
              className="dark:border-dark-700 dark:text-white dark:bg-dark-600 focus:outline-none border border-steel-200 p-2 px-4 border-r-0 rounded-l-md font-bold"
              onClick={() => setValueFromPercent(25)}
            >
              25%
            </button>
            <button
              className="dark:border-dark-700 dark:text-white dark:bg-dark-600 focus:outline-none border border-steel-200 p-2 px-4 border-r-0 font-bold"
              onClick={() => setValueFromPercent(50)}
            >
              50%
            </button>
            <button
              className="dark:border-dark-700 dark:text-white dark:bg-dark-600 focus:outline-none border border-steel-200 p-2 px-4 border-r-0 font-bold"
              onClick={() => setValueFromPercent(75)}
            >
              75%
            </button>
            <button
              className="dark:border-dark-700 dark:bg-dark-600 focus:outline-none border border-steel-200 text-blue-400 p-2 px-4 rounded-r-md font-bold"
              onClick={() => setValueFromPercent(100)}
            >
              Max
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
}

const Dropdown = ({ selectedToken, tokens, onChangeSelected, isSwapFrom }) => {
  // dropdown props
  const [dropdownPopoverShow, setDropdownPopoverShow] = React.useState(false);
  const btnDropdownRef = React.createRef();
  const popoverDropdownRef = React.createRef();
  const openDropdownPopover = () => {
    createPopper(btnDropdownRef.current, popoverDropdownRef.current, {
      placement: "bottom-start",
    });
    setDropdownPopoverShow(true);
  };
  const closeDropdownPopover = () => {
    setDropdownPopoverShow(false);
  };

  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full sm:w-6/12 md:w-4/12 px-4">
          <div className="relative inline-flex align-middle w-full">
            <div
              className={`width-119 absolute mt-3 -ml-4 w-28  font-bold text-sm text-steel-400 px-4 ${
                selectedToken ? " py-2 " : " py-4 "
              }mt-1 rounded shadow hover:shadow-lg outline-none focus:outline-none  bg-white active:bg-gray-600 ease-linear transition-all duration-150 cursor-pointer dark:bg-dark-600 dark:border-0 dark:text-white`}
              ref={btnDropdownRef}
              onClick={() => {
                dropdownPopoverShow
                  ? closeDropdownPopover()
                  : openDropdownPopover();
              }}
            >
              {selectedToken ? (
                <div className="flex items-center">
                  <Image src={selectedToken.icon} width="26px" height="26px" />
                  <div className="font-bold ml-2 text-sm">
                    {selectedToken.symbol}
                  </div>
                </div>
              ) : null}
            </div>
            <div
              ref={popoverDropdownRef}
              className={
                (dropdownPopoverShow ? "block " : "hidden ") + "z-50 pt-1"
              }
            >
              <div
                className={
                  "dark:bg-dark-600 bg-white text-base  float-left py-2 list-none text-left rounded shadow-lg w-28"
                }
              >
                {tokens.map(({ symbol, value, icon, name, decimals }, i) => {
                  const formattedShortBalance =
                    value && formatBNToString(0, decimals);
                  const formattedLongBalance =
                    value && formatBNToString(value, decimals);
                  return (
                    <div key={symbol}>
                      <DropdownItem
                        symbol={symbol}
                        icon={icon}
                        handleOnClick={() => {
                          closeDropdownPopover();
                          onChangeSelected(symbol);
                        }}
                        {...(isSwapFrom ? (
                          <span className="tokenValue">
                            <ToolTip content={formattedLongBalance}>
                              {formattedShortBalance}
                            </ToolTip>
                          </span>
                        ) : null)}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const DropdownItem = ({ symbol, icon, handleOnClick }) => {
  return (
    <>
      <a
        href="#"
        className="swap-dropdown-item dark:bg-dark-600 dark:text-white hover:bg-gray-100 text-sm py-2 px-4 font-normal text-steel-400 block w-full whitespace-no-wrap bg-transparent text-white"
        onClick={() => handleOnClick()}
      >
        <div className="flex items-center">
          <Image src={icon} width="22px" height="22px" />
          <div className="font-bold ml-2 text-sm ">{symbol}</div>
        </div>
      </a>
    </>
  );
};

export default SwapForm;
