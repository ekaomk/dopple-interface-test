import { PoolDataType, UserShareType } from "../../hooks/usePoolData";
import React, {
  ReactElement,
  useState,
  Suspense,
  useCallback,
  useEffect,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { POOL_FEE_PRECISION } from "../../constants";
import { AppDispatch } from "../../state";
import { AppState } from "../../state";
import { BigNumber } from "@ethersproject/bignumber";
import ConfirmTransaction from "../ConfirmTransaction";
import DeadlineField from "../DeadlineField";
import GasField from "../GasField";
import { HistoricalPoolDataType } from "../../hooks/useHistoricalPoolData";
import InfiniteApprovalField from "../InfiniteApprovalField";
import Modal from "../Modal";
import MyActivityCard from "../MyActivityCard";
import MyShareCard from "../MyShareCard";
import NoShareContent from "../NoShareContent";
import { PayloadAction } from "@reduxjs/toolkit";
import PoolInfoCard from "../PoolInfoCard";
import RadioButton from "../RadioButton";
import ReviewWithdraw from "../ReviewWithdraw";
import SlippageField from "../SlippageField";
import { TokenInputWithdraw } from "../TokenInput";
import { WithdrawFormState } from "../../hooks/useWithdrawFormState";
import { Zero } from "@ethersproject/constants";
import classNames from "classnames";
import { formatBNToPercentString } from "../../utils";
import { updatePoolAdvancedMode } from "../../state/user";
import LPTOKEN_ABI from "../../constants/abis/lpToken.abi.json";

import usePoller from "../../hooks/usePoller";
import fetchGasPrices from "../../utils/updateGasPrices";
import {
  BLOCK_TIME,
  NETWORK,
  ACTIVE_POOL_NAME,
  POOLS_LIST,
  STAKING_DOPPLE_LP_TOKENS_LIST,
} from "../../constants";
import { getContract } from "../../utils/index";
import WhitePanel from "../../components/layouts/WhitePanel";
import { useActiveWeb3React } from "../../hooks";
import { formatBNToString, shiftBNDecimals } from "../../utils";
import { useApproveForWithdraw } from "../../hooks/useApproveAndWithdraw";
import {
  useSwapContract,
  useLPTokenContract,
  useFairLaunchContract,
} from "../../hooks/useContract";
import Skeleton from "react-loading-skeleton";
import Image from "next/image";
import { AddressZero } from "@ethersproject/constants";
import { createPopper } from "@popperjs/core";
import Link from "next/link";

const WithdrawPage = (props) => {
  const {
    title,
    tokensData,
    poolData,
    historicalPoolData,
    myShareData,
    onFormChange,
    formStateData,
    reviewData,
    onConfirmTransaction,
  } = props;
  const dispatch = useDispatch();
  const { userPoolAdvancedMode: advanced, apiData } = useSelector(
    (state) => state.user
  );
  const { gasPriceSelected } = useSelector((state) => state.user);
  const [currentModal, setCurrentModal] = useState(null);
  const { account, chainId, library } = useActiveWeb3React();
  const [allowance, setAllowance] = useState(BigNumber.from("0"));
  const [apy, setApy] = useState(null);
  const [totalValueLock, setTotalValueLock] = useState(null);
  const [radioActive, setRadioActive] = useState(null);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  const poolName = ACTIVE_POOL_NAME;
  const approveForWithdraw = useApproveForWithdraw(poolName);
  const swapContract = useSwapContract(poolName);
  const lpTokenContract = useLPTokenContract(poolName);
  const fairLaunchContract = useFairLaunchContract();

  const updateAllowance = async () => {
    if (lpTokenContract && swapContract) {
      const existingAllowance = await lpTokenContract.allowance(
        account,
        swapContract.address
      );
      setAllowance(existingAllowance);
    }
  };

  async function getTotalValueLock() {
    const lpTokenContract = getContract(
      STAKING_DOPPLE_LP_TOKENS_LIST[title].addresses[chainId],
      LPTOKEN_ABI,
      library,
      account !== null && account !== void 0 ? account : undefined
    );
    const [userLpTokenBalance, totalLpTokenBalance] = await Promise.all([
      lpTokenContract?.balanceOf(account || AddressZero),
      lpTokenContract?.totalSupply(),
    ]);
    if (totalLpTokenBalance) {
      setTotalValueLock(formatBNToString(totalLpTokenBalance, 18));
    }
  }

  useEffect(() => {
    getTotalValueLock();

    async function calcApy() {
      let _apy =
        apiData?.staking[
          STAKING_DOPPLE_LP_TOKENS_LIST[title].addresses[chainId]
        ].apy;

      if (_apy < Infinity && _apy > 0) {
        setApy(parseInt(_apy));
      }
    }

    calcApy();

    if (onFormChange && radioActive) {
      onFormChange({
        fieldName: "withdrawType",
        value: radioActive,
      });
    }

    (async () => {
      await updateAllowance();
    })();
  }, [apy, dopPrice, totalValueLock, radioActive]);

  const onSubmit = () => {
    setCurrentModal("review");
  };
  const approve = async () => {
    if (NETWORK != chainId) return;
    setCurrentModal("confirm");
    await approveForWithdraw();
    await updateAllowance();
    setCurrentModal(null);
  };
  const noShare = !myShareData || myShareData.lpTokenBalance.eq(Zero);

  const fetchAndUpdateGasPrice = useCallback(() => {
    fetchGasPrices(dispatch);
  }, [dispatch]);
  usePoller(fetchAndUpdateGasPrice, BLOCK_TIME);
  return (
    <>
      <div className="container">
        <div className="w-20">
          <Link href="/Withdraw">
            <a>
              <div className="text-blue-400 mt-3 ">
                <div className="flex items-center w-auto">
                  <Image
                    src="/images/icons/arrow-left.svg"
                    width="14"
                    height="14"
                  />
                  <div className="font-bold ml-1">Back</div>
                </div>
              </div>
            </a>
          </Link>
        </div>
        <div className="dark:text-white text-3xl font-bold mb-6 mt-3">
          Withdraw
        </div>
        <div className="grid grid-cols-2 gap-7">
          {/* left */}
          <div className="col-span-full sm:col-span-1 w-full">
            <div
              className={`dark:bg-dark-700 dark:text-white rounded-3xl shadow-md p-6 lg:p-10 py-4  bg-white`}
            >
              <div className="gap-3 ">
                {/* group 1 */}
                <div className="col-span-full">
                  <div className="text-2xl font-bold mb-6">
                    Remove Liquidity
                  </div>

                  {tokensData.map((token, index) => {
                    const myshareDataTokenValue = myShareData?.tokens.find(
                      (t) => t.symbol === token.symbol
                    ).value;
                    const tokenValueSub = myshareDataTokenValue?.sub(
                      BigNumber.from(10).pow(BigNumber.from(token.decimals))
                    );

                    const tokenValueResult = tokenValueSub?.lt(0)
                      ? Zero
                      : tokenValueSub;

                    return (
                      <div key={index}>
                        <TokenInputWithdraw
                          {...token}
                          max={
                            myShareData &&
                            formatBNToString(tokenValueResult, token.decimals)
                          }
                          myShareData={myShareData}
                          onChange={(value) =>
                            onFormChange({
                              fieldName: "tokenInputs",
                              value: value,
                              tokenSymbol: token.symbol,
                            })
                          }
                          deposit={false}
                          radioActive={radioActive}
                          setRadioActive={setRadioActive}
                        />
                      </div>
                    );
                  })}
                </div>
                <div className="mb-7">
                  <div
                    className={`dark:bg-dark-500 focus:outline-none w-full text-steel-300 rounded-lg radio-outline py-3 text-center btn cursor-pointer ${
                      radioActive == "ALL" ? "active" : ""
                    }`}
                    onClick={() => setRadioActive("ALL")}
                  >
                    Combo
                  </div>
                </div>

                <TextBonus transactionData={reviewData} />
                <TextWithdrawFee poolData={poolData} />

                <ButtonWithdraw
                  NETWORK={NETWORK}
                  chainId={chainId}
                  formStateData={formStateData}
                  onSubmit={onSubmit}
                  allowance={allowance}
                  approve={approve}
                />
                <div
                  className={classNames(
                    "transactionInfoContainer mt-4 mb-3 text-steel-300",
                    "show"
                  )}
                >
                  <div className="transactionInfo text-center text-sm">
                    <span className="text-red-400">
                      {formStateData.error ? formStateData.error.message : ""}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            {/* end panel */}
          </div>
          {/* right */}
          <div className="col-span-full sm:col-span-1 w-full">
            <div className="dark:bg-dark-700 dark:text-white bg-white rounded-3xl shadow-md mb-6">
              <MyShareCard
                data={myShareData}
                transactionData={reviewData}
                depositPage={false}
                pool_name={title}
              />

              {/* <MyActivityCard historicalPoolData={historicalPoolData} /> */}
            </div>
            <div className="dark:bg-dark-700 dark:text-white bg-white rounded-3xl shadow-md mb-6">
              <CardSuggestStake
                apy={apy}
                depositPage={false}
                stakingData={POOLS_LIST.find((x) => x.name == title)}
              />
            </div>
          </div>
        </div>
        <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
          {currentModal === "review" ? (
            <ReviewWithdraw
              data={reviewData}
              gas={gasPriceSelected}
              onConfirm={async () => {
                setCurrentModal("confirm");
                await onConfirmTransaction?.();
                setCurrentModal(null);
              }}
              onClose={() => setCurrentModal(null)}
            />
          ) : null}
          {currentModal === "confirm" ? <ConfirmTransaction /> : null}
        </Modal>
      </div>
    </>
  );
};

const TextWithdrawFee = () => {
  const [popoverShow, setPopoverShow] = React.useState(false);
  const btnRef = React.createRef();
  const popoverRef = React.createRef();
  const openTooltip = () => {
    createPopper(btnRef.current, popoverRef.current, {
      placement: "top",
    });
    setPopoverShow(true);
  };
  const closeTooltip = () => {
    setPopoverShow(false);
  };

  return (
    <div className="transactionInfoItem grid-cols-5 sm:grid-cols-2 grid ">
      <div className="slippage text-steel-300 text-sm dark:text-white ">
        <span>Withdrawal Fees: 0-0.1% </span>
        <span
          className=""
          onMouseEnter={openTooltip}
          onMouseLeave={closeTooltip}
          ref={btnRef}
        >
          (&#63;)
        </span>
      </div>
      <div
        className={(popoverShow ? "" : "hidden ") + "font-base"}
        ref={popoverRef}
      >
        <div>
          <div className="relative mx-2">
            <div className="bg-gray-500 text-white text-xs rounded py-2 px-4 right-0 bottom-full text-center font-base">
              The fee diminishes to 0% over the course of a month <br />
              after you first provide liquidity.
              <svg
                className="absolute text-gray-500 h-2 w-full left-0 top-full"
                x="0px"
                y="0px"
                viewBox="0 0 255 255"
              >
                <polygon
                  className="fill-current"
                  points="0,0 127.5,127.5 255,0"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const TextBonus = ({ transactionData }) => {
  return (
    <div className="transactionInfoItem grid-cols-2 grid ">
      {transactionData.priceImpact.gte(0) ? (
        <span className="dark:text-white bonus text-steel-300 font-bold">
          Bonus{" "}
        </span>
      ) : (
        <span className="dark:text-white slippage text-steel-300 font-bold">
          Price impact
        </span>
      )}
      <span
        className={
          "value font-bold  text-right pl-1 " +
          (transactionData.priceImpact.gte(0)
            ? "text-blue-400"
            : "text-red-400")
        }
      >
        {transactionData.priceImpact ? (
          formatBNToPercentString(transactionData.priceImpact, 18, 4)
        ) : (
          <Skeleton count={1} />
        )}
      </span>
    </div>
  );
};

const ButtonWithdraw = ({
  NETWORK,
  chainId,
  formStateData,
  onSubmit,
  allowance,
  approve,
}) => {
  return (
    <div className="flex justify-center">
      <div className="w-full text-center pt-5 ">
        <button
          className={` focus:outline-none w-full  text-white  py-3 rounded-lg shadow-sm ${
            NETWORK === chainId ? " bg-gradient-primary " : "bg-red-400"
          }`}
          disabled={
            !allowance.eq(0) &&
            (!!formStateData.error ||
              formStateData.lpTokenAmountToSpend.isZero())
          }
          onClick={allowance.eq(0) ? approve : onSubmit}
        >
          {NETWORK === chainId
            ? allowance.eq(0)
              ? "Approve"
              : "Withdraw"
            : "Invalid network"}
        </button>
      </div>
    </div>
  );
};

const CardSuggestStake = ({ apy, depositPage, stakingData }) => {
  return (
    <>
      <div className="myShareCard ">
        {depositPage && (
          <div className="flex w-full justify-center relative">
            <div className="absolute -mt-6 hidden sm:block transform rotate-90 ">
              <Image
                src="/images/icons/deposit-arrow-next.svg"
                width="50"
                height="50"
              />
            </div>
          </div>
        )}

        <div className="p-6  lg:px-8 ">
          <div className="text-2xl font-bold">
            {!depositPage && "Recomended"} Stakes
          </div>
          <div className="flex py-6 pb-3 justify-between">
            <div className="flex items-center">
              <Image src={stakingData.icon} width="44" height="44" />

              {stakingData.title == "BTC LPs" ? (
                <div className="ml-2">
                  <div className="font-bold text-lg">{stakingData.title}</div>
                  <div className="text-blue-400 text-sm font-bold ">
                    <div>APR : Coming Soon...</div>
                  </div>
                </div>
              ) : (
                <div className="ml-2">
                  <div className="font-bold text-lg">{stakingData.title} </div>
                  <div className="text-blue-400 text-sm font-bold ">
                    {apy ? <div>APR {apy}%</div> : <Skeleton count={1} />}
                  </div>
                </div>
              )}
            </div>
            <div>
              <a href="/Stake">
                <button className="focus:outline-none w-full  text-white  py-3 px-8 rounded-lg shadow-sm  bg-gradient-primary ">
                  Stake Now
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default WithdrawPage;
