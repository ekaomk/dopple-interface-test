import React, { ReactElement } from "react"
import { updateGasPriceCustom, updateGasPriceSelected } from "../../state/user"
import { useDispatch, useSelector } from "react-redux"

import { AppDispatch } from "../../state"
import { AppState } from "../../state/index"
import { GasPrices } from "../../state/user"
import { PayloadAction } from "@reduxjs/toolkit"
import classNames from "classnames"

export default function GasField() {
    const dispatch = useDispatch()
    const { gasCustom, gasPriceSelected } = useSelector(
        (state) => state.user,
    )
    const { gasStandard, gasFast, gasInstant } = useSelector(
        (state) => state.application,
    )
    return (
        <div className="gasField">
            <div className="options">
                <div className="label">gas:</div>
                {[GasPrices.Standard, GasPrices.Fast, GasPrices.Instant].map(
                    (gasPriceConst) => {
                        let priceValue
                        let text
                        if (gasPriceConst === GasPrices.Standard) {
                            priceValue = gasStandard
                            text = "standard"
                        } else if (gasPriceConst === GasPrices.Fast) {
                            priceValue = gasFast
                            text = "fast"
                        } else {
                            priceValue = gasInstant
                            text = "instant"
                        }

                        return (
                            <button
                                key={gasPriceConst}
                                className={classNames({
                                    selected: gasPriceSelected === gasPriceConst,
                                })}
                                onClick={() => {
                                    dispatch(updateGasPriceSelected(gasPriceConst))

                                }}
                            >
                                <div>
                                    <div>{priceValue}</div>
                                    <div>{text}</div>
                                </div>
                            </button>
                        )
                    },
                )}
                <input
                    type="text"
                    className={classNames({
                        selected: gasPriceSelected === GasPrices.Custom,
                    })}
                    value={gasCustom?.valueRaw}
                    onChange={(e) => {
                        const value = e.target.value
                        if (value && !isNaN(+value)) {
                            dispatch(updateGasPriceCustom(value))
                            if (gasPriceSelected !== GasPrices.Custom) {
                                dispatch(updateGasPriceSelected(GasPrices.Custom))
                            }
                        } else {
                            dispatch(updateGasPriceSelected(GasPrices.Fast))
                        }
                    }}
                />
            </div>
        </div>
    )
}
