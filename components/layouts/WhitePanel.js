export default function WhitePanel({ children, className }) {
  return (
    <>
      <div
        className={`${
          className ?? ""
        } dark:bg-dark-700 bg-white rounded-3xl shadow-md p-6 lg:p-12 mb-6`}
      >
        {children}
      </div>
    </>
  );
}
