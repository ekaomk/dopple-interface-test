const items = [
  {
    title: "",
    cardTitle: "",
    cardSubtitle: "Q1 - 25 March 2021",
    cardDetailedText: [
      "✔ Launch of Dopple Finance",
      "✔ Fair Launch of Dopple Token",
      "✔ Staking Pools for BUSD. USDT, DAI, USDC",
      "✔ Staking Pool for DOP",
    ],
  },

  {
    title: "",
    cardTitle: "",
    cardSubtitle: "Q2 2021",
    cardDetailedText: [
      "✔ Launch of UST pool",
      "✔ List on Coingecko & CoinMarketCap",
      "✔ Asset-Backed Stablecoin $DOLLY",
      "✔ UI Updates & Dark Mode",
      "✔ Smart Contract Audit by CertiK",
      "✔ Synthetic Stock Market - Twindex.com - The first Synthetic Stock Exchange on BSC",
      "✔ CEX Listings",
      "- Partner with lending platforms to use DOP/DOLLY as collateral",
      "- Provide ability to add liquidity to pools directly on our platform",
      "- Integrations with liquidity aggregators",
    ],
  },

  {
    title: "",
    cardTitle: "",
    cardSubtitle: "Q3 2021",
    cardDetailedText: [
      "- Auto-Compounding",
      "- Launching additional pools",
      "- User Dashboard",
      "- Partner with protocols to adopt DOLLY",
      "- Add support for additional web and mobile wallets",
      "- Additional Audits",
    ],
  },
  {
    title: "",
    cardTitle: "",
    cardSubtitle: "Q4 2021",
    cardDetailedText: [
      "- Hyper-Chain on Ethereum fork network",

      "- Dopple Wallet",
    ],
  },
];

export default items;
