import React from "react";
import { Chrono } from "react-chrono";
import data from "./data";

export default function Roadmap() {
  return (
    <div className="Roadmap ">
      <div className="dark:text-white text-center font-bold text-4xl text-steel-400 pb-20 pt-30">
        Roadmap
      </div>
      <div className="hidden sm:block">
        <Chrono
          items={data}
          mode="VERTICAL_ALTERNATING"
          allowDynamicUpdate
          hideControls
          scrollable={{ scrollbar: false }}
          theme={{
            primary: "#19c6bc",
            secondary: "#d5e7f6",
            cardBgColor: "white",
          }}
        />
      </div>

      <div className="block sm:hidden">
        <Chrono
          items={data}
          mode="VERTICAL"
          allowDynamicUpdate
          hideControls
          scrollable={{ scrollbar: false }}
          theme={{
            primary: "#19c6bc",
            secondary: "#d5e7f6",
            cardBgColor: "white",
          }}
        />
      </div>
    </div>
  );
}
