import { Interface } from "@ethersproject/abi";
import { getWeb3NoAccount } from "./web3";
import MULTICALL_ABI from "../constants/abis/mkrMulticallAbi.json";

const MULTICALL_ADDRESSES = {
  56: "0x8107467C23D14ed4B6a6ED98F31fdC002ba8d512",
  97: "0xae11C5B5f29A6a25e955F0CB8ddCc416f522AF5C",
};

const mkrMulticall = async (chainId, abi, calls) => {
  const web3 = getWeb3NoAccount();
  const multi = new web3.eth.Contract(
    MULTICALL_ABI,
    MULTICALL_ADDRESSES[chainId]
  );

  const itf = new Interface(abi);
  const calldata = calls.map((call) => [
    call.address.toLowerCase(),
    itf.encodeFunctionData(call.name, call.params),
  ]);

  const { returnData } = await multi.methods.aggregate(calldata).call();
  const res = returnData.map((call, i) =>
    itf.decodeFunctionResult(calls[i].name, call)
  );
  return res;
};

export default mkrMulticall;
