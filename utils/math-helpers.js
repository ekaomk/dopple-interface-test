import { bignumber } from "mathjs";
import { Unit } from "web3-utils";

export const weiToBigInt = (amount) => {
  if (amount) {
    return `${amount.split(".")[0]}.${
      amount.split(".")[1] ? amount.split(".")[1].slice(0, 18) : "0"
    }`;
  }
  return "0";
};

export const roundToSmaller = (amount, decimals) => {
  const bn = bignumber(amount);
  const [integer, decimalz] = bn.toFixed(128).split(".");
  let decimal = decimalz;
  if (decimal && decimal.length) {
    decimal = decimal.substr(0, decimals);
  } else {
    decimal = "0".repeat(decimals);
  }

  if (decimal.length < decimals) {
    decimal = decimal + "0".repeat(decimals - decimal.length);
  }

  if (decimal !== "") {
    return `${integer}.${decimal}`;
  }
  return `${integer}`;
};

export const fromWei = (amount, unit = "ether") => {
  let decimals = 0;
  switch (unit) {
    case "ether":
      decimals = 18;
      break;
    default:
      throw new Error("Unsupported unit (custom fromWei helper)");
  }

  return roundToSmaller(bignumber(amount || "0").div(10 ** decimals), decimals);
};

export const toWei = (amount, unit = "ether") => {
  let decimals = 0;
  switch (unit) {
    case "ether":
      decimals = 18;
      break;
    default:
      throw new Error("Unsupported unit (custom fromWei helper)");
  }

  return roundToSmaller(bignumber(amount || "0").mul(10 ** decimals), 0);
};

export const weiToFixed = (amount, decimals = 0) => {
  return roundToSmaller(bignumber(fromWei(amount, "ether")), decimals);
};

export const weiTo18 = (amount) => weiToFixed(amount, 18);

export const weiTo8 = (amount) => weiToFixed(amount, 8);

export const weiTo4 = (amount) => weiToFixed(amount, 4);

export const weiTo2 = (amount) => weiToFixed(amount, 2);
