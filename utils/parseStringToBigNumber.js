import { BigNumber } from "@ethersproject/bignumber";
import { formatUnits } from "@ethersproject/units";

/**
 * A curried function for representing user inputted number values.
 * Can be used to show errors in the UI, as well as safely interacting with the blockchain
 * @param {number} precision
 * @param {BigNumber} fallback
 * @return {function}
 */
export function numberInputStateCreator(precision, fallback) {
    /**
     * Transforms a user inputted string into a more verbose format including BigNumber representation
     * @param {string} inputValue
     * @return {NumberInputState}
     */
    return function createNumberInputState(inputValue) {
        if (BigNumber.isBigNumber(inputValue)) {
            return {
                isEmpty: false,
                isValid: true,
                precision,
                valueRaw: formatUnits(inputValue, precision),
                valueSafe: inputValue.toString(),
            };
        }
        else {
            const { value: valueSafe, isFallback } = parseStringToBigNumber(inputValue, precision, fallback);
            return {
                isEmpty: inputValue === "",
                isValid: !isFallback,
                precision,
                valueRaw: inputValue,
                valueSafe: valueSafe.toString(),
            };
        }
    };
}
import { TOKENS_MAP } from "../constants";
import { parseUnits } from "@ethersproject/units";
/**
 * Parses a user input string into a BigNumber.
 * Uses the native precision of the token if a tokenSymbol is provided
 * Defaults to a value of 0 if string cannot be parsed
 *
 * @param {string} valueRaw
 * @param {number} precision
 * @param {BigNumber} fallback
 * @return {Object} result
 * @return {BigNumber} result.value
 * @return {boolean} result.isFallback
 * }
 */
export default function parseStringToBigNumber(valueRaw, precision, fallback) {
    let valueSafe;
    let isFallback;
    try {
        // attempt to parse string. Use fallback value if library error is thrown
        valueSafe = parseUnits(valueRaw, precision);
        isFallback = false;
    }
    catch (_a) {
        valueSafe = fallback !== null && fallback !== void 0 ? fallback : BigNumber.from("0");
        isFallback = true;
    }
    return { value: valueSafe, isFallback };
}
/**
 * Parses a user input string into a BigNumber.
 * Uses the native precision of the token if a tokenSymbol is provided
 * Defaults to a value of 0 if string cannot be parsed
 * @param {string} value
 * @param {string} tokenSymbol
 * @return {Object} result
 * @return {BigNumber} result.value
 * @return {boolean} result.isFallback */
export function parseStringAndTokenToBigNumber(value, tokenSymbol) {
    var _a;
    return parseStringToBigNumber(value, tokenSymbol ? (_a = TOKENS_MAP[tokenSymbol]) === null || _a === void 0 ? void 0 : _a.decimals : 18, BigNumber.from("0"));
}