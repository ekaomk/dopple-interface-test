import dayjsInstance from 'dayjs'
import utc from 'dayjs/plugin/utc'

export function getFormattedTimeString() {
    const now = new Date()
    return now.toLocaleTimeString()
}

dayjsInstance.extend(utc)
export const dayjs = dayjsInstance
