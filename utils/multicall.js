import { AbiItem } from 'web3-utils'
import { Interface } from '@ethersproject/abi'
import { getWeb3NoAccount } from 'utils/web3'
import MultiCallAbi from 'config/abi/Multicall.json'
import { getMulticallAddress } from 'utils/addressHelpers'

const multicall = async (abi, calls) => {
  const web3 = getWeb3NoAccount()
  const multi = new web3.eth.Contract(MultiCallAbi, '0x1ee38d535d541c55c9dae27b12edf090c608e6fb')
  const itf = new Interface(abi)
  const blockNumber = await web3.eth.getBlockNumber()
  //const ethBalance = await multi.methods.getEthBalance('0x69F6829B0A62C34a844E9a0a123DD4b1822a7Bc5').call()
  calls.forEach(async (call2) => {
    const calldata = [call2].map((call) => [call.address.toLowerCase(), itf.encodeFunctionData(call.name, call.params)])
    const { returnData } = await multi.methods.aggregate(calldata).call()
    try {
      const res = returnData.map((call, i) => itf.decodeFunctionResult(calls[i].name, call))
    } catch (error) {
      console.log(error)
    }
  })
  // return []
  const calldata = calls.map((call) => [call.address.toLowerCase(), itf.encodeFunctionData(call.name, call.params)])
  const { returnData } = await multi.methods.aggregate(calldata).call()
  const res = returnData.map((call, i) => itf.decodeFunctionResult(calls[i].name, call))

  return res
}

export default multicall
