import retry from "async-retry";
import { updateGasPrices } from "../state/application";
// eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
const BSC_SCAN_KEY = process.env.NEXT_PUBLIC_REACT_BSC_SCAN_KEY;
const fetchGasPricePOA = () =>
  fetch("https://gasprice.poa.network/")
    .then(res => res.json())
    .then(body => {
      const { standard, fast, instant, health } = body;
      if (health) {
        return {
          gasStandard: Math.round(standard),
          gasFast: Math.round(fast),
          gasInstant: Math.round(instant)
        };
      }
      throw new Error("Unable to fetch gas price from POA Network");
    });

const fetchGasPriceGasNow = () => fetch("https://www.gasnow.org/api/v3/gas/price?utm_source=dopple")
  .then((res) => res.json())
  .then((body) => {
    const { code, data: { rapid, fast, standard }, } = body;
    if (code >= 200 && code < 300) {
      return {
        gasStandard: Math.round(standard / 1e9),
        gasFast: Math.round(fast / 1e9),
        gasInstant: Math.round(rapid / 1e9),
      };
    }
    throw new Error("Unable to fetch gas price from GasNow Network");
  });

const fetchGasPriceBSCScan = () =>
  fetch(`https://api.bscscan.com/api?module=proxy&action=eth_gasPrice&apikey=${BSC_SCAN_KEY}`)
    .then(res => res.json())
    .then(body => {
      const { result: gasHex } = body;
      const gasDecimal = parseInt(gasHex) / 1e9
      return {
        gasStandard: gasDecimal,
        gasFast: gasDecimal,
        gasInstant: gasDecimal
      };
    });

export default async function fetchGasPrices(dispatch) {
  await retry(
    () =>
      fetchGasPriceBSCScan().then(gasPrices => {
        // console.log(gasPrices)
        dispatch(updateGasPrices(gasPrices));
      }),
    {
      retries: 3
    }
  );
}
