import { AddressZero } from "@ethersproject/constants";
import { BigNumber } from "@ethersproject/bignumber";
import { Contract } from "@ethersproject/contracts";
import { Deadlines } from "../state/user";
import { formatUnits } from "@ethersproject/units";
import { getAddress } from "@ethersproject/address";
// returns the checksummed address if the address is valid, otherwise returns false
export function isAddress(value) {
  try {
    return getAddress(value);
  } catch (_a) {
    return false;
  }
}
// account is not optional
export function getSigner(library, account) {
  return library.getSigner(account).connectUnchecked();
}
// account is optional
export function getProviderOrSigner(library, account) {
  return account ? getSigner(library, account) : library;
}
// account is optional
export function getContract(address, ABI, library, account) {
  if (!isAddress(address) || address === AddressZero) {
    throw Error(`Invalid 'address' parameter '${address}'.`);
  }
  return new Contract(address, ABI, getProviderOrSigner(library, account));
}
export function formatBNToString(bn, nativePrecison, decimalPlaces = 4) {
  try {
    const fullPrecision = formatUnits(bn, nativePrecison);
    const decimalIdx = fullPrecision.indexOf(".");
    return decimalPlaces === undefined || decimalIdx === -1
      ? fullPrecision
      : fullPrecision.slice(
          0,
          decimalIdx + (decimalPlaces > 0 ? decimalPlaces + 1 : 0)
        );
  } catch (error) {
    return bn;
  }
}
export function formatBNToPercentString(bn, nativePrecison, decimalPlaces = 2) {
  return `${formatBNToString(bn, nativePrecison - 2, decimalPlaces)}%`;
}
export function shiftBNDecimals(bn, shiftAmount) {
  if (shiftAmount < 0) throw new Error("shiftAmount must be positive");
  return bn.mul(BigNumber.from(10).pow(shiftAmount));
}
export function calculateExchangeRate(
  amountFrom,
  tokenPrecisionFrom,
  amountTo,
  tokenPrecisionTo
) {
  return amountFrom.gt("0")
    ? amountTo
        .mul(BigNumber.from(10).pow(36 - tokenPrecisionTo)) // convert to standard 1e18 precision
        .div(amountFrom.mul(BigNumber.from(10).pow(18 - tokenPrecisionFrom)))
    : BigNumber.from("0");
}
export function formatDeadlineToNumber(deadlineSelected, deadlineCustom) {
  let deadline;
  if (deadlineSelected === Deadlines.Thirty) {
    deadline = 30;
  } else if (deadlineSelected === Deadlines.Custom) {
    deadline = +(deadlineCustom || 10);
  } else {
    deadline = 10;
  }
  return deadline;
}
