import {
  BLOCK_TIME,
  DOLLY,
  DOLLY_MINT_POOL_NAME,
  MINT_TOKEN_USER_INFO_MAP,
  POOLS_MAP,
  STAKING_DOPPLE_LP_TOKENS_LIST,
} from "../../constants";
import React, { useCallback, useEffect, useState } from "react";
import Image from "next/image";
import { useActiveWeb3React } from "../../hooks";
import {
  useFairLaunchContract,
  useLPTokenContract,
  useSwapContract,
  useTokenContract,
} from "../../hooks/useContract";
import { Zero, MaxUint256 } from "@ethersproject/constants";
import { BUSD, USDT, DOPPLE } from "../../constants";
import { formatBNToString, getContract } from "../../utils/index";
import Skeleton from "react-loading-skeleton";
import usePoolData from "../../hooks/usePoolData";
import useWithdrawFormStateDolly from "../../hooks/useWithdrawFormStateDolly";
import { useApproveAndWithdraw } from "../../hooks/useApproveAndWithdraw";
import { useDispatch, useSelector } from "react-redux";
import { formatSlippageToString } from "../../utils/slippage";
import { BigNumber } from "@ethersproject/bignumber";
import { commify, formatUnits, parseUnits } from "@ethersproject/units";
import { calculatePriceImpact } from "../../utils/priceImpact";
import fetchGasPrices from "../../utils/updateGasPrices";
import usePoller from "../../hooks/usePoller";
import LPTOKEN_ABI from "../../constants/abis/lpToken.abi.json";
import ReviewWithdraw from "../../components/ReviewWithdraw";
import Modal from "../../components/Modal";
import { TokenInputWithdraw } from "../../components/TokenInput";
import parseStringToBigNumber from "../../utils/parseStringToBigNumber";
import { TokenDollyDisplay } from "../../components/TokenDollyDisplay";
import ConfirmTransaction from "../../components/ConfirmTransaction";
import checkAndApproveTokenForTrade from "../../utils/checkAndApproveTokenForTrade";

const busdActiveLogo = "/images/icons/busd.svg";
const usdtActiveLogo = "/images/icons/usdt.svg";
const busdUsdtActiveLogo = "/images/icons/busd-usdt-color.svg";
const busdInActiveLogo = "/images/icons/busd-inactive.svg";
const usdtInActiveLogo = "/images/icons/usdt-inactive.svg";
const busdUsdtInActiveLogo = "/images/icons/busd-usdt-inactive.svg";
const arrowDownIcon = "/images/icons/arrow-down.svg";

export default function RepayComponent() {
  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
    library,
  } = useActiveWeb3React();

  const [withdrawAmount, setWithdrawAmount] = useState(BigNumber.from(0));
  const { gasPriceSelected } = useSelector((state) => state.user);

  const [poolData, userShareData] = usePoolData(DOLLY_MINT_POOL_NAME);
  const [withdrawFormState, updateWithdrawFormState] =
    useWithdrawFormStateDolly(DOLLY_MINT_POOL_NAME, withdrawAmount);

  const { slippageCustom, slippageSelected } = useSelector(
    (state) => state.user
  );
  const approveAndWithdraw = useApproveAndWithdraw(DOLLY_MINT_POOL_NAME);
  const swapContract = useSwapContract(DOLLY_MINT_POOL_NAME);
  const POOL = POOLS_MAP[DOLLY_MINT_POOL_NAME];
  // const lpTokenContract = useLPTokenContract(DOLLY.addresses);
  const doppleTokenContract = useTokenContract(DOPPLE);
  const busdTokenContract = useTokenContract(BUSD);
  const usdtTokenContract = useTokenContract(USDT);
  const dollyTokenContract = useTokenContract(DOLLY);

  const [estWithdrawBonus, setEstWithdrawBonus] = useState(Zero);
  const [apy, setApy] = useState(null);
  const [totalValueLock, setTotalValueLock] = useState(null);
  const [radioActive, setRadioActive] = useState(null);
  const [allowance, setAllowance] = useState(BigNumber.from("0"));
  const [currentModal, setCurrentModal] = useState(null);
  const dispatch = useDispatch();
  const [dollyInput, setDollyInput] = useState("");

  const fairLaunchContract = useFairLaunchContract();

  const { apiData } = useSelector((state) => state.user);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  useEffect(() => {
    // evaluate if a new withdraw will exceed the pool's per-user limit
    async function calculateWithdrawBonus() {
      if (
        swapContract == null ||
        userShareData == null ||
        poolData == null ||
        account == null
      ) {
        return;
      }
      const tokenInputSum = parseUnits(
        POOL.poolTokens
          .reduce(
            (sum, { symbol }) =>
              sum + (+withdrawFormState.tokenInputs[symbol].valueRaw || 0),
            0
          )
          .toFixed(18),
        18
      );
      let withdrawLPTokenAmount;
      if (poolData.totalLocked.gt(0) && tokenInputSum.gt(0)) {
        try {
          // FIX THIS LATER
          withdrawLPTokenAmount = await swapContract.calculateTokenAmount(
            account,
            POOL.poolTokens.map(
              ({ symbol }) =>
                withdrawFormState.tokenInputs[symbol].valueSafeXXXXXX
            ),
            false
          );
        } catch {
          withdrawLPTokenAmount = tokenInputSum;
        }
      } else {
        // when pool is empty, estimate the lptokens by just summing the input instead of calling contract
        withdrawLPTokenAmount = tokenInputSum;
      }

      // FIX THIS LATER
      setEstWithdrawBonus(
        calculatePriceImpact(
          withdrawLPTokenAmount,
          tokenInputSum,
          poolData.virtualPrice
        )
      );
    }
    calculateWithdrawBonus();
  }, [
    poolData,
    withdrawFormState,
    swapContract,
    userShareData,
    account,
    POOL.poolTokens,
  ]);

  useEffect(() => {
    if (updateWithdrawFormState && radioActive) {
      updateWithdrawFormState({
        fieldName: "withdrawType",
        value: radioActive,
      });
    }
  }, [dollyInput, radioActive]);

  async function onConfirmTransaction() {
    const { withdrawType, tokenInputs, lpTokenAmountToSpend } =
      withdrawFormState;

    await approveAndWithdraw({
      tokenFormState: tokenInputs,
      withdrawType,
      lpTokenAmountToSpend,
    });

    updateWithdrawFormState({ fieldName: "reset", value: "reset" });
  }

  const tokensData = React.useMemo(
    () =>
      POOL.poolTokens.map(({ name, symbol, icon, decimals }) => ({
        name,
        symbol,
        icon,
        decimals,
        inputValue: withdrawFormState.tokenInputs[symbol].valueRaw,
      })),
    [withdrawFormState, POOL.poolTokens]
  );

  const reviewWithdrawData = {
    withdraw: [],
    rates: [],
    slippage: formatSlippageToString(slippageSelected, slippageCustom),
    priceImpact: estWithdrawBonus,
  };
  POOL.poolTokens.forEach(({ name, decimals, icon, symbol }) => {
    if (BigNumber.from(withdrawFormState.tokenInputs[symbol].valueSafe).gt(0)) {
      reviewWithdrawData.withdraw.push({
        name,
        value: commify(
          formatUnits(withdrawFormState.tokenInputs[symbol].valueSafe, decimals)
        ),
        icon,
      });
    }
  });

  //
  const onSubmit = () => {
    setCurrentModal("review");
  };

  const fetchAndUpdateGasPrice = useCallback(() => {
    fetchGasPrices(dispatch);
  }, [dispatch]);
  usePoller(fetchAndUpdateGasPrice, BLOCK_TIME);

  //added dolly
  const tokenUserInfoList = React.useMemo(
    () =>
      MINT_TOKEN_USER_INFO_MAP.map(({ name, symbol, icon, decimals }) => ({
        name,
        symbol,
        icon,
        decimals,
        inputValue: "",
      })),
    []
  );
  const receiveList = [
    {
      name: "BUSD",
      value: "BUSD",
      iconActive: busdActiveLogo,
      iconInActive: busdInActiveLogo,
    },
    {
      name: "USDT",
      value: "USDT",
      iconActive: usdtActiveLogo,
      iconInActive: usdtInActiveLogo,
    },
    {
      name: "BUSD + USDT",
      value: "ALL",
      iconActive: busdUsdtActiveLogo,
      iconInActive: busdUsdtInActiveLogo,
    },
  ];

  const [fee, setFee] = useState("");
  const [exceedsWallet, setExceedsWallet] = useState(false);
  const [myWalletBalance, setMyWalletBalance] = useState({
    DOP: 0,
    BUSD: 0,
    USDT: 0,
    DOLLY: 0,
  });

  let fairLaunchDecimals;

  const loadWalletBalance = async () => {
    if (
      !doppleTokenContract ||
      !busdTokenContract ||
      !usdtTokenContract ||
      !dollyTokenContract ||
      !account
    )
      return;
    const totalDoppleInWallet = await doppleTokenContract.balanceOf(account);
    const totalBUSDInWallet = await busdTokenContract.balanceOf(account);
    const totalUSDTInWallet = await usdtTokenContract.balanceOf(account);
    const totalDollyInWallet = await dollyTokenContract.balanceOf(account);

    let objResult = myWalletBalance;
    objResult.DOP = formatBNToString(totalDoppleInWallet, fairLaunchDecimals);
    objResult.BUSD = formatBNToString(totalBUSDInWallet, fairLaunchDecimals);
    objResult.USDT = formatBNToString(totalUSDTInWallet, fairLaunchDecimals);

    const tokenValueSub = totalDollyInWallet?.sub(
      BigNumber.from(10).pow(BigNumber.from(18))
    );
    const tokenValueResult = tokenValueSub?.lt(0) ? Zero : tokenValueSub;

    objResult.DOLLY = formatBNToString(tokenValueResult, fairLaunchDecimals);
    // console.log(objResult.DOLLY);

    setMyWalletBalance(objResult);
    return objResult;
  };

  // handle onchange Receive Tokens
  const handleFormChange = (selectedToken) => {
    // console.log(selectedToken);
  };

  //handle onclick Max
  const handleOnClickMax = (e) => {
    e.preventDefault();
    setFee(myWalletBalance["DOLLY"] / 50);
    setDollyInput(myWalletBalance["DOLLY"]);
    setWithdrawAmount(
      parseStringToBigNumber(myWalletBalance["DOLLY"], 18).value
    );
    checkWalletExceed(myWalletBalance["DOLLY"]);
  };

  // handle input change
  const handleOnInputChange = (e) => {
    const inputValue = e.target.value;
    setFee(inputValue / 50);
    setDollyInput(inputValue);
    setWithdrawAmount(parseStringToBigNumber(inputValue, 18).value);
    checkWalletExceed(inputValue);
  };

  const checkWalletExceed = (inputValue) => {
    if (parseFloat(inputValue) > parseFloat(myWalletBalance["DOLLY"])) {
      setExceedsWallet(true);
    } else {
      setExceedsWallet(false);
    }
  };

  useEffect(() => {
    loadWalletBalance();
  }, [account]);

  return (
    <>
      {/* left side */}
      <div className="dark:bg-dark-700 dark:text-white rounded-2xl shadow-md p-6 lg:p-10 py-4 bg-white -mt-6">
        <div className="text-2xl font-bold mb-6">Redeem Dolly</div>
        <TokenInputRepay
          symbol={DOLLY.symbol}
          icon={DOLLY.icon}
          dollyInput={dollyInput}
          withdrawAmount={withdrawAmount}
          onChange={handleOnInputChange}
          onClickMax={handleOnClickMax}
          myWalletBalance={myWalletBalance}
        />
        <TextMintFee fee={fee} />
        <Devider />
        <ReceiveTokenList
          radioActive={radioActive}
          receiveList={receiveList}
          setRadioActive={setRadioActive}
        />
        {/*  */}
        <div className="pt-4">
          <div className="w-full">
            <div className="grid grid-cols-1 items-center">
              {tokensData.map((token, index) => {
                const myshareDataTokenValue = userShareData?.tokens.find(
                  (t) => t.symbol === token.symbol
                ).value;
                const tokenValueSub = myshareDataTokenValue?.sub(
                  BigNumber.from(10).pow(BigNumber.from(token.decimals))
                );

                const tokenValueResult = tokenValueSub?.lt(0)
                  ? Zero
                  : tokenValueSub;
                return (
                  <TokenDollyDisplay
                    key={index}
                    inputValue={dollyInput}
                    withdrawAmount={withdrawAmount}
                    // dollyInput={dollyInput}
                    {...token}
                    max={
                      userShareData &&
                      formatBNToString(tokenValueResult, token.decimals)
                    }
                    myShareData={userShareData}
                    onChange={(value) =>
                      updateWithdrawFormState({
                        fieldName: "tokenInputs",
                        value: value,
                        tokenSymbol: token.symbol,
                      })
                    }
                    deposit={false}
                    radioActive={radioActive}
                    setRadioActive={setRadioActive}
                  />
                );
              })}
            </div>
          </div>
        </div>
        <BtnConfirmRedeem
          exceedsWallet={exceedsWallet}
          setCurrentModal={setCurrentModal}
        />
      </div>

      {/* right side */}

      <div className="right">
        <TotalInfo
          tokenUserInfoList={tokenUserInfoList}
          loadWalletBalance={loadWalletBalance}
        />
      </div>
      <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
        {currentModal === "review" ? (
          <ReviewWithdraw
            data={reviewWithdrawData}
            gas={gasPriceSelected}
            onConfirm={async () => {
              setCurrentModal("confirm");
              await onConfirmTransaction?.();
              setCurrentModal(null);
            }}
            onClose={() => setCurrentModal(null)}
          />
        ) : null}
        {currentModal === "confirm" ? <ConfirmTransaction /> : null}
      </Modal>
    </>
  );
}

function TokenInputRepay({
  symbol,
  icon,
  dollyInput,
  onChange,
  onClickMax,
  myWalletBalance,
}) {
  return (
    <>
      <div className="overflow-hidden relative">
        {/* balance text */}
        <div className="flex justify-between pb-1">
          <div className="pl-1 text-sm flex">
            <div>Balance :&nbsp;</div>
            <span className="text-blue-400 font-bold">
              {myWalletBalance["DOLLY"]}
            </span>
          </div>
        </div>
        {/* input */}
        <div className="pb-8 ">
          <input
            type="number"
            className="dark:bg-dark-600 dark:text-white dark:border-0 border rounded-md focus:outline-none bg-steel-100 p-2 pl-32 h-12 w-full"
            placeholder="0.0"
            value={dollyInput}
            onChange={onChange}
          />
          <div className="flex justify-between ">
            {/* icon  */}
            <div className="dark:bg-dark-500 dark:border-0 border rounded-md bg-white w-28 flex items-center p-2 -mt-12 z-10 relative">
              <Image src={icon} width="30px" height="30px" />
              <span className="font-bold ml-2">{symbol}</span>
            </div>
            {/* max btn */}
            <div className="p-1 -mt-12 z-10 relative">
              <button
                className={`focus:outline-none px-4 py-2 bg-blue-400 rounded-md text-white`}
                onClick={onClickMax}
              >
                MAX
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

// left
function TextMintFee({ fee }) {
  return (
    <>
      <div className="flex justify-between text-sm">
        <div className="dark:text-white font-bold text-steel-300">
          Redeem Fee :
        </div>
        <div className="text-right font-bold text-blue-400">
          {fee ? fee : "0"} DOP
        </div>
      </div>
      <div className="dark:text-white text-steel-300 text-sm mt-3">
        Fee Rate = 50 DOLLY : 1 DOP
      </div>
    </>
  );
}

function Devider() {
  return (
    <>
      <div className="flex items-center my-5 mb-6">
        <div className="bg-steel-200 w-full h-1px"></div>
        <div className="border rounded-lg flex items-center justify-center py-2 px-2">
          <Image src={arrowDownIcon} width="34" height="34" />
        </div>
        <div className="bg-steel-200 w-full h-1px"></div>
      </div>
    </>
  );
}

function TotalInfo({ tokenUserInfoList, loadWalletBalance }) {
  const [balance, setBalance] = useState({
    DOP: 0,
    BUSD: 0,
    USDT: 0,
    DOLLY: 0,
  });

  useEffect(() => {
    (async () => {
      const balance = await loadWalletBalance();
      setBalance(balance);
    })();
  }, [balance]);

  return (
    <>
      <div className="dark:bg-dark-700 dark:text-white rounded-2xl shadow-md py-3 bg-white -mt-6 ">
        <div className="absolute -ml-6 mt-8 hidden sm:block"></div>
        <div className="dark:border-b-dark-500 flex items-center border-b pb-2 lg:px-8 ">
          <div className="pb-2 mr-4">
            <Image
              src="/images/icons/dolly-account.svg"
              width="100"
              height="100"
            />
          </div>
          <div>
            {/* <div className="text-sm mb-1">You will receive</div> */}
            <div className="text-xl font-bold mb-1">Total Dolly</div>
            <div className="text-2xl text-blue-400 font-bold">
              {balance && balance["DOLLY"]}
            </div>
          </div>
        </div>
        <div className="items-center p-6 pb-3 text-sm">
          {tokenUserInfoList.map((coin, index) => (
            <div key={index}>
              {coin.symbol == "DOLLY" ? null : (
                <div className="grid grid-cols-2 mb-4">
                  {coin.symbol ? (
                    <div className="flex">
                      <Image src={coin.icon} width="20" height="20" />
                      <div className="dark:text-white text-steel-300 ml-2 ">
                        {coin.symbol} : &nbsp;
                      </div>
                    </div>
                  ) : (
                    <div className="grid grid-cols-2">
                      <Skeleton count={1} />
                    </div>
                  )}
                  <span className="font-bold text-right">
                    {balance && balance[coin.symbol] ? (
                      balance[coin.symbol]
                    ) : (
                      <Skeleton count={1} />
                    )}
                  </span>
                </div>
              )}
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

function BtnConfirmRedeem({ setCurrentModal, exceedsWallet }) {
  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
  } = useActiveWeb3React();

  const [allowance, setAllowance] = useState(BigNumber.from("0"));
  const [redeemAllowance, setRedeemAllowance] = useState(BigNumber.from("0"));

  const tokenContract = useTokenContract(DOPPLE);
  const dollyTokenContract = useTokenContract(DOLLY);

  const mintContract = useSwapContract(DOLLY_MINT_POOL_NAME);

  const updateAllowance = async () => {
    if (!tokenContract || !mintContract || !account) return;
    const existingAllowance = await tokenContract.allowance(
      account,
      mintContract.address
    );
    setAllowance(existingAllowance);
  };

  const updateDollyAllowance = async () => {
    if (dollyTokenContract && mintContract) {
      const existingAllowance = await dollyTokenContract.allowance(
        account,
        mintContract.address
      );
      setRedeemAllowance(existingAllowance);
    }
  };

  useEffect(() => {
    (async () => {
      updateAllowance();
      updateDollyAllowance();
    })();
  }, [tokenContract, mintContract, chainId, account]);

  async function approveDopple({ setShowLoadingIcon }) {
    try {
      const result = await tokenContract.approve(
        mintContract.address,
        MaxUint256
      );
      setShowLoadingIcon(true);
      await result.wait();
      await updateAllowance();
    } catch (error) {}
  }

  async function approveRedeem({ setShowLoadingIcon }) {
    try {
      const result = await dollyTokenContract.approve(
        mintContract.address,
        MaxUint256
      );
      setShowLoadingIcon(true);
      await result.wait();
      await updateDollyAllowance();
    } catch (error) {}
  }

  return (
    <>
      <div
        className={`${
          allowance.gt(0) && redeemAllowance.gt(0)
            ? "grid-cols-1"
            : "grid-cols-2 gap-2"
        } grid justify-center items-center`}
      >
        {allowance.gt(0) ? (
          ""
        ) : (
          <BtnApproveDopple handleOnClick={approveDopple} />
        )}

        {redeemAllowance.gt(0) ? (
          ""
        ) : (
          <BtnApproveRedeem handleOnClick={approveRedeem} />
        )}

        {redeemAllowance.gt(0) || allowance.gt(0) ? (
          <BtnRedeemDolly
            disabled={allowance.lt(1) || redeemAllowance.lt(1) ? true : false}
            setCurrentModal={setCurrentModal}
            exceedsWallet={exceedsWallet}
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
}

function BtnRedeemDolly({ setCurrentModal, disabled, exceedsWallet }) {
  return (
    <>
      <button
        className={`focus:outline-none w-full text-white py-3 rounded-lg shadow-sm mt-8 text-center font-bold ${
          disabled || exceedsWallet
            ? " bg-gray-400 cursor-default"
            : "  bg-gradient-primary  "
        } `}
        disabled={disabled || exceedsWallet ? true : false}
        onClick={() => {
          setCurrentModal("review");
        }}
      >
        {exceedsWallet ? "Insuffician Balance" : "Redeem"}
      </button>
    </>
  );
}

function BtnApproveDopple({ handleOnClick }) {
  const [showLoadingIcon, setShowLoadingIcon] = useState(false);
  return (
    <>
      <button
        className="focus:outline-none  w-full text-white py-3 rounded-lg shadow-sm  bg-gradient-primary  mt-8 text-center font-bold flex items-center justify-center"
        onClick={async () => {
          if (showLoadingIcon) return;
          await handleOnClick({ setShowLoadingIcon });
          setShowLoadingIcon(false);
        }}
      >
        {showLoadingIcon && <LoadingIcon />}
        Approve Fee
      </button>
    </>
  );
}

function BtnApproveRedeem({ handleOnClick }) {
  const [showLoadingIcon, setShowLoadingIcon] = useState(false);
  return (
    <>
      <button
        className="focus:outline-none  w-full text-white py-3 rounded-lg shadow-sm  bg-gradient-primary  mt-8 text-center font-bold flex items-center justify-center"
        onClick={async () => {
          if (showLoadingIcon) return;
          await handleOnClick({ setShowLoadingIcon });
          setShowLoadingIcon(false);
        }}
      >
        {showLoadingIcon && <LoadingIcon />}
        Approve Redeem
      </button>
    </>
  );
}

function ReceiveTokenList({ receiveList, radioActive, setRadioActive }) {
  const handleOnClick = (val) => {
    setRadioActive(val);
  };

  return (
    <>
      <div className="mt-4">
        <div className="dark:text-white text-steel-300 text-sm mb-2">
          You will receive as
        </div>
        <div className="grid grid-cols-3 gap-3">
          {receiveList.map((val, key) => (
            <div
              className={`${
                radioActive == val.value
                  ? "bg-gradient-primary text-white"
                  : "bg-steel-100 text-steel-300"
              } rounded  p-2 pt-3 grid grid-cols-1 items-center justify-center cursor-pointer`}
              key={key}
              onClick={() => handleOnClick(val.value)}
            >
              <div className="flex items-center justify-center">
                <Image
                  src={
                    radioActive == val.value ? val.iconActive : val.iconInActive
                  }
                  width={val.value == "BUSD" ? 24 : 60}
                  height="24"
                />
              </div>
              <div className="font-bold text-center text-xs mt-2">
                {val.name}
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

const LoadingIcon = () => (
  <svg
    className="animate-spin mr-1 h-4 w-4 text-white"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
  >
    <circle
      className="opacity-25"
      cx="12"
      cy="12"
      r="10"
      stroke="currentColor"
      strokeWidth="4"
    ></circle>
    <path
      className="opacity-75"
      fill="currentColor"
      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
    ></path>
  </svg>
);
