import Head from "next/head";
import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import MintComponent from "./mint";
import RepayComponent from "./repay";
import { useGetApiData } from "../../hooks/useGetApiData";

export default function DollyPage() {
  const [tabSelected, setTabSelected] = useState("mint");
  useGetApiData();
  return (
    <>
      <Head>
        <title>Dolly : Stablecoin DEX on BSC</title>
      </Head>
      <div className="mint-container container mb-12">
        <div className="text-3xl font-bold mb-6 mt-4 flex px-5 relative z-1">
          <div
            className={`${
              tabSelected == "mint"
                ? "border-b-2 text-blue-400"
                : " dark:text-white "
            } border-blue-400 cursor-pointer px-7 pb-5`}
            onClick={() => setTabSelected("mint")}
          >
            Mint
          </div>
          <div
            className={`${
              tabSelected == "repay"
                ? "border-b-2 text-blue-400"
                : " dark:text-white "
            } border-blue-400 cursor-pointer px-7 pb-5`}
            onClick={() => setTabSelected("repay")}
          >
            Redeem
          </div>
        </div>
        <div className="grid grid-cols-auto sm:grid-cols-2 gap-8">
          {tabSelected == "mint" ? <MintComponent /> : <RepayComponent />}
        </div>
      </div>
    </>
  );
}
