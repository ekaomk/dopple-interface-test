import {
  BLOCK_TIME,
  DOLLY,
  DOLLY_MINT_POOL_NAME,
  MINT_TOKEN_MAP,
  MINT_TOKEN_USER_INFO_MAP,
  POOLS_MAP,
  TOKENS_MAP,
} from "../../constants";
import React, { useCallback, useEffect, useState } from "react";
import Image from "next/image";
import { useActiveWeb3React } from "../../hooks";
import {
  useLPTokenContract,
  useSwapContract,
  useTokenContract,
} from "../../hooks/useContract";
import { BUSD, USDT, DOPPLE } from "../../constants";
import { formatBNToString, shiftBNDecimals } from "../../utils/index";
import Skeleton from "react-loading-skeleton";
import { BigNumber } from "@ethersproject/bignumber";
import {
  useApproveAndDeposit,
  useApproveForDeposit,
} from "../../hooks/useApproveAndDeposit";
import ReviewDeposit from "../../components/ReviewDeposit";
import Modal from "../../components/Modal";
import ConfirmTransaction from "../../components/ConfirmTransaction";
import usePoolData from "../../hooks/usePoolData";
import {
  TokensStateType,
  useTokenFormState,
} from "../../hooks/useTokenFormState";
import { usePoolTokenBalances } from "../../state/wallet/hook";
import { Zero, MaxUint256 } from "@ethersproject/constants";
import { parseUnits } from "@ethersproject/units";
import { calculatePriceImpact } from "../../utils/priceImpact";
import { useDispatch, useSelector } from "react-redux";
import parseStringToBigNumber from "../../utils/parseStringToBigNumber";
import usePoller from "../../hooks/usePoller";
import fetchGasPrices from "../../utils/updateGasPrices";

export default function MintComponent() {
  const doppleTokenContract = useTokenContract(DOPPLE);
  const busdTokenContract = useTokenContract(BUSD);
  const usdtTokenContract = useTokenContract(USDT);
  const dollyTokenContract = useTokenContract(DOLLY);

  const [currentModal, setCurrentModal] = useState(null);

  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
  } = useActiveWeb3React();

  // -----------
  const POOL = POOLS_MAP[DOLLY_MINT_POOL_NAME];
  const approveAndDeposit = useApproveAndDeposit(DOLLY_MINT_POOL_NAME);
  const [poolData, userShareData] = usePoolData(DOLLY_MINT_POOL_NAME);
  const [tokenFormState, updateTokenFormState] = useTokenFormState(
    POOL.poolTokens
  );
  const tokenBalances = usePoolTokenBalances(DOLLY_MINT_POOL_NAME);
  const [estDepositLPTokenAmount, setEstDepositLPTokenAmount] = useState(Zero);
  const [priceImpact, setPriceImpact] = useState(Zero);

  const dispatch = useDispatch();

  const fetchAndUpdateGasPrice = useCallback(() => {
    fetchGasPrices(dispatch);
  }, [dispatch]);
  usePoller(fetchAndUpdateGasPrice, BLOCK_TIME);

  useEffect(() => {
    // evaluate if a new deposit will exceed the pool's per-user limit
    async function calculateMaxDeposits() {
      if (
        swapContract == null ||
        userShareData == null ||
        poolData == null ||
        account == null
      ) {
        setEstDepositLPTokenAmount(Zero);
        return;
      }
      const tokenInputSum = parseUnits(
        POOL.poolTokens
          .reduce(
            (sum, { symbol }) => sum + (+tokenFormState[symbol].valueRaw || 0),
            0
          )
          .toFixed(18),
        18
      );
      let depositLPTokenAmount;
      if (poolData.totalLocked.gt(0) && tokenInputSum.gt(0)) {
        try {
          depositLPTokenAmount = await swapContract.calculateTokenAmount(
            account,
            POOL.poolTokens.map(
              ({ symbol }) => tokenFormState[symbol].valueSafe
            ),
            true // deposit boolean
          );
        } catch {
          // For handle value is more uint 256
          return;
        }
      } else {
        // when pool is empty, estimate the lptokens by just summing the input instead of calling contract
        depositLPTokenAmount = tokenInputSum;
      }
      setEstDepositLPTokenAmount(depositLPTokenAmount);
      setPriceImpact(
        calculatePriceImpact(
          tokenInputSum,
          depositLPTokenAmount,
          poolData.virtualPrice
        )
      );
    }
    calculateMaxDeposits();
  }, [
    poolData,
    tokenFormState,
    swapContract,
    userShareData,
    account,
    POOL.poolTokens,
  ]);

  // A represention of tokens used for UI
  // const tokens = POOL.poolTokens.map(({ symbol, name, icon, decimals }) => ({
  //   symbol,
  //   name,
  //   icon,
  //   max: formatBNToString(tokenBalances?.[symbol] || Zero, decimals),
  //   inputValue: tokenFormState[symbol].valueRaw,
  // }));

  const exceedsWallet = POOL.poolTokens.some(({ symbol }) => {
    const exceedsBoolean = (tokenBalances?.[symbol] || Zero).lt(
      BigNumber.from(tokenFormState[symbol].valueSafe)
    );
    return exceedsBoolean;
  });

  async function onConfirmTransaction() {
    await approveAndDeposit(tokenFormState);
    // Clear input after deposit
    updateTokenFormState(
      POOL.poolTokens.reduce(
        (acc, t) => ({
          ...acc,
          [t.symbol]: "",
        }),
        {}
      )
    );
  }

  function updateTokenFormValue(symbol, value) {
    updateTokenFormState({ [symbol]: value });
  }

  const depositTransaction = buildTransactionData(
    tokenFormState,
    poolData,
    POOL.poolTokens,
    POOL.lpToken,
    priceImpact,
    estDepositLPTokenAmount
  );

  // const { userPoolAdvancedMode: advanced } = useSelector((state) => state.user);

  // -----------

  const tokenContractList = {
    [DOPPLE.symbol]: doppleTokenContract,
    [BUSD.symbol]: busdTokenContract,
    [USDT.symbol]: usdtTokenContract,
    [DOLLY.symbol]: dollyTokenContract,
  };

  const tokenToDollyListObject = React.useMemo(
    () =>
      MINT_TOKEN_MAP.map(({ name, symbol, icon, decimals }) => ({
        name,
        symbol,
        icon,
        decimals,
        inputValue: 0,
        contract: tokenContractList[symbol],
      })),
    []
  );

  const tokenUserInfoList = React.useMemo(
    () =>
      MINT_TOKEN_USER_INFO_MAP.map(({ name, symbol, icon, decimals }) => ({
        name,
        symbol,
        icon,
        decimals,
        inputValue: "",
      })),
    []
  );

  const [tokenToDollyList, setTokenToDollyList] = useState(
    tokenToDollyListObject
  );
  const [mintFee, setMintFee] = useState(0);

  const [myWalletBalance, setMyWalletBalance] = useState({
    DOP: 0,
    BUSD: 0,
    USDT: 0,
  });

  const swapContract = useSwapContract(DOLLY_MINT_POOL_NAME);

  let fairLaunchDecimals;

  const loadWalletBalance = async () => {
    if (
      !doppleTokenContract ||
      !busdTokenContract ||
      !usdtTokenContract ||
      !dollyTokenContract ||
      !account
    )
      return;
    const totalDoppleInWallet = await doppleTokenContract.balanceOf(account);
    const totalBUSDInWallet = await busdTokenContract.balanceOf(account);
    const totalUSDTInWallet = await usdtTokenContract.balanceOf(account);
    const totalDOLLYInWallet = await dollyTokenContract.balanceOf(account);

    let objResult = myWalletBalance;
    objResult.DOP = formatBNToString(totalDoppleInWallet, fairLaunchDecimals);
    objResult.BUSD = formatBNToString(totalBUSDInWallet, fairLaunchDecimals);
    objResult.USDT = formatBNToString(totalUSDTInWallet, fairLaunchDecimals);
    objResult.DOLLY = formatBNToString(totalDOLLYInWallet, fairLaunchDecimals);

    setMyWalletBalance(objResult);
    return objResult;
  };

  useEffect(() => {
    loadWalletBalance();
  }, [account]);

  return (
    <>
      {/* left side */}
      <div className="dark:bg-dark-700 dark:text-white rounded-2xl shadow-md p-6 lg:p-10 py-4 bg-white -mt-6">
        <div className="text-2xl font-bold mb-6">Mint Dolly</div>
        {tokenToDollyList.map((val, key) => (
          <div key={key}>
            <TokenInputMint
              symbol={val.symbol}
              icon={val.icon}
              index={key}
              tokenToDollyList={tokenToDollyList}
              setTokenToDollyList={setTokenToDollyList}
              setMintFee={setMintFee}
              loadWalletBalance={loadWalletBalance}
              setCurrentModal={setCurrentModal}
              tokenContract={val.contract}
              swapContract={swapContract}
              chainId={chainId}
              account={account}
              onChangeTokenInputValue={updateTokenFormValue}
              decimals={TOKENS_MAP[val.symbol]}
            />
          </div>
        ))}
        <TextMintFee fee={mintFee} />

        <BtnConfirmMint
          exceedsWallet={exceedsWallet}
          setCurrentModal={setCurrentModal}
        />
      </div>
      {/* right side */}
      <div className="right">
        <TotalInfo
          tokenUserInfoList={tokenUserInfoList}
          loadWalletBalance={loadWalletBalance}
          depositTransaction={depositTransaction}
        />
        <CardStakingDolly />
      </div>
      <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
        {currentModal === "review" ? (
          <ReviewDeposit
            transactionData={depositTransaction}
            onConfirm={async () => {
              setCurrentModal("confirm");
              await onConfirmTransaction?.();
              setCurrentModal(null);
            }}
            onClose={() => setCurrentModal(null)}
          />
        ) : null}
        {currentModal === "confirm" ? <ConfirmTransaction /> : null}
      </Modal>
    </>
  );
}

// left
function TextMintFee({ fee }) {
  return (
    <>
      <div className="flex justify-between text-sm">
        <div className="dark:text-white font-bold text-steel-300">
          Mint Fee :
        </div>
        <div className="text-right font-bold text-blue-400">{fee} DOP</div>
      </div>
      <div className="dark:text-white text-steel-300 text-sm mt-3">
        Fee Rate = 100 DOLLY : 1 DOP
      </div>
    </>
  );
}

function BtnMintDolly({ setCurrentModal, disabled, exceedsWallet }) {
  return (
    <>
      <button
        className={`focus:outline-none w-full text-white py-3 rounded-lg shadow-sm mt-8 text-center font-bold ${
          disabled || exceedsWallet
            ? " bg-gray-400 cursor-default"
            : "  bg-gradient-primary  "
        } `}
        disabled={disabled || exceedsWallet ? true : false}
        onClick={() => {
          setCurrentModal("review");
        }}
      >
        {exceedsWallet ? "Insuffician Balance" : "Mint"}
      </button>
    </>
  );
}

function BtnApproveDopple({ handleOnClick }) {
  const [showLoadingIcon, setShowLoadingIcon] = useState(false);
  return (
    <>
      <button
        className="focus:outline-none w-full text-white py-3 rounded-lg shadow-sm  bg-gradient-primary  mt-8 text-center font-bold flex items-center justify-center"
        onClick={async () => {
          if (showLoadingIcon) return;
          await handleOnClick({ setShowLoadingIcon });
          setShowLoadingIcon(false);
        }}
      >
        {showLoadingIcon && <LoadingIcon />}
        Approve
      </button>
    </>
  );
}

const AllowanceButton = ({ icon, symbol, approve }) => {
  return (
    <>
      <div className=" overflow-hidden">
        <div className="dark:bg-dark-600  bg-white absolute w-full h-18 z-40 bg-opacity-100 rounded backdrop-filter flex items-center justify-center">
          <button
            type="button"
            className={`focus:outline-none bg-transparent text-center py-2  h-full w-full border rounded-md font-bold border-blue-500 text-blue-500`}
            onClick={(e) => {
              approve();
            }}
          >
            <div className="flex justify-between px-2 items-center">
              <div className="flex items-center">
                <Image src={icon} width="30px" height="30px" />
                <span className="dark:text-white text-black font-base ml-2">
                  {symbol}
                </span>
              </div>
              <span> Approve</span>
              <div className="flex items-center invisible">
                <Image src={icon} width="30px" height="30px" />
                <span className="text-black font-base ml-2">{symbol}</span>
              </div>
            </div>
          </button>
        </div>
      </div>
    </>
  );
};

function BtnConfirmMint({ setCurrentModal, exceedsWallet }) {
  const {
    account,
    chainId,
    error: web3Error,
    deactivate,
  } = useActiveWeb3React();

  const [allowance, setAllowance] = useState(BigNumber.from("0"));

  const tokenContract = useTokenContract(DOPPLE);
  const mintContract = useSwapContract(DOLLY_MINT_POOL_NAME);

  const updateAllowance = async () => {
    if (!tokenContract || !mintContract || !account) return;
    const existingAllowance = await tokenContract.allowance(
      account,
      mintContract.address
    );
    setAllowance(existingAllowance);
  };

  useEffect(() => {
    (async () => {
      updateAllowance();
    })();
  }, [tokenContract, mintContract, chainId, account]);

  async function approveDopple({ setShowLoadingIcon }) {
    try {
      const result = await tokenContract.approve(
        mintContract.address,
        MaxUint256
      );
      setShowLoadingIcon(true);
      await result.wait();
      await updateAllowance();
    } catch (error) {}
  }

  return (
    <>
      <div
        className={`${
          allowance.gt(0) ? "grid-cols-1" : "grid-cols-2 gap-2"
        } grid justify-center items-center`}
      >
        {allowance.gt(0) ? (
          ""
        ) : (
          <BtnApproveDopple handleOnClick={approveDopple} />
        )}
        <BtnMintDolly
          disabled={allowance.gt(0) ? false : true}
          setCurrentModal={setCurrentModal}
          exceedsWallet={exceedsWallet}
        />
      </div>
    </>
  );
}

function TokenInputMint({
  symbol,
  icon,
  index,
  tokenToDollyList,
  setTokenToDollyList,
  setMintFee,
  loadWalletBalance,
  setCurrentModal,
  tokenContract,
  swapContract,
  chainId,
  account,
  decimals,
  onChangeTokenInputValue,
}) {
  const [balance, setBalance] = useState(0);
  const [allowance, setAllowance] = useState(BigNumber.from("0"));
  const [inputValue, setInputValue] = useState("");

  function onClickMax(e) {
    e.preventDefault();
    onChangeInput(balance, true);
    setInputValue(balance);
    onChangeTokenInputValue(symbol, balance);
  }

  const updateAllowance = async () => {
    if (!tokenContract || !swapContract || !account) return;
    const existingAllowance = await tokenContract.allowance(
      account,
      swapContract.address
    );
    setAllowance(existingAllowance);
  };

  useEffect(() => {
    (async () => {
      updateAllowance();
    })();
  }, [tokenContract, swapContract, balance, chainId, account]);

  function onChangeInput(inputValue, maxButton) {
    // Validate
    const parsedValue = parseFloat("0" + inputValue);
    const periodIndex = inputValue.indexOf(".");
    const isValidInput = inputValue === "" || !isNaN(parsedValue);
    // const isValidPrecision =
    //   periodIndex === -1 || inputValue.length - 1 - periodIndex <= decimals;
    if (isValidInput) {
      // don't allow input longer than the token allows
      // set data for Input
      setInputValue(inputValue);

      // set data for calc
      tokenToDollyList[index].inputValue = inputValue;
      setTokenToDollyList(tokenToDollyList);

      // calc Mint Fee
      let mintFee = 0;
      tokenToDollyList.map((x) => {
        if (x.inputValue == "") {
          mintFee += 0;
        } else {
          mintFee += parseFloat(x.inputValue);
        }
      });
      setMintFee(isNaN(mintFee) ? 0 : mintFee / 100);
      onChangeTokenInputValue(symbol, inputValue);
    }
  }

  useEffect(() => {
    (async () => {
      const walletBalance = await loadWalletBalance();
      if (walletBalance) setBalance(walletBalance[symbol]);
    })();
  }, [balance]);

  const approveForDepositToken = useApproveForDeposit(
    DOLLY_MINT_POOL_NAME,
    TOKENS_MAP[symbol]
  );

  const approveForDeposit = async () => {
    setCurrentModal("confirm");
    await approveForDepositToken();
    await updateAllowance();
    setCurrentModal(null);
  };

  return (
    <>
      <div className="overflow-hidden relative">
        {/* balance text */}
        <div className="flex justify-between pb-1">
          <div className="pl-1 text-sm flex">
            <div>Balance :&nbsp;</div>
            <span className="text-blue-400 font-bold">{balance}</span>
          </div>
        </div>
        {/* input */}
        <div className="pb-8 ">
          {allowance.eq(0) && (
            <AllowanceButton
              icon={icon}
              symbol={symbol}
              approve={approveForDeposit}
            />
          )}
          <input
            type="number"
            className="dark:border-0 dark:bg-dark-600 border rounded-md focus:outline-none bg-steel-100 p-2 pl-32 h-12 w-full"
            placeholder="0.0"
            value={inputValue}
            onChange={(e) => onChangeInput(e.target.value, false)}
          />

          <div className="flex justify-between ">
            {/* icon  */}
            <div className="dark:border-0 dark:bg-dark-500 border rounded-md bg-white w-28 flex items-center p-2 -mt-12 z-10 relative">
              <Image src={icon} width="30px" height="30px" />
              <span className="font-bold ml-2">{symbol}</span>
            </div>
            {/* max btn */}
            <div className="p-1 -mt-12 z-10 relative">
              <button
                className={`focus:outline-none px-4 py-2 bg-blue-400 rounded-md text-white`}
                onClick={onClickMax}
              >
                MAX
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

// right
function TotalInfo({
  tokenUserInfoList,
  loadWalletBalance,
  depositTransaction,
}) {
  const [balance, setBalance] = useState({ DOP: 0, BUSD: 0, USDT: 0 });
  const [dollyBalance, setDollyBalance] = useState("0.0");

  useEffect(() => {
    (async () => {
      const balance = await loadWalletBalance();
      setBalance(balance);
    })();
  }, [balance]);

  useEffect(() => {
    setDollyBalance(formatBNToString(depositTransaction.to.totalAmount));
  }, [depositTransaction]);

  return (
    <>
      <div className="dark:bg-dark-700 dark:text-white rounded-2xl shadow-md py-3 bg-white -mt-6 ">
        <div className="absolute -ml-6 mt-8 hidden sm:block">
          <Image
            src="/images/icons/deposit-arrow-next.svg"
            width="50"
            height="50"
          />
        </div>
        <div className="dark:border-b-dark-500 flex items-center border-b pb-2 lg:px-12 ">
          <div className="pb-2 mr-4">
            <Image
              src="/images/icons/dolly-account.svg"
              width="100"
              height="100"
            />
          </div>
          <div>
            <div className="text-sm mb-1">You will receive</div>
            <div className="text-xl font-bold mb-1">Total Dolly</div>
            <div className="text-2xl text-blue-400 font-bold">
              {dollyBalance}
            </div>
          </div>
        </div>
        <div className="items-center p-6 pb-3 text-sm">
          {tokenUserInfoList.map((coin, index) => (
            <div key={index} className="grid grid-cols-2 mb-4">
              {coin.symbol ? (
                <div className="flex">
                  <Image src={coin.icon} width="20" height="20" />
                  <div className="dark:text-white text-steel-300 ml-2 ">
                    {coin.symbol} : &nbsp;
                  </div>
                </div>
              ) : (
                <div className="grid grid-cols-2">
                  <Skeleton count={1} />
                </div>
              )}
              <span className="font-bold text-right">
                {balance && balance[coin.symbol] ? (
                  balance[coin.symbol]
                ) : (
                  <Skeleton count={1} />
                )}
              </span>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}

const CardStakingDolly = () => {
  return (
    <>
      <div className="dark:text-white dark:bg-dark-700 rounded-2xl shadow-md py-3 bg-white mt-8">
        <div className="flex w-full justify-center relative">
          <div className="absolute -mt-10 hidden sm:block transform rotate-90 ">
            <Image
              src="/images/icons/deposit-arrow-next.svg"
              width="50"
              height="50"
            />
          </div>
        </div>

        <div className="p-2 lg:px-8 ">
          <div className="text-2xl font-bold">Deposit & Farm</div>
          <div className="flex py-3 pb-3 justify-between">
            <div className="flex items-center">
              <Image
                src="/images/icons/dolly-pool.svg"
                width="60"
                height="50"
              />
              <div className="ml-2">
                <div className="font-bold text-lg">DOLLY Pool</div>
                <div className="text-blue-400 text-sm font-bold ">
                  Farm DOLLY LPs (10X)
                </div>
              </div>
            </div>
            <div>
              <a href="/Deposit/dolly-lps">
                <button className="focus:outline-none w-full  text-white  py-3 px-8 rounded-lg shadow-sm  bg-gradient-primary ">
                  Deposit Now
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

function buildTransactionData(
  tokenFormState,
  poolData,
  poolTokens,
  poolLpToken,
  priceImpact,
  estDepositLPTokenAmount
) {
  const from = {
    items: [],
    totalAmount: Zero,
  };
  const TOTAL_AMOUNT_DECIMALS = 18;
  poolTokens.forEach((token) => {
    const { symbol, decimals } = token;
    const amount = BigNumber.from(tokenFormState[symbol].valueSafe);
    if (amount.lte("0")) return;
    const item = {
      token,
      amount,
    };
    from.items.push(item);
    from.totalAmount = from.totalAmount.add(
      shiftBNDecimals(amount, TOTAL_AMOUNT_DECIMALS - decimals)
    );
  });

  const to = {
    item: {
      token: poolLpToken,
      amount: estDepositLPTokenAmount,
    },
    totalAmount: estDepositLPTokenAmount,
  };
  const shareOfPool = poolData?.totalLocked.gt(0)
    ? estDepositLPTokenAmount
        .mul(BigNumber.from(10).pow(18))
        .div(estDepositLPTokenAmount.add(poolData?.totalLocked))
    : BigNumber.from(10).pow(18);
  return {
    from,
    to,
    priceImpact,
    shareOfPool,
  };
}

const LoadingIcon = () => (
  <svg
    className="animate-spin mr-1 h-4 w-4 text-white"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    viewBox="0 0 24 24"
  >
    <circle
      className="opacity-25"
      cx="12"
      cy="12"
      r="10"
      stroke="currentColor"
      strokeWidth="4"
    ></circle>
    <path
      className="opacity-75"
      fill="currentColor"
      d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
    ></path>
  </svg>
);
