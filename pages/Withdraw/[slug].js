import Head from "next/head";
import {
  POOLS_MAP,
  PoolName,
  ACTIVE_POOL_NAME,
  POOLS_LIST,
} from "../../constants";
import React, { ReactElement, useEffect, useState } from "react";
import WithdrawPage, {
  ReviewWithdrawData,
} from "../../components/WithdrawPage";
import { commify, formatUnits, parseUnits } from "@ethersproject/units";
import PageNotFound from "../../components/PageNotFound";

import { AppState } from "../../state";
import { BigNumber } from "@ethersproject/bignumber";
import { Zero } from "@ethersproject/constants";
import { calculatePriceImpact } from "../../utils/priceImpact";
import { formatSlippageToString } from "../../utils/slippage";
import { useActiveWeb3React } from "../../hooks";
import { useApproveAndWithdraw } from "../../hooks/useApproveAndWithdraw";
import usePoolData from "../../hooks/usePoolData";
import { useSelector } from "react-redux";
import { useSwapContract } from "../../hooks/useContract";
import useWithdrawFormState from "../../hooks/useWithdrawFormState";
import { formatBNToString } from "../../utils";

import { useRouter } from "next/router";

function Withdraw({ poolName }) {
  const router = useRouter();

  if (!POOLS_LIST?.find((x) => x.slug == router?.query?.slug))
    return <PageNotFound />;

  // Force pool here
  poolName = POOLS_LIST.find((x) => x.slug == router.query.slug).name;

  const [poolData, userShareData] = usePoolData(poolName);
  const [withdrawFormState, updateWithdrawFormState] = useWithdrawFormState(
    poolName
  );
  const { slippageCustom, slippageSelected } = useSelector(
    (state) => state.user
  );
  const approveAndWithdraw = useApproveAndWithdraw(poolName);
  const swapContract = useSwapContract(poolName);
  const { account } = useActiveWeb3React();
  const POOL = POOLS_MAP[poolName];

  const [estWithdrawBonus, setEstWithdrawBonus] = useState(Zero);
  useEffect(() => {
    // evaluate if a new withdraw will exceed the pool's per-user limit
    async function calculateWithdrawBonus() {
      if (
        swapContract == null ||
        userShareData == null ||
        poolData == null ||
        account == null
      ) {
        return;
      }
      const tokenInputSum = parseUnits(
        POOL.poolTokens
          .reduce(
            (sum, { symbol }) =>
              sum + (+withdrawFormState.tokenInputs[symbol].valueRaw || 0),
            0
          )
          .toFixed(18),
        18
      );
      let withdrawLPTokenAmount;
      if (poolData.totalLocked.gt(0) && tokenInputSum.gt(0)) {
        try {
          // FIX THIS LATER
          withdrawLPTokenAmount = await swapContract.calculateTokenAmount(
            account,
            POOL.poolTokens.map(
              ({ symbol }) =>
                withdrawFormState.tokenInputs[symbol].valueSafeXXXXXX
            ),
            false
          );
        } catch {
          withdrawLPTokenAmount = tokenInputSum;
        }
      } else {
        // when pool is empty, estimate the lptokens by just summing the input instead of calling contract
        withdrawLPTokenAmount = tokenInputSum;
      }
      // setEstWithdrawBonus(
      //   calculatePriceImpact(
      //     withdrawLPTokenAmount,
      //     tokenInputSum,
      //     poolData.virtualPrice
      //   )
      // );

      // FIX THIS LATER
      setEstWithdrawBonus(
        calculatePriceImpact(
          withdrawLPTokenAmount,
          tokenInputSum,
          poolData.virtualPrice
        )
      );
    }
    calculateWithdrawBonus();
  }, [
    poolData,
    withdrawFormState,
    swapContract,
    userShareData,
    account,
    POOL.poolTokens,
  ]);
  async function onConfirmTransaction() {
    const {
      withdrawType,
      tokenInputs,
      lpTokenAmountToSpend,
    } = withdrawFormState;
    await approveAndWithdraw({
      tokenFormState: tokenInputs,
      withdrawType,
      lpTokenAmountToSpend,
    });
    updateWithdrawFormState({ fieldName: "reset", value: "reset" });
  }

  const tokensData = React.useMemo(
    () =>
      POOL.poolTokens.map(({ name, symbol, icon, decimals }) => ({
        name,
        symbol,
        icon,
        decimals,
        inputValue: withdrawFormState.tokenInputs[symbol].valueRaw,
      })),
    [withdrawFormState, POOL.poolTokens]
  );

  const reviewWithdrawData = {
    withdraw: [],
    rates: [],
    slippage: formatSlippageToString(slippageSelected, slippageCustom),
    priceImpact: estWithdrawBonus,
  };
  POOL.poolTokens.forEach(({ name, decimals, icon, symbol }) => {
    if (BigNumber.from(withdrawFormState.tokenInputs[symbol].valueSafe).gt(0)) {
      reviewWithdrawData.withdraw.push({
        name,
        value: commify(
          formatUnits(withdrawFormState.tokenInputs[symbol].valueSafe, decimals)
        ),
        icon,
      });
    }
  });

  return (
    <>
      <Head>
        <title>Withdraw</title>
      </Head>
      <WithdrawPage
        title={poolName}
        reviewData={reviewWithdrawData}
        tokensData={tokensData}
        poolData={poolData}
        historicalPoolData={null}
        myShareData={userShareData}
        formStateData={withdrawFormState}
        onConfirmTransaction={onConfirmTransaction}
        onFormChange={updateWithdrawFormState}
      />
    </>
  );
}

export default Withdraw;
