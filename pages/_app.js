import { Web3ReactProvider, createWeb3ReactRoot } from "@web3-react/core";
import "tailwindcss/tailwind.css";
import { Web3Provider } from "@ethersproject/providers";
import "../styles/globals.css";
import Navbar from "../components/Navbar/Navbar";
import Sidebar from "../components/Sidebar/Sidebar";
import Footer from "../components/Footer/Footer";

import "../styles/ConnectWallet.scss";
import "../styles/Modal.scss";
import "../styles/Web3Status.scss";
import Head from "next/head";
// import "../styles/ReviewDeposit.scss";
import getLibrary from "../utils/getLibrary";
import Web3ReactManager from "../components/Web3ReactManager";
import { Provider } from "react-redux";
import store from "../state";
import ToastsProvider from "../providers/ToastsProvider";

import { useEffect, useState } from "react";
import { NetworkContextName } from "../constants";

import { useRouter } from "next/router";
// import { hotjar } from "react-hotjar";
import { useGetApiData } from "../hooks/useGetApiData";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const currentURL = "https://dopple.finance" + router.pathname;
  const twitterHandle = "Dopple Finance - Stablecoin DEX on BSC";
  const previewImage = "https://dopple.finance/images/og.jpg";
  const siteName = "Dopple Finance";
  const pageTitle = "Dopple Finance - Stablecoin DEX on BSC";
  const description =
    "Dopple Your Money! Dopple Finance is a Stablecoin Dex designed for efficiently trading stablecoins and pegged assets on the Binance Smart Chain. Liquidity Pools inlcude: BUSD, USDT, DAI, USDC & UST.";

  const [web3ReactRootCreated, setWeb3ReactRootCreated] = useState(false);

  useEffect(() => {
    // You now have access to `window`
    if (!web3ReactRootCreated) {
      window.web3ProviderNetwork = createWeb3ReactRoot(NetworkContextName);
      setWeb3ReactRootCreated(true);
    }

    if (window && window.ethereum) {
      window.ethereum.autoRefreshOnNetworkChange = false;
    }
  }, []);

  if (!web3ReactRootCreated) return <>Loading...</>;

  // Hotjar
  // hotjar.initialize(2526626, 6);

  return (
    <>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />

        {/* Twitter */}
        <meta name="twitter:card" content="summary" key="twcard" />
        <meta name="twitter:creator" content={twitterHandle} key="twhandle" />

        {/* Open Graph */}
        <meta property="og:url" content={currentURL} key="ogurl" />
        <meta property="og:image" content={previewImage} key="ogimage" />
        <meta property="og:site_name" content={siteName} key="ogsitename" />
        <meta property="og:title" content={pageTitle} key="ogtitle" />
        <meta property="og:description" content={description} key="ogdesc" />

        <link rel="icon" href="/favicon.ico" />
        {/* Global Site Tag (gtag.js) - Google Analytics */}
        <script
          async
          src={`https://www.googletagmanager.com/gtag/js?id=${process.env.NEXT_PUBLIC_REACT_APP_GOOGLE_ANALYTICS_MEASUREMENT_ID}`}
        />
        {/* Start of dopple Zendesk Widget script */}
        <script
          id="ze-snippet"
          src="https://static.zdassets.com/ekr/snippet.js?key=924942e8-f192-4f88-adda-bbb62235d4e3"
        >
          {" "}
        </script>
        {/* End of dopple Zendesk Widget script */}
        <script
          dangerouslySetInnerHTML={{
            __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.NEXT_PUBLIC_REACT_APP_GOOGLE_ANALYTICS_MEASUREMENT_ID}');
          `,
          }}
        />
      </Head>
      <window.web3ProviderNetwork getLibrary={getLibrary}>
        <Web3ReactProvider getLibrary={getLibrary}>
          <Provider store={store}>
            <Web3ReactManager>
              <ToastsProvider>
                <Navbar />
                <Sidebar />
                <div className="main-container">
                  <Component {...pageProps} />
                </div>
                <Footer />
              </ToastsProvider>
            </Web3ReactManager>
          </Provider>
        </Web3ReactProvider>
      </window.web3ProviderNetwork>
    </>
  );
}

export function reportWebVitals({ id, name, label, value }) {
  // Use `window.gtag` if you initialized Google Analytics as this example:
  // https://github.com/vercel/next.js/blob/canary/examples/with-google-analytics/pages/_document.js
  if (window.gtag) {
    window.gtag("event", name, {
      event_category:
        label === "web-vital" ? "Web Vitals" : "Next.js custom metric",
      value: Math.round(name === "CLS" ? value * 1000 : value), // values must be integers
      event_label: id, // id unique to current page load
      non_interaction: true, // avoids affecting bounce rate.
    });
  }
}

export default MyApp;
