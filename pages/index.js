import Head from "next/head";
import Link from "next/link";
import Roadmap from "../components/Timeline/Roadmap";
import Image from "next/image";

// contract
import { useEffect, useState } from "react";
import Skeleton from "react-loading-skeleton";
import { useSelector } from "react-redux";
import { useGetApiData } from "../hooks/useGetApiData";
export default function Home() {
  const [dopStats, setDopStats] = useState({});
  const [tvl, setTvl] = useState();
  const [darkMode, setDarkMode] = useState(false);

  const [aprList, setAprList] = useState();

  useGetApiData();

  const { apiData } = useSelector((state) => state.user);
  const dopPrice = apiData?.twindexPrice?.dopPrice;

  async function updateTotalValueLock() {
    setAprList(
      apiData.doppleStableCoinStakingData.concat(
        apiData.doppleTwindexStakingData
      )
    );
    setTvl(apiData.doppleData.totalTvl);
    updateDopStats(apiData.doppleData);
  }

  async function updateDopStats(doppleData) {
    setDopStats({
      dopple_price_usd: doppleData?.dopPrice,
      total_trading_volume: doppleData?.totalTradingVolume,
      dopple_total_supply: doppleData?.totalSupply,
      dopple_max_supply: doppleData?.maxSupply,
      dopple_per_block: doppleData?.dopPerBlock,
      market_cap: doppleData?.marketCap,
    });
  }

  useEffect(() => {
    updateTotalValueLock();
  }, [dopPrice]);

  // toggle darkmode
  const toggleDarkmode = () => {
    let htmlClasses = document.querySelector("html").classList;
    if (localStorage.theme == "dark") {
      htmlClasses.remove("dark");
      localStorage.removeItem("theme");
      setDarkMode(false);
    } else {
      htmlClasses.add("dark");
      localStorage.theme = "dark";
      setDarkMode(true);
    }
  };

  // remember darkmode
  useEffect(() => {
    document.querySelector("html").classList.add("dark");
    setDarkMode(true);
  }, []);

  return (
    <>
      <Head>
        <title>Dopple Finance</title>
      </Head>

      <div className="absolute-panel absolute top-0 left-0">
        {/* header */}
        <div className="container">
          <div className="py-5 mb-2 flex justify-between items-center relative z-10 sm:px-0 px-2">
            <Link href="/">
              <a>
                <div className="flex items-center cursor-pointer">
                  {/* dark */}
                  <div className="dark:flex hidden">
                    <div className="lg:flex hidden">
                      <Image
                        src="/images/logo_with_text.svg"
                        height="68"
                        width="184px"
                      />
                    </div>
                    <div className="lg:hidden flex">
                      <Image
                        src="/images/logo_with_text.svg"
                        height="54px"
                        width="120px"
                      />
                    </div>
                  </div>

                  {/* light */}
                  <div className="dark:hidden flex">
                    <div className="lg:flex hidden">
                      <Image
                        src="/images/logo_with_text_light.svg"
                        height="68"
                        width="184px"
                      />
                    </div>
                    <div className="lg:hidden flex">
                      <Image
                        src="/images/logo_with_text_light.svg"
                        height="54px"
                        width="120px"
                      />
                    </div>
                  </div>
                </div>
              </a>
            </Link>
            <div className="flex justify-end">
              {/* lightmode darkmode */}
              <div
                onClick={() => toggleDarkmode()}
                className="mobile-menu-button mr-3 w-10 sm:px-2 py-2 ml-2 dark:bg-dark-700 bg-white rounded-lg cursor-pointer dark:bg-dark-700 dark:border-0 sm:border-0 flex justify-center items-center"
              >
                {darkMode ? (
                  <Image
                    src="/images/icons/darkmode-icon.svg"
                    width="20px"
                    height="20px"
                  />
                ) : (
                  <Image
                    src="/images/icons/lightmode-icon.svg"
                    width="20px"
                    height="20px"
                  />
                )}
              </div>
              <div className="flex items-center">
                <Link href="/Swap">
                  <a>
                    <button className="focus:outline-none  px-8 py-2 text-white flex items-center rounded-lg shadow-sm  bg-gradient-primary ">
                      Launch App
                    </button>
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        {/* header */}
        {/* body */}
        <div className="w-full h-full flex items-center top-0 justify-center py-28 sm:py-28 mb-8 sm:px-0 px-4">
          <div className="text-3xl sm:text-5xl font-bold text-center">
            <div className="dark:text-white">
              <span className="text-blue-400">Stablecoin </span> DeFi Ecosystem
              on BSC
            </div>

            <div className="dark:text-steel-300 text-lg font-normal text-steel-400 my-6">
              Swap your stablecoins at the best rate. Stake your stablecoins to
              earn high yield.
            </div>
            <div className="flex gap-4 text-sm font-bold justify-center my-8">
              <Link href="/Swap">
                <a>
                  <button className="bg-gradient-primary focus:outline-none  shadow-sm rounded-md px-8 py-2 text-white">
                    Launch app
                  </button>
                </a>
              </Link>
              <Link href="https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28">
                <a target="_blank">
                  <button className="dark:bg-dark-700  focus:outline-none ml-2 font-bold bg-white shadow-sm rounded-md px-8 py-2 text-blue-400">
                    Buy Dopple
                  </button>
                </a>
              </Link>
            </div>
          </div>
        </div>
        {/* body */}

        <div className="container">
          <div className="sm:px-0 px-4">
            {darkMode ? (
              <div className="grid sm:grid-cols-3 gap-3">
                <CardDetail
                  title="Total Value Locked"
                  value={tvl}
                  icon={"/images/landing/tvl-dark-icon.svg"}
                />
                <CardDetail
                  title="Total Trading Volume"
                  value={dopStats?.total_trading_volume}
                  icon={"/images/landing/trading-dark-icon.svg"}
                />
                <CardDetail
                  title="DOP Market Cap"
                  value={dopStats?.market_cap}
                  icon={"/images/landing/market-cap-dark-icon.svg"}
                />
              </div>
            ) : (
              <div className="grid sm:grid-cols-3 gap-3">
                <CardDetail
                  title="Total Value Locked"
                  value={tvl?.tvl_sum}
                  icon={"/images/landing/tvl-icon.svg"}
                />
                <CardDetail
                  title="Total Trading Volume"
                  value={dopStats?.total_trading_volume}
                  icon={"/images/landing/trading-icon.svg"}
                />
                <CardDetail
                  title="DOP Market Cap"
                  value={dopStats?.market_cap}
                  icon={"/images/landing/market-cap-icon.svg"}
                />
              </div>
            )}
          </div>
        </div>

        <div className="dark:bg-dark-900 w-full bg-white mt-12">
          <div className="container py-10">
            <div className="sm:grid grid-cols-2">
              <div className="w-full p-8">
                <Image
                  src="/images/landing/banner.svg"
                  height="600"
                  width="600"
                />
              </div>
              <div className="flex items-center justify-center p-2 px-4 mb-8 sm:mb-0">
                <div>
                  <div className="text-3xl text-blue-400">Protocol</div>
                  <div className="dark:text-white text-steel-400 text-xl sm:text-3xl py-4">
                    Stablecoin Swap Algorithm
                  </div>
                  <div className="text-steel-300 text-lg leading-8">
                    Designed for efficiently trading stablecoins and pegged
                    assets on the Binance Smart Chain.
                  </div>
                  <div className="flex items-center mt-5">
                    <Link href="/Swap">
                      <a>
                        <button className="focus:outline-none  px-8 py-2 text-white flex items-center rounded-lg shadow-sm  bg-gradient-primary ">
                          Launch Swap
                        </button>
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>

            <div className="grid sm:grid-cols-2">
              <div className="flex items-center justify-center px-4">
                <div className="sm:mb-24">
                  <div className="text-3xl text-blue-400">Farm</div>
                  <div className="dark:text-white text-steel-400  text-xl sm:text-3xl py-4 leading-10">
                    Earn high yield on your stablecoins and liquidity
                  </div>
                  <div className="text-steel-300 text-lg leading-8">
                    Liquidity Providers earn fees in stablecoin currency for
                    every swap made through the underlying liquidity pools.
                    Liquidity Providers also receive LP tokens, which can be
                    staked to earn high yields.
                  </div>
                </div>
              </div>
              <div className="p-2 px-4">
                <StakeCardItem
                  icon="/images/icons/dop.svg"
                  title="DOP"
                  apr={
                    aprList
                      ? aprList.find(
                          (data) =>
                            (data.data.tokenNumberInPool === 1
                              ? data.data.tokenAddress
                              : data.data.lpAddress) ===
                            "0x844FA82f1E54824655470970F7004Dd90546bB28"
                        ).apr
                      : null
                  }
                />
                <StakeCardItem
                  icon="/images/icons/dop-busd.svg"
                  title="DOP-BUSD LPs"
                  apr={
                    aprList
                      ? aprList.find(
                          (data) =>
                            (data.data.tokenNumberInPool === 1
                              ? data.data.tokenAddress
                              : data.data.lpAddress) ===
                            "0xC789F6C658809eED4d1769a46fc7BCe5dbB8316E"
                        ).apr
                      : null
                  }
                />
                <StakeCardItem
                  icon="/images/icons/dop-bnb.svg"
                  title="DOP-BNB LPs"
                  apr={
                    aprList
                      ? aprList.find(
                          (data) =>
                            (data.data.tokenNumberInPool === 1
                              ? data.data.tokenAddress
                              : data.data.lpAddress) ===
                            "0x64de2eb6Af1C101f4053BE1DEa89Cb8455A64f0D"
                        ).apr
                      : null
                  }
                />
                <StakeCardItem
                  icon="/images/icons/busd-usdt.svg"
                  title="2 Pools LPs"
                  apr={
                    aprList
                      ? aprList.find(
                          (data) =>
                            (data.data.tokenNumberInPool === 1
                              ? data.data.tokenAddress
                              : data.data.lpAddress) ===
                            "0x124166103814E5a033869c88e0F40c61700Fca17"
                        ).apr
                      : null
                  }
                />
                <div className="w-full flex items-center justify-center mt-10">
                  <a href="/Stake">
                    <button className="rounded focus:outline-none bg-blue-400 shadow-sm rounded-sm text-center px-2 sm:px-8 py-2 text-white">
                      View More
                    </button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="w-full w-gradient-blue">
          <div className="container pb-10">{/* <Roadmap></Roadmap> */}</div>
        </div>

        <div className="dark:bg-dark-900 w-full bg-white pt-8">
          <div className="container py-8">
            <div className="w-full grid grid-cols-1 items-center justify-center rounded-lg shadow p-14 dark:bg-dark-700">
              <Image src="/images/logo.svg" width="120" height="120" />
              <div className="text-black dark:text-white text-2xl my-8 text-center">
                Get the most out of your stablecoins
              </div>
              <div className="flex items-center justify-center">
                <Link href="/Swap">
                  <a>
                    <button className="rounded focus:outline-none bg-blue-400 shadow-sm rounded-sm text-center px-2 sm:px-8 py-2 text-white">
                      Launch App
                    </button>
                  </a>
                </Link>
              </div>
            </div>

            <div className="dark:text-white text-center font-bold text-steel-400 text-5xl  mt-16 mb-12">
              Join our community
            </div>

            <div className="grid sm:grid-cols-2 my-4 gap-6 mb-12">
              <a href="https://twitter.com/dopplefi" target="_blank">
                <div className="dark:bg-dark-700 bg-white shadow rounded-lg p-6 py-12 text-center">
                  <Image
                    src="/images/icons/blue-twitter-icon.svg"
                    width="80"
                    height="80"
                  />
                  <div className="dark:text-white font-bold text-steel-400 my-2 text-xl">
                    Twitter
                  </div>
                  <div className="dark:text-white text-steel-400">
                    Dopple Finance #BSC
                  </div>
                </div>
              </a>{" "}
              <a href="https://t.me/dopplefiAnn" target="_blank">
                <div className="dark:bg-dark-700 shadow rounded-lg p-6 py-12 text-center">
                  <Image
                    src="/images/icons/blue-telegram-icon.svg"
                    width="80"
                    height="80"
                  />
                  <div className="dark:text-white font-bold text-steel-400 my-2 text-xl">
                    Telegram
                  </div>
                  <div className="dark:text-white text-steel-400">
                    Dopple Finance Community
                  </div>
                </div>
              </a>{" "}
            </div>
          </div>

          <div className="container ">
            <div className="flex justify-between items-center pb-6 pt-4 gap-2">
              <div className="flex items-center">
                <Image
                  src="/images/icons/bsc-icon.svg"
                  width="30"
                  height="40"
                />
                <span className="dark:text-white ml-3 text-steel-400 text-sm sm:text-lg">
                  Binance Smart Chain
                </span>
              </div>
              <div className="flex gap-4">
                <Link href="https://github.com/DoppleFinance/dopple-contract">
                  <a target="_blank">
                    <Image
                      src="/images/icons/github-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>
                <Link href="https://www.coingecko.com/en/coins/dopple-finance">
                  <a target="_blank">
                    <Image
                      src="/images/icons/coin-gecko-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>
                <Link href="https://coinmarketcap.com/en/currencies/dopple-finance/">
                  <a target="_blank">
                    <Image
                      src="/images/icons/coin-maket-cap-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>

                <Link href="https://t.me/dopplefiAnn">
                  <a target="_blank">
                    <Image
                      src="/images/icons/announcement-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>
                <Link href="https://t.me/dopplefi">
                  <a target="_blank">
                    <Image
                      src="/images/icons/telegram-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>
                <Link href="https://twitter.com/dopplefi">
                  <a target="_blank">
                    <Image
                      src="/images/icons/twitter-icon.svg"
                      width="38"
                      height="38"
                    />
                  </a>
                </Link>
              </div>
            </div>
          </div>
        </div>

        {/* <div className="container pt-20 pb-20">
         
        </div> */}
      </div>
    </>
  );
}
function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(0);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
const StakeCardItem = ({ icon, title, apr }) => {
  return (
    <div className="dark:text-white dark:bg-dark-700 dark:border-dark-700 border shadow-lg flex justify-between items-center px-6 py-3 my-4 rounded-lg">
      <div className="flex">
        {title == "DOP" ? (
          <div className="my-2">
            <Image src={icon} height="34" width="60" />
          </div>
        ) : (
          <Image src={icon} height="60" width="60" />
        )}
        <div className="ml-3 flex items-center">{title}</div>
      </div>
      <div>
        <div className="text-green-500">
          {apr ? apr.toFixed(0) : <Skeleton count="1" />}%
        </div>
        <div className="steel-300 text-right">APR</div>
      </div>
    </div>
  );
};

const CardDetail = ({ title, value, icon }) => {
  return (
    <div className="dark:bg-dark-700 flex justify-between bg-white p-6 rounded-lg shadow-sm">
      <div>
        <div className="text-sm mb-2 dark:text-white">{title}</div>
        <div className="sm:text-5xl text-3xl text-blue-400 font-bold">
          {value ? "$" + formatNumber(value) + "M" : <Skeleton count="1" />}
        </div>
      </div>
      <div className="flex justify-center items-center">
        <Image src={icon} width="38" height="38" />
      </div>
    </div>
  );
};

function formatNumber(value) {
  value = parseFloat(value).toFixed(0);
  return value?.substr(0, value?.length - 6);
}
