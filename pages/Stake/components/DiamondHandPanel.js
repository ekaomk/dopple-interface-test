import Image from "next/image";
import React, { useState, useEffect, useCallback } from "react";
import ConfirmTransaction from "../../../components/ConfirmTransaction";
import Modal from "../../../components/Modal";
import { BigNumber } from "@ethersproject/bignumber";
import { weiToFixed } from "../../../utils/math-helpers";
import { useWeiAmount } from "../../../hooks/useWeiAmount";
import DIVIDEND_ABI from "../../../constants/abis/dividend.json";
import ERC20_ABI from "../../../constants/abis/ERC20.abi.json";
import { useDividendContract } from "../../../hooks/useContract";
import { MaxUint256 } from "@ethersproject/constants";
import usePoller from "../../../hooks/usePoller";
import { data } from "autoprefixer";
import { useSelector } from "react-redux";

const DiamondHandPanel = ({
  index,
  allowanceResult,
  balanceResult,
  depositBalanceResult,
  stakingRewardResult,
  dividend,
  updateAllData,
  dopPrice,
  dopApiData,
}) => {
  const [show, setToggle] = useState(false);
  const [currentModal, setCurrentModal] = useState(null);
  const [isDataLoadFail, setIsDataLoadFail] = useState(isDataLoadFail);

  const [depositInput, setDepositInput] = useState("");
  const [withdrawInput, setWithdrawInput] = useState("");
  const depositWei = useWeiAmount(depositInput);
  const withdrawWei = useWeiAmount(withdrawInput);

  const [allowance, setAllowance] = useState(BigNumber.from(0));
  const [balance, setBalance] = useState(BigNumber.from(0));
  const [depositBalance, setDepositBalance] = useState(BigNumber.from(0));
  const [stakingReward, setStakingReward] = useState(BigNumber.from(0));
  const [totalValueLock, setTotalValueLock] = useState("loading...");
  const [apr, setApr] = useState("loading...");

  const { apiData } = useSelector((state) => state.user);

  async function updateDataFromAPI() {
    if (apiData) {
      setTotalValueLock(
        apiData.diamondHand[dividend.name.toLowerCase()].tvl.toFixed(0)
      );
      setApr(apiData.diamondHand[dividend.name.toLowerCase()].apr.toFixed(0));
    }
  }

  useEffect(() => {
    updateDataFromAPI();
  }, [apiData]);

  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setAllowance(
      allowanceResult.length
        ? allowanceResult[index].allowance
        : BigNumber.from(0)
    );
    setBalance(
      balanceResult.length ? balanceResult[index].balance : BigNumber.from(0)
    );
    setDepositBalance(
      depositBalanceResult.length
        ? depositBalanceResult[index].amount
        : BigNumber.from(0)
    );
    setStakingReward(
      stakingRewardResult.length
        ? stakingRewardResult[index].reward
        : BigNumber.from(0)
    );
  }, [
    allowanceResult,
    balanceResult,
    depositBalanceResult,
    stakingRewardResult,
  ]);
  const depositTokenContract = useDividendContract(
    dividend.deposit_token_address,
    ERC20_ABI,
    true
  );
  const dividendContract = useDividendContract(
    dividend.address,
    DIVIDEND_ABI,
    true
  );

  const approve = async () => {
    const transaction = await depositTokenContract?.approve(
      dividend.address,
      MaxUint256
    );
    await transaction.wait();
  };
  const deposit = async () => {
    const transaction = await dividendContract.deposit(depositWei);
    await transaction.wait();
  };
  const withdraw = async () => {
    const transaction = await dividendContract.withdraw(withdrawWei);
    await transaction.wait();
  };
  const claimReward = async () => {
    const transaction = await dividendContract.harvest();
    await transaction.wait();
  };
  const disableDeposit = !(
    depositInput &&
    BigNumber.from(depositWei).gt(0) &&
    balance.gte(BigNumber.from(depositWei)) &&
    !allowance.isZero()
  );
  const disableWithdraw = !(
    withdrawInput &&
    BigNumber.from(withdrawWei).gt(0) &&
    depositBalance.gte(BigNumber.from(withdrawWei))
  );
  const disableClaim = stakingReward.isZero();
  // Ui handler
  const handleApproveBtn = async () => {
    setLoading(true);
    approve()
      .then(() => {
        updateAllData();
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleDepositBtn = async () => {
    setLoading(true);
    deposit()
      .then(() => {
        updateAllData();
      })
      .finally(() => {
        setLoading(false);
        setDepositInput("");
      });
  };

  const handleWithdrawBtn = async () => {
    setLoading(true);
    withdraw()
      .then(() => {
        updateAllData();
      })
      .finally(() => {
        setLoading(false);
        setWithdrawInput("");
      });
  };

  const handleClaimBtn = async () => {
    setLoading(true);
    claimReward()
      .then(() => {
        updateAllData();
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const handleMaxDepositBtn = () => {
    setDepositInput(weiToFixed(balance.toString(), 18));
  };

  const handleMaxWithdrawBtn = () => {
    setWithdrawInput(weiToFixed(depositBalance.toString(), 18));
  };

  return (
    <>
      <div className="diamond-hand-header mt-5 text-sm sm:text-base dark:text-white border dark:border-0 rounded-xl px-2 sm:px-6 py-6 dark:border-dark-500">
        {/* header */}
        <div
          className="grid grid-cols-5 cursor-pointer"
          onClick={() => setToggle(!show)}
        >
          <div className="col-span-2 text-center flex items-center">
            <div className="sm:block hidden pt-1">
              <Image src={dividend.icon} width="54" height="54" />
            </div>
            <div className="sm:hidden block">
              <Image src={dividend.icon} width="34" height="34" />
            </div>
            <div className="ml-4">
              <div className="text-xs sm:text-lg text-left">
                {dividend.name} Token Holders
              </div>
              <div className="text-xs sm:text-sm text-blue-400 text-left">
                <a href={dividend.add_lp_url} target="_blank">
                  Buy {dividend.name}
                </a>
              </div>
            </div>
          </div>
          <div className="col-span-1 text-center flex items-center justify-center">
            <div className="rounded-lg sm:rounded-xl text-white bg-orange-500 px-1 sm:px-2 ml-1 sm:px-3 sm:text-base text-xs py-2">
              {apr}%
            </div>
          </div>
          <div className="col-span-1 text-center flex items-center justify-center">
            <div className="">
              <div className="dark:text-white text-xs sm:text-lg">
                {numberWithCommasZeroFixed(totalValueLock)}
              </div>
              <div className="text-steel-300 text-xs sm:text-sm">
                {dividend.name}
              </div>
            </div>
          </div>
          <div className="col-span-1 text-center flex items-center justify-center">
            <div className="">
              <div className="">
                <div className="text-blue-400 text-xs sm:text-lg">
                  {" "}
                  {stakingReward.isZero()
                    ? "0.00"
                    : weiToFixed(stakingReward.toString(), 2)}
                </div>
                <div className="text-steel-300 text-xs sm:text-sm">DOP</div>
              </div>
            </div>
          </div>
        </div>
        {/* header */}

        {/* body */}
        <div className={`${show ? "active" : ""} card-toggle `}>
          <div className="dark:bg-steel-500 py-3 sm:py-0 grid grid-cols-8  bg-gray-100 rounded-lg mt-4">
            {/* left */}
            <div className="col-span-full sm:col-span-3 sm:py-6 px-3 sm:pl-6 sm:pr-3 grid grid-cols-1 sm:gap-y-1">
              <div className="flex justify-between items-center">
                <div className="dark:text-white font-bold text-gray-500 text-md">
                  Stake
                </div>
                <div className="text-gray-400 text-xs sm:text-sm flex">
                  <div>Balance: </div>
                  <div className="ml-1 cursor-pointer text-blue-500">
                    {balance.isZero() ? "0" : weiToFixed(balance.toString(), 2)}
                  </div>
                </div>
              </div>

              <div className="">
                <input
                  type="number"
                  placeholder="0"
                  className="dark:bg-dark-400 dark:text-white focus:outline-none focus:ring focus:border-blue-200 rounded-md p-2 w-full mb-1 sm:mb-0 sm:my-3"
                  value={depositInput}
                  onChange={(e) => setDepositInput(e.target.value)}
                />
              </div>
              <div className="-mt-8 sm:-mt-10 pr-1">
                <button
                  className="focus:outline-none text-blue-400 px-5 py-1 rounded-md float-right relative z-1"
                  onClick={handleMaxDepositBtn}
                >
                  Max
                </button>
              </div>
              <div className="mt-1">
                <div className="flex gap-3">
                  <button
                    type="button"
                    className={`focus:outline-none text-center py-2 mt-1 w-full border-0 rounded-md font-bold bg-blue-400 text-white ${
                      !allowance.isZero() ? "hidden" : ""
                    }`}
                    onClick={handleApproveBtn}
                  >
                    Approve
                  </button>
                  <button
                    type="button"
                    className={`focus:outline-none text-center py-2 mt-1 w-full rounded-md font-bold  ${
                      disableDeposit
                        ? "dark:text-dark-700 text-gray-400 dark:bg-dark-500 bg-steel-200"
                        : "bg-blue-400 text-white"
                    }`}
                    disabled={disableDeposit}
                    onClick={handleDepositBtn}
                  >
                    Deposit
                  </button>
                </div>
              </div>
            </div>
            {/* center */}
            <div className="col-span-full sm:col-span-3 py-4 sm:py-6 pl-3 pr-3 grid grid-cols-1 sm:gap-y-1">
              <div className="flex items-center justify-between">
                <div className="dark:text-white font-bold text-gray-500 text-md">
                  Unstake
                </div>

                <div className="text-gray-400 text-xs sm:text-sm flex">
                  <div>Balance: </div>
                  <div className="ml-1 cursor-pointer text-blue-500">
                    {depositBalance.isZero()
                      ? "0"
                      : weiToFixed(depositBalance.toString(), 2)}
                  </div>
                </div>
              </div>

              <div className="">
                <input
                  type="number"
                  placeholder="0"
                  className="dark:bg-dark-400 dark:text-white focus:outline-none focus:ring focus:border-blue-200 rounded-md p-2 w-full mb-1 sm:mb-0 sm:my-3"
                  value={withdrawInput}
                  onChange={(e) => setWithdrawInput(e.target.value)}
                />
              </div>
              <div className="-mt-8 sm:-mt-10 pr-1">
                <button
                  className="focus:outline-none text-blue-400 px-5 py-1 rounded-md float-right relative z-1"
                  onClick={handleMaxWithdrawBtn}
                >
                  Max
                </button>
              </div>
              <div className="mt-1">
                <button
                  type="button"
                  disabled={disableWithdraw}
                  className={`focus:outline-none text-center py-2 mt-1 w-full rounded-md font-bold ${
                    disableWithdraw
                      ? "dark:text-dark-700 text-gray-400 dark:bg-dark-500 bg-steel-200"
                      : "bg-blue-400 text-white"
                  }`}
                  onClick={handleWithdrawBtn}
                >
                  Withdraw
                </button>
              </div>
            </div>
            {/* right */}
            <div className="col-span-full sm:col-span-2 text-center p-5 pl-3 grid grid-cols-1 gap-y-1">
              <div className="text-total-reward text-3xl font-bold text-blue-500 pt-4 flex justify-center items-center">
                {stakingReward.isZero()
                  ? "0.00"
                  : weiToFixed(stakingReward.toString(), 2)}
              </div>
              <div className="dark:text-white text-xs mb-4">
                {"(≈ $" +
                  (weiToFixed(stakingReward.toString(), 2) * dopPrice).toFixed(
                    2
                  ) +
                  ")"}
              </div>
              <button
                type="button"
                className="focus:outline-none text-center my-1 w-full py-3 sm:py-2 rounded-md  bg-gradient-primary  text-white font-bold sm:mb-2"
                disabled={disableClaim}
                onClick={handleClaimBtn}
              >
                Claim
              </button>
            </div>
          </div>
          <div className="mt-4 text-xs dark:text-gray-400 flex items-center justify-center">
            <span className="mr-1 text-center">
              A penalty fee of 1% will be applied when withdrawing your funds
              within 14 days after depositing into the Diamond Hands Pools.{" "}
              <br />
              Any following deposit extends the penalty period for another 14
              days.
            </span>
          </div>
        </div>
        {/* body */}
      </div>

      <Modal isOpen={loading} onClose={() => setCurrentModal(null)}>
        <ConfirmTransaction />
      </Modal>
    </>
  );
};

export default DiamondHandPanel;

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberWithCommasZeroFixed(x) {
  var x = Number.parseFloat(x).toFixed(0);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
