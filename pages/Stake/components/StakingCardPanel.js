import Image from "next/image";

import React, { useState, useEffect, useCallback } from "react";
import Link from "next/link";

import { Zero, MaxUint256 } from "@ethersproject/constants";

import { BigNumber } from "@ethersproject/bignumber";
import { formatUnits, parseUnits, parseEther } from "@ethersproject/units";
import {
  formatBNToPercentString,
  formatBNToString,
} from "../../../utils/index";

import ConfirmTransaction from "../../../components/ConfirmTransaction";
import Modal from "../../../components/Modal";

import usePoller from "../../../hooks/usePoller";

async function approveForStaking(
  account,
  fairLaunchContract,
  stakingTokenContract,
  deposit = true
) {
  const existingAllowance = await stakingTokenContract.allowance(
    account,
    deposit ? fairLaunchContract.address : stakingTokenContract.address
  );

  async function approve(amount) {
    try {
      const approvalTransaction = await stakingTokenContract.approve(
        deposit ? fairLaunchContract.address : stakingTokenContract.address,
        amount
      );
      await approvalTransaction.wait();
    } catch (error) {
      throw error;
    }
  }

  if (existingAllowance.lte("0")) {
    await approve(MaxUint256);
  }
}

async function deposit(
  value,
  account,
  token,
  fairLaunchContract,
  stakingTokenContract
) {
  try {
    const spendingValue = value;
    // Deposit
    console.log("depositing");

    const spendTransaction = await fairLaunchContract.deposit(
      account,
      token.pool,
      spendingValue
    );

    await spendTransaction.wait();
  } catch (error) {
    console.log(error);
  }
}

async function withdraw(
  value,
  account,
  token,
  fairLaunchContract,
  stakingTokenContract
) {
  const spendingValue = value;
  // Withdraw
  const spendTransaction = await fairLaunchContract.withdraw(
    account,
    token.pool,
    spendingValue
  );
  await spendTransaction.wait();
}

async function claimReward(fairLaunchContract, pool) {
  const harvesting = await fairLaunchContract.harvest(pool);
  await harvesting.wait();
}

const StakingCardPanel = ({
  token,
  stakingTokenContract,
  fairLaunchContract,
  doppleTokenContract,
  account,
  title,
  subtitle,
  coin_desc,
  icon_1,
  icon_2,
  onRewardChange,
  bnbContract,
  description,
  chainId,
  isPoolActive,
  updateWalletBalance,
  flagUpdateStakingBalance,
  dopPrice,
  dopApiData,
}) => {
  const [show, setToggle] = useState(false);
  const [tokenBalance, setTokenBalance] = useState(BigNumber.from("0"));
  const [stakingBalance, setStakingBalance] = useState(BigNumber.from("0"));
  const [stakeValueInput, setStakeValueInput] = useState("");
  const [unStakeValueInput, setUnStakeValueInput] = useState("");
  const [stakeValue, setStakeValue] = useState(BigNumber.from("0"));
  const [unStakeValue, setUnStakeValue] = useState(BigNumber.from("0"));
  const [totalReward, setTotalReward] = useState(BigNumber.from("0"));
  const [currentModal, setCurrentModal] = useState(null);
  const [allowanceDeposit, setAllowanceDeposit] = useState(BigNumber.from("0"));

  // const [poolWeight, setPoolWeight] = useState("...");
  const [isDataLoadFail, setIsDataLoadFail] = useState(isDataLoadFail);
  const [isStakingPoolActive, setIsStakingPoolActive] = useState(isPoolActive);

  const [apy, setApy] = useState(BigNumber.from("0"));
  const [tvl, setTvl] = useState(0);

  let fairLaunchDecimals;

  const updateBalance = async (pool, account) => {
    const userInfo = await fairLaunchContract.userInfo(pool, account);
    const doppleTokenBalance = await stakingTokenContract.balanceOf(account);

    setTokenBalance(doppleTokenBalance);
    setStakingBalance(userInfo.amount);

    const existingAllowanceDeposit = await stakingTokenContract.allowance(
      account,
      fairLaunchContract.address
    );

    setAllowanceDeposit(existingAllowanceDeposit);
  };

  usePoller(async () => {
    if (!fairLaunchContract) return;
    try {
      const pendingDopple = await fairLaunchContract.pendingDopple(
        token.pool,
        account
      );
      setTotalReward(pendingDopple);
      onRewardChange();
      setIsDataLoadFail(false);
      if (isPoolActive) setIsStakingPoolActive(true);
      else setIsStakingPoolActive(false);
    } catch (err) {
      setIsDataLoadFail(true);
    }
  }, 10000);

  const calcAPY = async (dopApiData) => {
    try {
      const poolInfo = await fairLaunchContract.poolInfo(token.pool);
      var allocPoint = poolInfo.allocPoint.toNumber();
    } catch (err) {
      var allocPoint = BigNumber.from(0);
    }
    // setPoolWeight(allocPoint / 10);
    let _apy;
    let _tvl;

    if (dopApiData) {
      const concatApiData = dopApiData.doppleStableCoinStakingData.concat(
        dopApiData.doppleTwindexStakingData
      );
      const data = concatApiData.find(
        (api) =>
          (api.data.tokenNumberInPool === 1
            ? api.data.tokenAddress
            : api.data.lpAddress) === token.addresses[chainId]
      );

      if (data) {
        _apy = data.apr;
        _tvl = data.tvl;
      }
    }

    if (_apy < Infinity && _apy > 0) {
      setApy(parseInt(_apy));
    }
    if (_tvl < Infinity && _tvl > 0) {
      setTvl(parseInt(_tvl));
    }
  };

  useEffect(() => {
    (async () => {
      calcAPY(dopApiData);
    })();
  }, [dopApiData]);

  useEffect(() => {
    (async () => {
      if (!fairLaunchContract) return;
      fairLaunchDecimals = fairLaunchContract.decimals;
      await updateBalance(token.pool, account);
    })();
  }, [account, flagUpdateStakingBalance]);

  const approving = async (deposit) => {
    setCurrentModal("confirm");
    await approveForStaking(
      account,
      fairLaunchContract,
      stakingTokenContract,
      deposit
    ).finally(() => {
      setCurrentModal(null);
    });
    await updateBalance(token.pool, account);
  };

  const staking = async (value) => {
    setCurrentModal("confirm");
    await deposit(
      value,
      account,
      token,
      fairLaunchContract,
      stakingTokenContract
    ).finally(() => {
      setCurrentModal(null);
    });
    await updateBalance(token.pool, account);
    updateWalletBalance();
  };

  const unStaking = async (value) => {
    setCurrentModal("confirm");
    await withdraw(
      value,
      account,
      token,
      fairLaunchContract,
      stakingTokenContract
    ).finally(() => {
      setCurrentModal(null);
    });
    await updateBalance(token.pool, account);
    updateWalletBalance();
  };

  return (
    <>
      {subtitle && (
        <div className="text-sm text-gray-400 dark:text-white ">
          ({subtitle})
        </div>
      )}
      {/* start header */}

      <div className="dark:bg-dark-700 dark:border-0 border-t relative dark:border-t-dark-500">
        {!isStakingPoolActive ? (
          <>
            <div className="bg-white absolute w-full h-full z-50 bg-opacity-40 rounded backdrop-filter flex items-center justify-center">
              <div className="text-xl rounded-lg text-white bg-gradient-to-r from-blue-400 to-green-400 px-8 py-3 sm:ml-1 sm:px-14">
                Coming Soon...
              </div>
            </div>
          </>
        ) : null}
        <div
          className="grid grid-cols-5  sm:gap-2 cursor-pointer"
          onClick={() => setToggle(!show)}
        >
          <div className="flex col-span-2 py-2 sm:py-6 sm:px-4 justify-between items-center">
            <div className="text-xs flex items-center">
              <div className="flex mx-2 sm:mx-0 sm:mr-4">
                {/* mobile */}
                <div className="flex sm:hidden relative z-10">
                  <Image src={icon_1} width="30px" height="30px" />
                </div>
                {/* desktop */}
                <div className="hidden sm:flex relative z-10">
                  <Image src={icon_1} width="60px" height="60px" />
                </div>
              </div>
              <div className="text-xs sm:text-lg font-bold dark:text-white">
                <div>
                  {coin_desc}
                  {/* <Badge text={`${poolWeight}X`} /> */}
                </div>
                <div className="font-light text-xs cursor_pointer text-blue-500 ">
                  {coin_desc?.split(" ")[0] == "Legacy" ? (
                    <span className="text-red-400">
                      <Link
                        href={description?.url
                          .toString()
                          .replace("add", "remove")}
                      >
                        <a target="_blank">
                          {description?.text.replace("Add", "Remove")}
                        </a>
                      </Link>
                    </span>
                  ) : (
                    <Link href={description?.url.toString()}>
                      <a target="_blank">{description?.text}</a>
                    </Link>
                  )}
                </div>
              </div>
            </div>
          </div>

          <div className="col-span-1 flex items-center justify-center">
            <div className="text-center text-xs px-2 sm:text-base dark:text-white font-bold py-1 rounded-lg text-black">
              APR <span className="sm:inline hidden">: </span>
              {parseInt(apy) == 0 && coin_desc?.split(" ")[0] == "Legacy" ? (
                "0"
              ) : (
                <>{parseInt(apy) == 0 ? "Calculating" : parseInt(apy) + "%"}</>
              )}
              <div className="text-xs text-center text-steel-300">
                {parseInt(apy) == 0
                  ? ""
                  : `≈ ${(parseInt(apy) / 365).toFixed(2)}% daily`}
              </div>
            </div>
          </div>

          <div className="col-span-1 text-xs sm:text-base dark:text-white flex items-center justify-center">
            {parseInt(tvl) == 0
              ? "Calculating"
              : "$" + numberWithCommasZeroFixed(tvl)}
          </div>

          <div className="text-blue-400 font-bold text-center sm:text-left text-xs sm:text-lg flex items-center justify-center">
            {numberWithCommas(
              formatBNToString(totalReward, fairLaunchDecimals)
            )}
          </div>
        </div>
        {/* end header */}

        {/* start body */}
        <div className={`${show ? "active" : ""} card-toggle`}>
          <div className="dark:bg-steel-500 py-3 sm:py-0 grid grid-cols-8 border-t dark:border-0 bg-gray-100 ">
            {/* left */}
            <div className="col-span-full sm:col-span-3 sm:py-6 px-3 sm:pl-6 sm:pr-3 grid grid-cols-1 sm:gap-y-1">
              <div className="flex justify-between items-center">
                <div className="dark:text-white font-bold text-gray-500 text-md">
                  Stake
                </div>
                <div className="text-gray-400 text-xs sm:text-sm flex">
                  <div>Balance: </div>
                  <div
                    className="ml-1 cursor-pointer text-blue-500"
                    onClick={() => {
                      setStakeValueInput(
                        formatBNToString(tokenBalance, token.decimals)
                      );
                      setStakeValue(tokenBalance);
                    }}
                  >
                    {numberWithCommas(
                      formatBNToString(tokenBalance, token.decimals)
                    )}
                  </div>
                </div>
              </div>

              <div className="">
                <input
                  type="number"
                  placeholder="0"
                  className="dark:bg-dark-400 dark:text-white focus:outline-none focus:ring focus:border-blue-200 rounded-md p-2 w-full mb-1 sm:mb-0 sm:my-3"
                  value={stakeValueInput}
                  onChange={(e) => {
                    setStakeValueInput(e.target.value);
                    const cleanedFormFromValue = e.target.value.replace(
                      /[$,]/g,
                      ""
                    );
                    if (cleanedFormFromValue)
                      setStakeValue(
                        parseUnits(cleanedFormFromValue, token.decimals)
                      );
                  }}
                  readOnly={allowanceDeposit.lte(Zero)}
                />
              </div>
              <div className="-mt-10 pr-1">
                <button
                  className="focus:outline-none text-blue-400 px-5 py-1 rounded-md float-right relative z-1"
                  onClick={() => {
                    setStakeValueInput(
                      formatBNToString(tokenBalance, token.decimals)
                    );
                    setStakeValue(tokenBalance);
                  }}
                >
                  Max
                </button>
              </div>
              <div className="mt-1">
                {allowanceDeposit.gt(Zero) ? (
                  <button
                    type="button"
                    className={`focus:outline-none text-center py-2 mt-1 w-full  rounded-md font-bold ${
                      stakeValue.gt(MaxUint256) ||
                      stakeValue.gt(tokenBalance) ||
                      stakeValue.eq(0) ||
                      stakeValueInput == ""
                        ? "text-gray-400 dark:text-gray-400 dark:bg-dark-500 bg-steel-200"
                        : "bg-blue-400 text-white"
                    }`}
                    disabled={
                      stakeValue.gt(MaxUint256) ||
                      stakeValue.gt(tokenBalance) ||
                      stakeValue.eq(0) ||
                      stakeValueInput == ""
                    }
                    onClick={async () => {
                      if (stakeValueInput != "" && stakeValue.gt(0)) {
                        await staking(stakeValue);
                        setStakeValueInput("");
                      }
                    }}
                  >
                    Deposit
                  </button>
                ) : (
                  <div className="flex gap-3">
                    <button
                      type="button"
                      className="focus:outline-none text-center py-2 mt-1 w-full border-0 rounded-md font-bold bg-blue-400 text-white"
                      onClick={() => {
                        approving(true);
                      }}
                    >
                      Approve
                    </button>
                    <button
                      type="button"
                      className={`focus:outline-none text-center py-2 mt-1 w-full bg-steel-200 rounded-md font-bold dark:text-dark-700 text-gray-400 dark:bg-dark-500`}
                      disabled={true}
                    >
                      Deposit
                    </button>
                  </div>
                )}
              </div>
              {/* check if dop dont need to show ADD / REMOVE LP */}
              <div
                className={`${
                  coin_desc == "DOP" ? "invisible" : ""
                } mt-4 text-xs dark:text-gray-400 flex items-center justify-center`}
              >
                <span className="mr-1">You don’t have LP token yet? </span>
                <Link href={description?.url.toString()}>
                  <a target="_blank">
                    <span className="text-blue-400">Add LP</span>
                  </a>
                </Link>
              </div>
            </div>
            {/* center */}
            <div className="col-span-full sm:col-span-3 py-4 sm:py-6 pl-3 pr-3 grid grid-cols-1 sm:gap-y-1">
              <div className="flex items-center justify-between">
                <div className="dark:text-white font-bold text-gray-500 text-md">
                  Unstake
                </div>

                <div className="text-gray-400 text-xs sm:text-sm flex">
                  <div>Balance: </div>
                  <div
                    className="ml-1 cursor-pointer text-blue-500"
                    onClick={() => {
                      setUnStakeValueInput(
                        formatBNToString(stakingBalance, fairLaunchDecimals)
                      );
                      setUnStakeValue(stakingBalance);
                    }}
                  >
                    {numberWithCommas(
                      formatBNToString(stakingBalance, fairLaunchDecimals)
                    )}
                  </div>
                </div>
              </div>

              <div className="">
                <input
                  type="number"
                  placeholder="0"
                  className="dark:bg-dark-400  dark:text-white focus:outline-none focus:ring focus:border-blue-200 rounded-md p-2 w-full mb-1 sm:mb-0 sm:my-3"
                  value={unStakeValueInput}
                  onChange={(e) => {
                    setUnStakeValueInput(e.target.value);
                    const cleanedFormFromValue = e.target.value.replace(
                      /[$,]/g,
                      ""
                    );
                    if (cleanedFormFromValue)
                      setUnStakeValue(
                        parseUnits(cleanedFormFromValue, token.decimals)
                      );
                  }}
                />
              </div>
              <div className="-mt-10 pr-1">
                <button
                  className=" focus:outline-none text-blue-400 px-5 py-1 rounded-md float-right relative z-1"
                  onClick={() => {
                    setUnStakeValueInput(
                      formatBNToString(stakingBalance, fairLaunchDecimals)
                    );
                    setUnStakeValue(stakingBalance);
                  }}
                >
                  Max
                </button>
              </div>
              <div className="mt-1">
                <button
                  type="button"
                  className={`focus:outline-none text-center py-2 mt-1 w-full rounded-md font-bold ${
                    unStakeValue.gt(MaxUint256) ||
                    unStakeValue.gt(stakingBalance) ||
                    unStakeValue.eq(0) ||
                    unStakeValueInput == ""
                      ? "dark:text-dark-700 text-gray-400 dark:bg-dark-500 bg-steel-200"
                      : "bg-blue-400 text-white"
                  }`}
                  disabled={
                    unStakeValue.gt(MaxUint256) ||
                    unStakeValue.gt(stakingBalance) ||
                    unStakeValue.eq(0) ||
                    unStakeValueInput == ""
                  }
                  onClick={async () => {
                    if (unStakeValueInput != "" && unStakeValue.gt(0)) {
                      await unStaking(unStakeValue);
                      setUnStakeValueInput("");
                    }
                  }}
                >
                  Withdraw
                </button>
              </div>
              {/* check if dop dont need to show ADD / REMOVE LP */}
              <div
                className={`${
                  coin_desc == "DOP" ? "invisible" : ""
                } mt-4 text-xs dark:text-gray-400 flex items-center justify-center`}
              >
                <span className="mr-1">You want remove LP token? </span>
                {description?.url.toString().search("Deposit") >= 0 ? (
                  // if staking stablecoin
                  <>
                    <Link
                      href={description?.url
                        .toString()
                        .replace("Deposit", "Withdraw")}
                    >
                      <a target="_blank">
                        <span className="text-blue-400">Remove LP</span>
                      </a>
                    </Link>
                  </>
                ) : (
                  <>
                    <Link
                      href={description?.url
                        .toString()
                        .replace("add", "remove")}
                    >
                      <a target="_blank">
                        <span className="text-blue-400">Remove LP</span>
                      </a>
                    </Link>
                  </>
                )}
              </div>
            </div>
            {/* right */}
            <div className="col-span-full sm:col-span-2 text-center p-5 pl-3 grid grid-cols-1 gap-y-1">
              <div className="text-total-reward text-3xl font-bold text-blue-500 pt-4 flex justify-center items-center">
                {numberWithCommas(
                  formatBNToString(totalReward, fairLaunchDecimals)
                )}
              </div>
              <div className="dark:text-white text-xs mb-4">
                {"(≈ $" +
                  (
                    parseFloat(
                      formatBNToString(totalReward, fairLaunchDecimals)
                    ) * dopPrice
                  ).toFixed(2) +
                  ")"}
              </div>
              <button
                type="button"
                className="focus:outline-none text-center my-1  w-full py-3 sm:py-2 rounded-md  bg-gradient-primary  text-white font-bold sm:mb-10"
                onClick={async () => {
                  setCurrentModal("confirm");
                  await claimReward(fairLaunchContract, token.pool);
                  setTotalReward(BigNumber.from("0"));
                  onRewardChange();
                  updateWalletBalance();
                  setCurrentModal(null);
                }}
              >
                Claim
              </button>
            </div>
          </div>
        </div>
        {/* end  body */}

        <Modal isOpen={!!currentModal} onClose={() => setCurrentModal(null)}>
          {currentModal === "confirm" ? <ConfirmTransaction /> : null}
        </Modal>
      </div>
    </>
  );
};

export default StakingCardPanel;

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberWithCommasZeroFixed(x) {
  var x = Number.parseFloat(x).toFixed(0);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
