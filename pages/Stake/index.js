import Head from "next/head";
import Image from "next/image";
import Reward from "../../components/Reward/Reward";

import { dayjs } from "../../utils/dateTime";
import React, { useState, useEffect, useCallback } from "react";
import Link from "next/link";
import { useActiveWeb3React } from "../../hooks";
import {
  useStakingTokenContract,
  useFairLaunchContract,
  useLPTokenContract,
  useSwapContract,
  useSwapLPContract,
  useTokenContract,
  useAllContracts,
} from "../../hooks/useContract";

import usePoller from "../../hooks/usePoller";

import {
  DOP_STAKING_TOKENS,
  STABLE_COIN_STAKING_TOKENS,
  LEGACY_PANCAKE_DOP_STAKING_TOKENS,
  DOPPLE,
  DIAMONDHAND_STAKING_LIST,
  ACTIVE_POOL_NAME,
  DOPPLE_LP_TOKEN,
} from "../../constants";

import StakingCardPanel from "./components/StakingCardPanel";
import DiamondHandPanel from "./components/DiamondHandPanel";

import Countdown from "react-countdown";

import { useCountUp } from "react-countup";

import {
  useStakingAllowance,
  useStakingBalance,
  useStakingDepositBalance,
  useStakingReward,
} from "../../hooks/diamounhand";
import DIAMONDHAND_LIST from "../../constants/daimondhand";
import { useSelector } from "react-redux";
import { useGetApiData } from "../../hooks/useGetApiData";

export default function Stake() {
  const { account, chainId, error: web3Error, library } = useActiveWeb3React();
  const fairLaunchContract = useFairLaunchContract();
  const doppleTokenContract = useTokenContract(DOPPLE);
  const allContracts = useAllContracts();
  const bnbContract = allContracts.BNB;

  const [totalUnClaimReward, setTotalUnClaimReward] = useState(0);
  const [earnDopReward, setEarnDopReward] = useState(0);
  const [flagUpdateWalletBalance, setFlagUpdateWalletBalance] = useState(true);
  const [flagUpdateStakingBalance, setFlagUpdateStakingBalance] =
    useState(true);

  useGetApiData();

  const { apiData } = useSelector((state) => state.user);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  function calUnClaimReward() {
    var allUnClaimReward = document.getElementsByClassName("text-total-reward");
    var resultTotalUnClaimReward = 0;
    for (let index = 0; index < allUnClaimReward.length; index++) {
      resultTotalUnClaimReward += parseFloat(
        allUnClaimReward[index].textContent.replace(",", "")
      );
    }
    setTotalUnClaimReward(resultTotalUnClaimReward);
  }

  const updateWalletBalance = () => {
    setFlagUpdateWalletBalance(!flagUpdateWalletBalance);
  };

  const updateStakingBalance = () => {
    setFlagUpdateStakingBalance(!flagUpdateStakingBalance);
  };

  const getNearestBuyBackDate = () => {
    const now = dayjs.utc();
    const hour = now.hour();

    const startOfToday = now.startOf("date");

    if (hour < 5) {
      return startOfToday.add(5, "hours"); // 5AM UTC
    }
    if (hour >= 5 && hour < 13) {
      return startOfToday.add(13, "hours"); // 1PM UTC
    }
    if (hour >= 13 && hour < 21) {
      return startOfToday.add(21, "hours"); // 9PM UTC
    }
    return startOfToday.add(29, "hours"); // 5AM UTC (next day)
  };

  const nearestBuyBackDate = getNearestBuyBackDate();

  const { countUp, start, pauseResume, reset, update } = useCountUp({
    start: 0,
    end: 0,
    delay: 1000,
    duration: 2,
    decimals: 4,
  });

  async function updateSumReward() {
    const response = await fetch("https://api.twindex.com/");
    const apiDataTwindex = await response.json();
    update(apiDataTwindex.sum_dev_lp);

    setEarnDopReward(apiData?.diamondHand?.rewarder);
  }

  usePoller(async () => {
    updateSumReward();
  }, 10 * 1000);

  // Renderer callback with condition
  const renderer = ({ days, hours, minutes, seconds, completed }) => {
    // Render a countdown
    return (
      <div className="w-full">
        <div className="flex justify-between text-3xl  text-center">
          <div className="w-16">{days * 24 + hours}</div>
          <div className="w-2 font-normal">:</div>
          <div className="w-16">{minutes.toString().padStart(2, "0")}</div>
          <div className="w-2 font-normal">:</div>
          <div className="w-16">{seconds.toString().padStart(2, "0")}</div>
        </div>
        <div className="flex justify-between text-steel-300 text-sm text-center">
          <div className="w-16">HR</div>
          <div className="w-16">MIN</div>
          <div className="w-16">SEC</div>
        </div>
      </div>
    );
  };

  const { allowanceResult, update: updateAllowance } = useStakingAllowance();
  const { balanceResult, update: updateBalance } = useStakingBalance();
  const { depositBalanceResult, update: updateDepositBalance } =
    useStakingDepositBalance();
  const { stakingRewardResult, update: updateStakingReward } =
    useStakingReward();

  const updateAllData = () => {
    updateAllowance();
    updateBalance();
    updateDepositBalance();
  };
  // Update staking reward every 10 secs
  useEffect(() => {
    setInterval(updateStakingReward, 5000);
  }, []);

  // useEffect(() => {
  //   console.log("allowanceResult", allowanceResult);
  //   console.log("balanceResult", balanceResult);
  //   console.log("depositBalanceResult", depositBalanceResult);
  // }, [allowanceResult, balanceResult, depositBalanceResult]);

  return (
    <>
      <Head>
        <title>Stake</title>
      </Head>
      <div className="container p-0 sm:p-4">
        <div className="grid sm:grid-cols-2">
          <div className="sm:hidden block">
            <div className="col-span-1 dark:flex hidden">
              <Image
                src="/images/icons/diamond-hands-dark.png"
                width="400"
                height="282"
              />
            </div>
            <div className="col-span-1 dark:hidden flex">
              <Image
                src="/images/icons/diamond-hands-light.png"
                width="400"
                height="282"
              />
            </div>
          </div>

          <div className="col-span-1 mt-4 sm:mt-0 -mt-20 relative sm:px-0 px-4">
            <div className="sm:text-xl text-normal font-bold dark:text-white">
              Diamond Hands Revenue
            </div>
            <div className="text-xs dark:text-white text-steel-400 mb-3 mt-3 sm:mb-6">
              <span className="dark:opacity-80">
                We use 70% of our fee revenues to buy back DOP and redistribute
                those to our most loyal holders through the Diamond Hands pools.{" "}
                <a
                  href="https://dopple.gitbook.io/dopple-finance/products/diamond-hands"
                  target="_blank"
                  className="text-blue-400 dark:text-blue-300 underline"
                >
                  Learn more
                </a>
              </span>
            </div>

            <div className="d-inline text-4xl sm:text-5xl text-blue-400 font-bold mt-2 sm:my-4">
              {parseInt(earnDopReward) == 0 ? (
                "Loading..."
              ) : (
                <>
                  {numberWithCommas(earnDopReward)}{" "}
                  <span className="text-2xl">DOP</span>
                </>
              )}
            </div>
            <div className="text-xs mb-4 text-steel-300 dark:text-white mt-4 dark:opacity-80">
              Amount of DOP to be distributed
            </div>

            <div className="text-2xl sm:text-3xl text-blue-400 font-bold mt-2 sm:mt-5">
              {countUp == 0 ? (
                "Loading..."
              ) : (
                <>
                  <span className="sm:mr-3 mr-0">$</span>
                  {numberWithCommas(countUp)}
                </>
              )}
            </div>
            <div className="text-xs mb-4 text-steel-300 dark:text-white mt-4 dark:opacity-80">
              Value for the next buy back
            </div>

            <div className="sm:flex">
              <div className="flex items-center mr-3">
                <div className="text-normal dark:text-white font-bold mr-2 sm:mb-0 mb-2">
                  Next buy back in
                </div>
              </div>
              <div className="dark:text-white">
                <Countdown date={nearestBuyBackDate} renderer={renderer} />
              </div>
            </div>
          </div>
          <div className="sm:block hidden">
            <div className="col-span-1 dark:flex hidden">
              <Image
                src="/images/icons/diamond-hands-dark.png"
                width="852"
                height="568"
              />
            </div>
            <div className="col-span-1 dark:hidden flex">
              <Image
                src="/images/icons/diamond-hands-light.png"
                width="852"
                height="568"
              />
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md py-6 lg:pt-6 mb-12 relative mt-6 ">
          <div className="px-0 sm:px-8">
            <div className="px-4 sm:px-6 text-xs sm:text-base grid grid-cols-5 dark:text-white py-2 pt-4">
              <div className="col-span-2 text-center">Pool</div>
              <div className="col-span-1 text-center">APR</div>
              <div className="col-span-1 text-center">TVL</div>
              <div className="col-span-1 text-center">Earnings</div>
            </div>
          </div>
          <div className="px-2 sm:px-8 mb-8">
            {allowanceResult &&
              balanceResult &&
              depositBalanceResult &&
              stakingRewardResult &&
              DIAMONDHAND_LIST.map((dataObject, i) => (
                <DiamondHandPanel
                  key={i}
                  dividend={dataObject}
                  index={i}
                  allowanceResult={allowanceResult}
                  balanceResult={balanceResult}
                  depositBalanceResult={depositBalanceResult}
                  stakingRewardResult={stakingRewardResult}
                  updateAllData={updateAllData}
                  dopPrice={dopPrice}
                  dopApiData={apiData}
                />
              ))}
          </div>
        </div>
      </div>

      <div className="container">
        <Reward
          sumOfUnClaimReward={totalUnClaimReward}
          flagFromStake={true}
          flagUpdateWalletBalance={flagUpdateWalletBalance}
          updateStakingBalance={updateStakingBalance}
        />
      </div>
      <div className="container">
        <div className="text-center font-bold text-4xl pt-10 mt-10 pb-10 mb-5 dark:text-white">
          <span className="text-blue-400">Stake</span> your LP tokens to earn{" "}
          <span className="text-blue-400">DOP</span>
          <div className="text-sm font-normal sm:px-40 mt-2">
            Stake your LP tokens in the farming pools to earn DOP as a reward.
            LP tokens can be withdraw at any time. However, they must be staked
            to earn yield.
          </div>
        </div>
      </div>
      <div className="container">
        {/* start Available staking */}
        <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md py-6 lg:pt-6 mb-6">
          <div className="text-base sm:text-2xl font-bold dark:text-white ml-4 sm:ml-6 mb-4 ">
            <div className="flex items-center">
              <span>Staking DOPPLE</span>
              <span className="text-blue-400 text-sm ml-2">(Twindex LP)</span>
            </div>
          </div>
          <div className="text-sm sm:text-base dark:border-0 border-t grid grid-cols-5 dark:text-white dark:border-t-dark-500 py-4 ">
            <div className="col-span-2 text-center sm:text-left sm:ml-6 ">
              LP Tokens Name
            </div>
            <div className="col-span-1 text-center">APR</div>
            <div className="col-span-1 text-center">TVL</div>
            <div className="col-span-1 text-center">Earned</div>
          </div>
          {DOP_STAKING_TOKENS.map((token, id) => (
            <StakingCardPanel
              key={id}
              token={token}
              stakingTokenContract={useStakingTokenContract(token)}
              fairLaunchContract={fairLaunchContract}
              doppleTokenContract={doppleTokenContract}
              bnbContract={bnbContract}
              account={account}
              coin_desc={token.name}
              icon_1={token.icon}
              onRewardChange={calUnClaimReward}
              description={token.description}
              chainId={chainId}
              isPoolActive={token.isActive}
              dopPrice={dopPrice}
              updateWalletBalance={updateWalletBalance}
              flagUpdateStakingBalance={flagUpdateStakingBalance}
              dopApiData={apiData}
            />
          ))}
        </div>

        <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md py-6 lg:pt-6 mb-6">
          <div className="text-base sm:text-2xl font-bold dark:text-white ml-4 sm:ml-6 mb-4">
            Staking Stablecoin
          </div>

          <div className="text-sm sm:text-base dark:border-0 border-t grid grid-cols-5 dark:text-white dark:border-t-dark-500 py-4 ">
            <div className="col-span-2 text-center sm:text-left sm:ml-6 ">
              LP Tokens Name
            </div>
            <div className="col-span-1 text-center">APR</div>
            <div className="col-span-1 text-center">TVL</div>
            <div className="col-span-1 text-center">Earned</div>
          </div>

          {STABLE_COIN_STAKING_TOKENS.map((token, id) => (
            <StakingCardPanel
              key={id}
              token={token}
              stakingTokenContract={useStakingTokenContract(token)}
              fairLaunchContract={fairLaunchContract}
              doppleTokenContract={doppleTokenContract}
              bnbContract={bnbContract}
              account={account}
              coin_desc={token.name}
              icon_1={token.icon}
              onRewardChange={calUnClaimReward}
              description={token.description}
              chainId={chainId}
              isPoolActive={token.isActive}
              dopPrice={dopPrice}
              updateWalletBalance={updateWalletBalance}
              flagUpdateStakingBalance={flagUpdateStakingBalance}
              dopApiData={apiData}
            />
          ))}
        </div>

        <div className="dark:bg-dark-700 bg-white rounded-3xl shadow-md py-6 lg:pt-6 mb-6">
          <div className="text-base sm:text-2xl font-bold dark:text-white ml-4 sm:ml-6 mb-4">
            <div className="flex items-center">
              <span className="">Legacy Staking DOPPLE</span>
              {/* <span className="text-red-500 text-sm ml-2">(Pancake LP)</span> */}
            </div>
            {/* <div className="text-xs text-red-300">
              <span className="text-red-500">After 25/05/2021 5AM (GMT+7)</span>
              , Emission Rates from Pancake will be decreased to 0x.
              <br />
              Please unstake and remove liquidity of all Pancake LPs. We will
              provide new LPs on Twindex.
            </div> */}
          </div>

          <div className="text-sm sm:text-base dark:border-0 border-t grid grid-cols-5 dark:text-white dark:border-t-dark-500 py-4 ">
            <div className="col-span-2 text-center sm:text-left sm:ml-6 ">
              LP Tokens Name
            </div>
            <div className="col-span-1 text-center">APR</div>
            <div className="col-span-1 text-center">TVL</div>
            <div className="col-span-1 text-center">Earned</div>
          </div>

          {LEGACY_PANCAKE_DOP_STAKING_TOKENS.map((token, id) => (
            <StakingCardPanel
              key={id}
              token={token}
              stakingTokenContract={useStakingTokenContract(token)}
              fairLaunchContract={fairLaunchContract}
              doppleTokenContract={doppleTokenContract}
              bnbContract={bnbContract}
              account={account}
              coin_desc={token.name}
              icon_1={token.icon}
              onRewardChange={calUnClaimReward}
              description={token.description}
              chainId={chainId}
              isPoolActive={token.isActive}
              dopPrice={dopPrice}
              updateWalletBalance={updateWalletBalance}
              flagUpdateStakingBalance={flagUpdateStakingBalance}
              dopApiData={apiData}
            />
          ))}
        </div>
        {/* end Available staking */}
        <BeginnerGuide account={account} />
      </div>
    </>
  );
}

const DiamondHandList = ({ data }) => {
  return (
    <>
      <div className="mt-5 text-sm sm:text-base grid grid-cols-5 dark:text-white  border dark:border-0 rounded-xl px-2 sm:px-6 py-6 dark:border-dark-500">
        <div className="col-span-2 text-center flex items-center">
          <div className="sm:block hidden">
            <Image src={data.icon} width="54" height="54" />
          </div>
          <div className="sm:hidden block">
            <Image src={data.icon} width="34" height="34" />
          </div>
          <div className="ml-4">
            <div className="text-xs sm:text-lg text-left">
              {data.name} Token Holders
            </div>
            <div className="text-xs sm:text-sm text-blue-400 text-left">
              <a href={data.add_lp_url} target="_blank">
                Buy {data.name}
              </a>
            </div>
          </div>
        </div>
        <div className="col-span-1 text-center flex items-center justify-center">
          <div className="rounded-lg sm:rounded-xl text-white bg-pink-500 px-1 sm:px-2 ml-1 sm:px-3 sm:text-base text-xs py-2">
            {data.percentage}
          </div>
        </div>
        <div className="col-span-1 text-center flex items-center justify-center">
          <div className="">
            <div className="dark:text-white text-xs sm:text-lg">0</div>
            <div className="text-steel-300 text-xs sm:text-sm">{data.name}</div>
          </div>
        </div>
        <div className="col-span-1 text-center flex items-center justify-center">
          <div className="">
            <div className="">
              <div className="text-blue-400 text-xs sm:text-lg">0.00</div>
              <div className="text-steel-300 text-xs sm:text-sm">DOP</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

const BeginnerGuide = ({ account }) => {
  return (
    <>
      <div className="text-2xl font-bold pl-2 sm:pl-6 sm:pt-4 dark:text-white">
        Noob Tools
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-3 mb-12 dark:text-white">
        <div className="beginner-card px-4">
          <Link href="https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28">
            <a target="_blank">
              <Image
                src="/images/banner/beginner-twindex-image.png"
                width="400"
                height="300"
                objectFit="contain"
              />
              <div className="text-center -mt-8 font-bold">Buy DOP</div>
            </a>
          </Link>
        </div>
        <div className="beginner-card px-4">
          <Link
            href={`https://apeboard.finance/dashboard/${account}?chain=BSC`}
          >
            <a target="_blank">
              <Image
                src="/images/banner/beginner-apeboard-image.png"
                width="400"
                height="300"
                objectFit="contain"
              />
              <div className="text-center -mt-8 font-bold">
                View Your Portfolio
              </div>
            </a>
          </Link>
        </div>
        <div className="beginner-card transform px-4">
          <Link href="https://swap.arken.finance/#/tokens/bsc_twindex_0x844fa82f1e54824655470970f7004dd90546bb28_0xe9e7cea3dedca5984780bafc599bd69add087d56">
            <a target="_blank">
              <Image
                src="/images/banner/beginner-arken-image.png"
                width="400"
                height="300"
                objectFit="contain"
              />
              <div className="text-center -mt-8 font-bold">View DOP Chart</div>
            </a>
          </Link>
        </div>
      </div>
    </>
  );
};

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed(2);
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
