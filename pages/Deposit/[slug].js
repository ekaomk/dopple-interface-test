import Head from "next/head";
import {
  POOLS_MAP,
  ACTIVE_POOL_NAME,
  PoolName,
  Token,
  TOKENS_MAP,
  POOLS_LIST,
} from "../../constants";
import React, { ReactElement, useEffect, useState } from "react";
import {
  TokensStateType,
  useTokenFormState,
} from "../../hooks/useTokenFormState";
import { formatBNToString, shiftBNDecimals } from "../../utils";
import usePoolData, { PoolDataType } from "../../hooks/usePoolData";

import { AppState } from "../../state";
import { BigNumber } from "@ethersproject/bignumber";
import DepositPage from "../../components/DepositPage";
import PageNotFound from "../../components/PageNotFound";

import { Zero } from "@ethersproject/constants";
import { calculatePriceImpact } from "../../utils/priceImpact";
import { parseUnits } from "@ethersproject/units";
import { useActiveWeb3React } from "../../hooks";
import { useApproveAndDeposit } from "../../hooks/useApproveAndDeposit";
import { usePoolTokenBalances } from "../../state/wallet/hook";
import { useSelector } from "react-redux";
import { useSwapContract } from "../../hooks/useContract";

import { useRouter } from "next/router";

function DepositPoolsList({ poolName }) {
  const router = useRouter();

  if (!POOLS_LIST?.find((x) => x.slug == router?.query?.slug))
    return <PageNotFound />;

  // Force pool here
  poolName = POOLS_LIST.find((x) => x.slug == router.query.slug).name;
  // poolName = BTC_POOL_NAME
  const POOL = POOLS_MAP[poolName];
  const { account } = useActiveWeb3React();
  const approveAndDeposit = useApproveAndDeposit(poolName);
  const [poolData, userShareData] = usePoolData(poolName);
  const swapContract = useSwapContract(poolName);
  const [tokenFormState, updateTokenFormState] = useTokenFormState(
    POOL.poolTokens
  );
  const tokenBalances = usePoolTokenBalances(poolName);
  const [estDepositLPTokenAmount, setEstDepositLPTokenAmount] = useState(Zero);
  const [priceImpact, setPriceImpact] = useState(Zero);

  //   console.log(TOKENS_MAP);

  useEffect(() => {
    // evaluate if a new deposit will exceed the pool's per-user limit
    async function calculateMaxDeposits() {
      if (
        swapContract == null ||
        userShareData == null ||
        poolData == null ||
        account == null
      ) {
        setEstDepositLPTokenAmount(Zero);
        return;
      }
      const tokenInputSum = parseUnits(
        POOL.poolTokens
          .reduce(
            (sum, { symbol }) => sum + (+tokenFormState[symbol].valueRaw || 0),
            0
          )
          .toFixed(18),
        18
      );
      let depositLPTokenAmount;
      if (poolData.totalLocked.gt(0) && tokenInputSum.gt(0)) {
        try {
          depositLPTokenAmount = await swapContract.calculateTokenAmount(
            account,
            POOL.poolTokens.map(
              ({ symbol }) => tokenFormState[symbol].valueSafe
            ),
            true // deposit boolean
          );
        } catch {
          // For handle value is more uint 256
          return;
        }
      } else {
        // when pool is empty, estimate the lptokens by just summing the input instead of calling contract
        depositLPTokenAmount = tokenInputSum;
      }
      setEstDepositLPTokenAmount(depositLPTokenAmount);

      setPriceImpact(
        calculatePriceImpact(
          tokenInputSum,
          depositLPTokenAmount,
          poolData.virtualPrice
        )
      );
    }
    calculateMaxDeposits();
  }, [
    poolData,
    tokenFormState,
    swapContract,
    userShareData,
    account,
    POOL.poolTokens,
  ]);

  // A represention of tokens used for UI
  const tokens = POOL.poolTokens.map(({ symbol, name, icon, decimals }) => ({
    symbol,
    name,
    icon,
    max: formatBNToString(tokenBalances?.[symbol] || Zero, decimals),
    inputValue: tokenFormState[symbol].valueRaw,
  }));

  const exceedsWallet = POOL.poolTokens.some(({ symbol }) => {
    const exceedsBoolean = (tokenBalances?.[symbol] || Zero).lt(
      BigNumber.from(tokenFormState[symbol].valueSafe)
    );
    return exceedsBoolean;
  });

  async function onConfirmTransaction() {
    await approveAndDeposit(tokenFormState);
    // Clear input after deposit
    updateTokenFormState(
      POOL.poolTokens.reduce(
        (acc, t) => ({
          ...acc,
          [t.symbol]: "",
        }),
        {}
      )
    );
  }
  function updateTokenFormValue(symbol, value) {
    updateTokenFormState({ [symbol]: value });
  }
  const depositTransaction = buildTransactionData(
    tokenFormState,
    poolData,
    POOL.poolTokens,
    POOL.lpToken,
    priceImpact,
    estDepositLPTokenAmount
  );

  //   console.log(poolData);

  return (
    <>
      <Head>
        <title>Deposit : Stablecoin DEX on BSC</title>
      </Head>
      <DepositPage
        onConfirmTransaction={onConfirmTransaction}
        onChangeTokenInputValue={updateTokenFormValue}
        title={poolName}
        tokens={tokens}
        exceedsWallet={exceedsWallet}
        poolData={poolData}
        historicalPoolData={null}
        myShareData={userShareData}
        transactionData={depositTransaction}
      />
    </>
  );
}

function buildTransactionData(
  tokenFormState,
  poolData,
  poolTokens,
  poolLpToken,
  priceImpact,
  estDepositLPTokenAmount
) {
  const from = {
    items: [],
    totalAmount: Zero,
  };
  const TOTAL_AMOUNT_DECIMALS = 18;
  poolTokens.forEach((token) => {
    const { symbol, decimals } = token;
    const amount = BigNumber.from(tokenFormState[symbol].valueSafe);
    if (amount.lte("0")) return;
    const item = {
      token,
      amount,
    };
    from.items.push(item);
    from.totalAmount = from.totalAmount.add(
      shiftBNDecimals(amount, TOTAL_AMOUNT_DECIMALS - decimals)
    );
  });

  const to = {
    item: {
      token: poolLpToken,
      amount: estDepositLPTokenAmount,
    },
    totalAmount: estDepositLPTokenAmount,
  };
  const shareOfPool = poolData?.totalLocked.gt(0)
    ? estDepositLPTokenAmount
        .mul(BigNumber.from(10).pow(18))
        .div(estDepositLPTokenAmount.add(poolData?.totalLocked))
    : BigNumber.from(10).pow(18);
  return {
    from,
    to,
    priceImpact,
    shareOfPool,
  };
}

export default DepositPoolsList;
