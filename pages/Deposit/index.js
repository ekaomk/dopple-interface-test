import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

import React, { useState, useEffect } from "react";
import Skeleton from "react-loading-skeleton";
import {
  POOLS_LIST,
  STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES,
  TWO_SWAP_TOKEN_CONTRACT_ADDRESSES,
  UST_SWAP_TOKEN_CONTRACT_ADDRESSES,
  DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES,
  BTC_SWAP_TOKEN_CONTRACT_ADDRESSES,
  // staking
  DOPPLE_LP_TOKEN_CONTRACT_ADDRESSES,
  TWO_POOLS_LP_TOKEN_CONTRACT_ADDRESSES,
  UST_LP_TOKEN_CONTRACT_ADDRESSES,
  DOLLY_LP_TOKEN_CONTRACT_ADDRESSES,
  BTC_LP_TOKEN_CONTRACT_ADDRESSES,
} from "../../constants";
import { useActiveWeb3React } from "../../hooks";
import { createPopper } from "@popperjs/core";
import TVLCard from "../../components/TVLCard";
import { useSelector } from "react-redux";
import { useGetApiData } from "../../hooks/useGetApiData";

export default function DepositPage() {
  const { chainId } = useActiveWeb3React();

  useGetApiData();

  return (
    <>
      <Head>
        <title>Deposit : Stablecoin DEX on BSC</title>
      </Head>
      <div className="container mb-12">
        <div className="flex dark:text-white text-3xl font-bold mb-6 mt-4">
          <TextToolTip />
        </div>
        <div className="grid grid-cols-auto sm:grid-cols-3 gap-3">
          {POOLS_LIST.map((poolData, index) => (
            <Link href={"Deposit/" + poolData.slug} key={index}>
              <a>
                <div className="pool-item dark:bg-dark-700 dark:border-dark-700 bg-white rounded-lg p-8 hover:border ">
                  <CardPoolItem poolData={poolData} chainId={chainId} />
                </div>{" "}
              </a>
            </Link>
          ))}
        </div>
        <div className="mt-8">
          {/* <Banner /> */}
          <TVLCard></TVLCard>
        </div>
      </div>
    </>
  );
}

const CardPoolItem = ({ poolData, chainId }) => {
  const [apy, setApy] = useState(0);
  const [apr, setApr] = useState(0);
  const [totalSupply, setTotalSupply] = useState(0);

  const { apiData } = useSelector((state) => state.user);

  useEffect(() => {
    (async () => {
      switch (poolData.name) {
        case "Stable Coin Pool":
          poolData = apiData.doppleStableCoinStakingData.find(
            (data) =>
              data.data.swapAddress ===
              STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId]
          );
          setApy(parseInt(poolData.apy));
          setTotalSupply(parseInt(poolData.tvl));
          setApr(
            apiData.doppleStableCoinStakingData.find(
              (data) =>
                data.data.lpAddress ===
                DOPPLE_LP_TOKEN_CONTRACT_ADDRESSES[chainId]
            ).apr
          );
          break;
        case "Two Pool":
          poolData = apiData.doppleStableCoinStakingData.find(
            (data) =>
              data.data.swapAddress ===
              TWO_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId]
          );
          setApy(parseInt(poolData.apr));
          setTotalSupply(parseInt(poolData.tvl));
          setApr(
            apiData.doppleStableCoinStakingData.find(
              (data) =>
                data.data.lpAddress ===
                TWO_POOLS_LP_TOKEN_CONTRACT_ADDRESSES[chainId]
            ).apr
          );
          break;
        case "UST Pool":
          poolData = apiData.doppleStableCoinStakingData.find(
            (data) =>
              data.data.swapAddress ===
              UST_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId]
          );
          setApy(parseInt(poolData.apr));
          setTotalSupply(parseInt(poolData.tvl));
          setApr(parseInt(poolData.apr));
          break;
        case "DOLLY Pool":
          poolData = apiData.doppleStableCoinStakingData.find(
            (data) =>
              data.data.swapAddress ===
              DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId]
          );
          setApy(parseInt(poolData.apr));
          setTotalSupply(parseInt(poolData.tvl));
          setApr(
            apiData.doppleStableCoinStakingData.find(
              (data) =>
                data.data.lpAddress === UST_LP_TOKEN_CONTRACT_ADDRESSES[chainId]
            ).apr
          );
          break;
        case "BTC Pool":
          poolData = apiData.doppleStableCoinStakingData.find(
            (data) =>
              data.data.swapAddress ===
              BTC_SWAP_TOKEN_CONTRACT_ADDRESSES[chainId]
          );
          setApy(poolData.apr);
          setApr(
            apiData.doppleStableCoinStakingData.find(
              (data) =>
                data.data.lpAddress === BTC_LP_TOKEN_CONTRACT_ADDRESSES[chainId]
            ).apr
          );
          setTotalSupply(parseFloat(poolData.tvl));
          break;
      }
    })();
  }, []);

  return (
    <>
      <div className="flex justify-center pt-2 pb-2">
        <Image src={poolData.icon} width="62" height="62" />
      </div>
      <div className="text-blue-400 font-bold text-center my-6 text-xl">
        {poolData.title}
      </div>
      <div className="flex justify-between">
        <div>
          <APYAPRTextToolTip />

          <div className="dark:text-white text-lg font-bold text-steel-400">
            {totalSupply > 0 ? (
              <>
                {apy}% + {parseInt(apr)}%
              </>
            ) : (
              <Skeleton count="1" />
            )}
          </div>
        </div>
        <div>
          <div className="dark:text-white text-sm text-steel-300 text-right">
            Total Supply
          </div>
          <div className="dark:text-white text-lg font-bold text-steel-400">
            {totalSupply > 0 ? (
              <>${numberWithCommas(totalSupply)}</>
            ) : (
              <Skeleton count="1" />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

const APYAPRTextToolTip = () => {
  const [popoverShow, setPopoverShow] = React.useState(false);
  const btnRef = React.createRef();
  const popoverRef = React.createRef();
  const openTooltip = () => {
    createPopper(btnRef.current, popoverRef.current, {
      placement: "bottom",
    });
    setPopoverShow(true);
  };
  const closeTooltip = () => {
    setPopoverShow(false);
  };

  return (
    <div className="transactionInfoItem z-50">
      <div className="slippage dark:text-white text-sm text-steel-300 flex">
        <span className="mr-1">APY + APR</span>
        <span
          className=""
          onMouseEnter={openTooltip}
          onMouseLeave={closeTooltip}
          ref={btnRef}
        >
          <div className="dark:hidden flex">
            <Image
              src="/images/icons/info-circle-black.svg"
              width="14"
              height="14"
            />
          </div>
          <div className="dark:flex hidden">
            <Image src="/images/icons/info-circle.svg" width="14" height="14" />
          </div>
        </span>
      </div>
      <div
        className={(popoverShow ? "" : "hidden ") + "font-base"}
        ref={popoverRef}
        onMouseEnter={openTooltip}
        onMouseLeave={closeTooltip}
      >
        <div>
          <div className="relative mx-2">
            <div className="bg-gray-500 text-white font-normal text-xs rounded py-2 px-4 right-0 bottom-full text-center font-base">
              The <span className="font-bold">APY</span> is calculated from the
              stablecoins <br /> you earn generated through the swap exchange
              fees.
              <br />
              <br /> The <span className="font-bold">APR</span> is calculated
              from the DOP rewards
              <br /> you earn when staking your LP tokens in{" "}
              <Link href="Stake/">
                <a className="text-blue-400">farming pools</a>
              </Link>
              .
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const TextToolTip = () => {
  const [popoverShow, setPopoverShow] = React.useState(false);
  const btnRef = React.createRef();
  const popoverRef = React.createRef();
  const openTooltip = () => {
    createPopper(btnRef.current, popoverRef.current, {
      placement: "bottom",
    });
    setPopoverShow(true);
  };
  const closeTooltip = () => {
    setPopoverShow(false);
  };

  return (
    <div className="transactionInfoItem z-50">
      <div className="slippage text-steel-400 text-sm dark:text-white ">
        <span className="mr-2 flex dark:text-white text-base sm:text-3xl">
          Deposit your Stablecoins into Liquidity Pools
        </span>
        <div className="text-sm text-steel-300  font-normal">
          Liquidity Providers earn fees on every swap made through the
          underlying liquidity pools.
          <br /> Liquidity Providers also receive LP tokens, which can be staked
          in the farming pools to earn high yields.
        </div>
      </div>
    </div>
  );
};

function numberWithCommas(x) {
  var x = Number.parseFloat(x).toFixed();
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
