import Head from "next/head";
import WhitePanel from "../../components/layouts/WhitePanel";

// contract
import { useActiveWeb3React } from "../../hooks";
import {
  useCakeLP,
  useTokenContract,
  useFairLaunchContract,
} from "../../hooks/useContract";

import { formatBNToString } from "../../utils";
import parseStringToBigNumber from "../../utils/parseStringToBigNumber";
import usePoller from "../../hooks/usePoller";
import { useEffect, useState } from "react";
import { useCountUp } from "react-countup";
import {
  DOPPLE,
  DOP_BUSD_LP_TOKEN_ADDRESSES,
  STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES,
  TWO_SWAP_TOKEN_CONTRACT_ADDRESSES,
  UST_SWAP_TOKEN_CONTRACT_ADDRESSES,
  DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES,
  DOP_DOLLY_LP_TOKEN_CONTRACT_ADDRESSES,
} from "../../constants";
import Image from "next/image";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import { useSelector } from "react-redux";
import { useGetApiData } from "../../hooks/useGetApiData";

export default function Info() {
  const { chainId } = useActiveWeb3React();

  const [totalValueLock, setTotalValueLock] = useState("loading...");
  const [dopStats, setDopStats] = useState({});
  const cakeLP = useCakeLP();
  const dopContract = useTokenContract(DOPPLE);
  const fairLaunchContract = useFairLaunchContract();

  // second
  const autoReloadTime = 10;

  useGetApiData();

  const { countUp, start, pauseResume, reset, update } = useCountUp({
    start: 0,
    end: 0,
    delay: 1000,
    duration: 2,
  });

  const { apiData } = useSelector((state) => state.user);

  const dopPrice = apiData?.twindexPrice?.dopPrice;

  async function updateTotalValueLock() {
    updateDopStats(apiData.dopple_data);
    setTotalValueLock(apiData.total_value_lock.tvl_sum);
    update(totalValueLock);
  }

  async function updateDopStats(dopple_data) {
    setDopStats({
      dopple_price_usd: dopple_data?.dopple_price_usd,
      total_trading_volume: dopple_data?.total_trading_volume,
      dopple_total_supply: dopple_data?.dopple_total_supply,
      dopple_max_supply: dopple_data?.dopple_max_supply,
      dopple_per_block: dopple_data?.dopple_per_block,
      market_cap: dopple_data?.market_cap,
    });
  }

  useEffect(() => {
    updateTotalValueLock();
  }, [totalValueLock, dopPrice]);

  usePoller(async () => {
    updateTotalValueLock();
  }, autoReloadTime * 1000);

  return (
    <>
      <Head>
        <title>Info : Stablecoin DEX on BSC</title>
      </Head>
      <div className="container">
        <div className="bg-gradient-primary rounded-3xl shadow-md mb-6">
          <div className="info-tvl-panel p-6 lg:py-20 text-white">
            <div className="text-center">
              <div className="text-3xl mb-1 font-bold">Total Value Locked</div>
              <div className="text-4xl sm:text-7xl font-bold my-16">
                {countUp == 0 ? (
                  "loading..."
                ) : (
                  <>
                    <span className="sm:mr-3 mr-0">$</span>
                    {numberWithCommas(countUp)}
                  </>
                )}
              </div>
            </div>
            <div className="text-center">
              See more information on{" "}
              <a
                target="_blank"
                className=""
                href="https://bscscan.com/address/0x5162f992EDF7101637446ecCcD5943A9dcC63A8A#code"
              >
                {" "}
                <u>BSCScan</u>
              </a>
            </div>
          </div>
        </div>

        <WhitePanel>
          <div className="grid grid-cols-3">
            <div className="col-span-3 sm:col-span-1 flex items-center justify-center">
              <div className="sm:p-10 w-full dark:border-dark border-light sm:border-r">
                <div className="font-bold text-2xl sm:text-3xl text-steel-400 dark:text-white">
                  DOP STATS
                </div>
                <Link href="https://coinmarketcap.com/en/currencies/dopple-finance/">
                  <a target="_blank">
                    <button className="focus:outline-none btn-CoinMarketCap rounded w-full flex items-center justify-center py-2 mt-7 text-sm">
                      <Image
                        src="/images/icons/coinmarketcap-icon.svg"
                        width="22"
                        height="22"
                      />
                      <span className="ml-1">CoinMarketCap</span>
                    </button>
                  </a>
                </Link>
                <Link href="https://www.coingecko.com/en/coins/dopple-finance">
                  <a target="_blank">
                    <button className="focus:outline-none  btn-CoinGecko rounded w-full flex items-center justify-center py-2 mt-6 text-sm">
                      <Image
                        src="/images/icons/coingecko-icon.svg"
                        width="22"
                        height="22"
                      />
                      <span className="ml-1">CoinGecko</span>
                    </button>
                  </a>
                </Link>
              </div>
            </div>
            <div className="pt-2 col-span-3 sm:col-span-2 sm:px-20 flex justify-center items-center sm:mt-0 mt-4">
              <div className="w-full">
                <StatsList
                  title="DOP Price"
                  value={
                    dopStats.dopple_price_usd
                      ? "$ " + parseFloat(dopStats?.dopple_price_usd).toFixed(2)
                      : null
                  }
                />
                <StatsList
                  title="Total Trading Volume"
                  value={
                    dopStats.total_trading_volume
                      ? "$ " +
                        parseFloat(dopStats?.total_trading_volume).toFixed(0)
                      : null
                  }
                />
                <StatsList
                  title="DOP Total Supply"
                  value={
                    dopStats.dopple_total_supply
                      ? parseFloat(dopStats?.dopple_total_supply).toFixed(0)
                      : null
                  }
                />
                <StatsList
                  title="DOP Market Cap"
                  value={
                    dopStats.dopple_max_supply
                      ? "$ " + parseFloat(dopStats?.market_cap).toFixed(2)
                      : null
                  }
                />
                <StatsList
                  title="DOP Max Supply"
                  value={
                    dopStats.dopple_max_supply
                      ? parseFloat(dopStats?.dopple_max_supply).toFixed(0)
                      : null
                  }
                />
                <StatsList
                  title="DOP/Block"
                  value={dopStats?.dopple_per_block}
                />
              </div>
            </div>
          </div>
        </WhitePanel>

        <WhitePanel>
          <div className="dark:text-white sm:text-3xl mb-4 sm:mb-6 text-2xl font-bold text-center">
            Relevant Contract Addresses
          </div>
          <div className="dark:text-white sm:text-base text-sm text-center sm:px-14">
            The most up-to-date contract addresses can be found on our GitHub
            page here. <br className="hidden sm:block" />
            We also list some of the most relevant addresses below.
          </div>
          <div className="mt-12 mb-14">
            <div className="grid grid-cols-2 sm:grid-cols-5 gap-4">
              <ContractItem
                title="DOP Token"
                icon="dop-logo"
                url="https://bscscan.com/address/0x844fa82f1e54824655470970f7004dd90546bb28"
              />
              <ContractItem
                title="Dolly Token"
                icon="dolly"
                url="https://bscscan.com/address/0xff54da7caf3bc3d34664891fc8f3c9b6dea6c7a5"
              />
              <ContractItem
                title="Fair Launch"
                icon="fair-launch"
                url="https://bscscan.com/address/0xda0a175960007b0919dbf11a38e6ec52896bddbe"
              />
              <ContractItem
                title="Dev Address"
                icon="dev-icon"
                url="https://bscscan.com/address/0x36E55406FAB7a11F3fA030fB2EEe20B60cdDb64F"
              />
              <ContractItem
                title="Deployer Address"
                icon="deployer"
                url="https://bscscan.com/address/0x5f188439575F7bf21C29E8B7894D9916aBeb306D"
              />
            </div>
          </div>
          <div className="dark:text-white sm:text-3xl mb-4 sm:mb-6 text-2xl font-bold text-center">
            Swap Contract Addresses
          </div>
          <div className="mt-12 mb-4">
            <div className="grid grid-cols-2 sm:grid-cols-5 gap-4">
              <ContractItem
                title="DOP Pool"
                icon="dop-pool"
                url="https://bscscan.com/address/0x5162f992EDF7101637446ecCcD5943A9dcC63A8A"
              />
              <ContractItem
                title="2 Pool"
                icon="two-pool"
                url="https://bscscan.com/address/0x449256e20ac3ed7F9AE81c2583068f7508d15c02"
              />
              <ContractItem
                title="UST Pool"
                icon="ust-pool"
                url="https://bscscan.com/address/0x830e287ac5947B1C0DA865dfB3Afd7CdF7900464"
              />
              <ContractItem
                title="Dolly Pool"
                icon="dolly-pool"
                url="https://bscscan.com/address/0x61f864a7dFE66Cc818a4Fd0baabe845323D70454"
              />
              <ContractItem
                title="BTC Pool"
                icon="btc-pool"
                url="https://bscscan.com/address/0x43AbDc46B14De7c96eA46bf1Fc670ddCE9863f3e"
              />
            </div>
          </div>
        </WhitePanel>
      </div>
    </>
  );

  function numberWithCommas(x) {
    var x = Number.parseFloat(x).toFixed();
    return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  // end functional
}

// ui
const ContractItem = ({ url, title, icon }) => {
  return (
    <>
      <Link href={url}>
        <a target="_blank">
          <div className="border dark:border-dark-500 rounded-lg py-7 cursor-pointer">
            <div className="flex items-center justify-center">
              <div className="text-center">
                <Image
                  src={`/images/icons/${icon}.svg`}
                  width="44"
                  height="44"
                />
                <div className="font-bold mt-3 dark:text-white ">{title}</div>
              </div>
            </div>
          </div>
        </a>
      </Link>
    </>
  );
};

const StatsList = ({ title, value }) => {
  return (
    <>
      <div className="grid grid-cols-2 mb-4">
        <div className="sm:text-base text-sm dark:text-white">{title}</div>
        <div className="sm:text-base text-sm font-bold text-blue-400 text-right">
          {value != null ? numberWithCommas(value) : <Skeleton count="1" />}
        </div>
      </div>
    </>
  );
};

function numberWithCommas(x) {
  return x?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
