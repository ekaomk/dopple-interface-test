import Head from "next/head";
import {
  POOLS_MAP,
  PoolName,
  TOKENS_MAP,
  ACTIVE_POOL_NAME,
  POOL_LIST,
  NETWORK,
  SWAP_TOKEN_CONDITION_LIST,
} from "../../constants";
import React, { ReactElement, useCallback, useState, useEffect } from "react";
import { formatUnits, parseUnits } from "@ethersproject/units";

import { BigNumber } from "@ethersproject/bignumber";
import SwapPage from "../../components/SwapPage";
import { calculateExchangeRate, formatBNToString } from "../../utils";
import { calculatePriceImpact } from "../../utils/priceImpact";
import { debounce } from "lodash";
import { useApproveAndSwap } from "../../hooks/useApproveAndSwap";
import usePoolData from "../../hooks/usePoolData";
import { usePoolTokenBalances } from "../../state/wallet/hook";
import { useSwapContract } from "../../hooks/useContract";
import { useActiveWeb3React } from "../../hooks";
import { useGetApiData } from "../../hooks/useGetApiData";

function Swap({ poolName, poolList }) {
  // Force pool here
  poolName = ACTIVE_POOL_NAME;
  poolList = POOL_LIST;

  const [poolData] = usePoolData(poolName);
  // const approveAndSwap = useApproveAndSwap(poolName);
  const tokenBalances = usePoolTokenBalances("SWAP_POOL_TOKEN_BALANCES");
  const swapContract = useSwapContract(poolName);
  const POOL = POOLS_MAP[poolName];
  const [bestTradingRoute, setBestTradingRoute] = useState(ACTIVE_POOL_NAME);
  const [tokenTo, setTokenTo] = useState([]);
  const approveAndSwapList = {};
  const swapContractList = [];
  poolList.map((pool_name, index) => {
    swapContractList.push({
      useSwapContract: useSwapContract(pool_name),
      poolName: pool_name,
    });
    approveAndSwapList[pool_name] = {
      approveAndSwap: useApproveAndSwap(pool_name),
      poolName: pool_name,
    };
  });

  useGetApiData();

  // console.log(approveAndSwapList);

  const [formState, setFormState] = useState({
    error: null,
    from: {
      symbol: POOL.poolTokens[0].symbol,
      value: "",
    },
    to: {
      symbol: POOL.poolTokens[1].symbol,
      value: BigNumber.from("0"),
    },
    priceImpact: BigNumber.from("0"),
    exchangeRate: BigNumber.from("0"),
    tradingRoute: "",
  });

  // console.log(TOKENS_MAP);
  const swapTokenList = [];
  Object.keys(TOKENS_MAP).map((coinName, index) => {
    swapTokenList.push(TOKENS_MAP[coinName]);
  });

  // build a representation of pool tokens for the UI
  const tokenFrom = swapTokenList.map(
    ({ symbol, name, icon, decimals, addresses }) => ({
      name,
      icon,
      symbol,
      decimals,
      value: tokenBalances ? tokenBalances[symbol] : BigNumber.from("0"),
      addresses,
    })
  );
  // first init is DAI
  function setNewTokenTo(symbol) {
    const newTokenTo = SWAP_TOKEN_CONDITION_LIST[symbol].map(
      ({ symbol, name, icon, decimals, addresses }) => ({
        name,
        icon,
        symbol,
        decimals,
        value: tokenBalances ? tokenBalances[symbol] : BigNumber.from("0"),
        addresses,
      })
    );
    setTokenTo(newTokenTo);
  }

  useEffect(() => {
    setNewTokenTo("DAI");
  }, []);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const calculateSwapAmount = useCallback(
    debounce(async (formStateArg) => {
      if (swapContract == null || tokenBalances === null || poolData == null)
        return;
      const cleanedFormFromValue = formStateArg.from.value.replace(/[$,]/g, ""); // remove common copy/pasted financial characters
      if (cleanedFormFromValue === "" || isNaN(+cleanedFormFromValue)) {
        setFormState((prevState) => ({
          ...prevState,
          to: {
            ...prevState.to,
            value: BigNumber.from("0"),
          },
          priceImpact: BigNumber.from("0"),
        }));
        return;
      }
      // TODO: improve the relationship between token / index
      const amountToGive = parseUnits(
        cleanedFormFromValue,
        TOKENS_MAP[formStateArg.from.symbol].decimals
      );

      let error = null;
      let amountToReceive = BigNumber.from("0");
      let tradingRoute = "Dopple Pool";
      if (amountToGive.gt(tokenBalances[formStateArg.from.symbol])) {
        error = "Insufficient Balance";
      }
      if (amountToGive.isZero()) {
        amountToReceive = BigNumber.from("0");
      } else {
        // CALCULATE SWAP
        let highestTokenAmount = 0;
        let highestTokenName = "";
        for (let index = 0; index < swapContractList.length; index++) {
          try {
            // try check token in pool is exist or not
            // improve the relationship between token / index
            const tokenIndexFrom = POOLS_MAP[
              poolList[index]
            ].poolTokens.findIndex(
              ({ symbol }) => symbol === formStateArg.from.symbol
            );
            const tokenIndexTo = POOLS_MAP[
              poolList[index]
            ].poolTokens.findIndex(
              ({ symbol }) => symbol === formStateArg.to.symbol
            );

            // call swap contract
            amountToReceive = await swapContractList[
              index
            ].useSwapContract.calculateSwap(
              tokenIndexFrom,
              tokenIndexTo,
              amountToGive
            );
          } catch (error) {
            // in case token in pool doesn't exist
            amountToReceive = BigNumber.from(0);
          }

          // console.log(
          //   swapContractList[index].poolName + " \n",
          //   amountToReceive / 1e18
          // );

          if (amountToReceive > highestTokenAmount) {
            highestTokenAmount = amountToReceive;
            highestTokenName = swapContractList[index].poolName;
          }
        }

        // set result of best route
        amountToReceive = highestTokenAmount;
        tradingRoute = highestTokenName;
        setBestTradingRoute(highestTokenName);
        // console.log("highestTokenName \n", highestTokenName);
      }

      const tokenTo = TOKENS_MAP[formStateArg.to.symbol];
      const tokenFrom = TOKENS_MAP[formStateArg.from.symbol];

      console.log("amountToReceive", amountToReceive);

      setFormState((prevState) => ({
        ...prevState,
        error,
        to: {
          ...prevState.to,
          value: amountToReceive,
        },
        priceImpact: calculatePriceImpact(
          amountToGive.mul(BigNumber.from(10).pow(18 - tokenFrom.decimals)),
          amountToReceive.mul(BigNumber.from(10).pow(18 - tokenTo.decimals)),
          poolData?.virtualPrice
        ),
        exchangeRate: calculateExchangeRate(
          amountToGive,
          tokenFrom.decimals,
          amountToReceive,
          tokenTo.decimals
        ),
        tradingRoute: tradingRoute,
      }));
    }, 250),
    [setFormState, swapContract, tokenBalances, poolData]
  );

  function handleUpdateAmountFrom(value, input = false) {
    setFormState((prevState) => {
      const nextState = {
        ...prevState,
        from: {
          ...prevState.from,
          value: !input ? formatDecimalPlace(value).toString() : value,
        },
        priceImpact: BigNumber.from("0"),
        exchangeRate: BigNumber.from("0"),
      };
      calculateSwapAmount(nextState);
      return nextState;
    });
  }
  function handleReverseExchangeDirection() {
    setFormState((prevState) => {
      setNewTokenTo(prevState.to.symbol);
      const nextState = {
        error: null,
        from: {
          symbol: prevState.to.symbol,
          value: prevState.from.value,
        },
        to: {
          symbol: prevState.from.symbol,
          value: BigNumber.from("0"),
        },
        priceImpact: BigNumber.from("0"),
        exchangeRate: BigNumber.from("0"),
      };
      calculateSwapAmount(nextState);
      return nextState;
    });
  }
  function handleUpdateTokenFrom(symbol) {
    setFormState((prevState) => {
      setNewTokenTo(symbol);
      handleUpdateTokenTo(SWAP_TOKEN_CONDITION_LIST[symbol][0].symbol);
      const nextState = {
        ...prevState,
        error: null,
        from: {
          ...prevState.from,
          symbol,
        },
        to: {
          ...prevState.to,
          value: BigNumber.from("0"),
        },
        priceImpact: BigNumber.from("0"),
        exchangeRate: BigNumber.from("0"),
      };
      calculateSwapAmount(nextState);
      return nextState;
    });
  }

  function handleUpdateTokenTo(symbol) {
    setFormState((prevState) => {
      const nextState = {
        ...prevState,
        error: null,
        to: {
          ...prevState.to,
          value: BigNumber.from("0"),
          symbol,
        },
        priceImpact: BigNumber.from("0"),
        exchangeRate: BigNumber.from("0"),
      };
      calculateSwapAmount(nextState);
      return nextState;
    });
  }

  async function handleConfirmTransaction() {
    console.log("bestTradingRoute", bestTradingRoute);
    const fromToken = TOKENS_MAP[formState.from.symbol];
    await approveAndSwapList[bestTradingRoute].approveAndSwap({
      fromAmount: parseUnits(formState.from.value, fromToken.decimals),
      fromTokenSymbol: formState.from.symbol,
      toAmount: formState.to.value,
      toTokenSymbol: formState.to.symbol,
    });
    // Clear input after deposit
    setFormState((prevState) => ({
      error: null,
      from: {
        ...prevState.from,
        value: "0.0",
      },
      to: {
        ...prevState.to,
        value: BigNumber.from("0"),
      },
      priceImpact: BigNumber.from("0"),
      exchangeRate: BigNumber.from("0"),
    }));
  }

  return (
    <>
      <Head>
        <title>Stablecoin DEX on BSC</title>
      </Head>
      <SwapPage
        tokenFrom={tokenFrom}
        tokenTo={tokenTo}
        exchangeRateInfo={{
          pair: `${formState.from.symbol}/${formState.to.symbol}`,
          exchangeRate: formState.exchangeRate,
          priceImpact: formState.priceImpact,
          tradingRoute: formState.tradingRoute,
        }}
        fromState={formState.from}
        toState={{
          ...formState.to,
          value: formatUnits(
            formState.to.value,
            TOKENS_MAP[formState.to.symbol].decimals
          ),
        }}
        onChangeFromAmount={handleUpdateAmountFrom}
        onChangeFromToken={handleUpdateTokenFrom}
        onChangeToToken={handleUpdateTokenTo}
        error={formState.error}
        onConfirmTransaction={handleConfirmTransaction}
        onClickReverseExchangeDirection={handleReverseExchangeDirection}
      />
    </>
  );
}

function formatDecimalPlace(value) {
  try {
    return Math.floor(parseFloat(value) * 10000) / 10000;
  } catch (error) {
    return;
  }
}

export default Swap;
