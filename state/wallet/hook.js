import {
  BLOCK_TIME,
  STABLE_COIN_POOL_NAME,
  TWO_POOL_NAME,
  UST_POOL_NAME,
  DOLLY_MINT_POOL_NAME,
  DOLLY_POOL_NAME,
  DOLLY,
  BTC_POOL_NAME,
  renBTC,
  BTCB,
} from "../../constants";
import { USDC, USDT, DAI, BUSD, UST } from "../../constants";
import { Zero } from "@ethersproject/constants";
import { useActiveWeb3React } from "../../hooks";
import { useMemo } from "react";
import usePoller from "../../hooks/usePoller";
import { useState } from "react";
import { useTokenContract } from "../../hooks/useContract";
export function useTokenBalance(t) {
  const { account, chainId } = useActiveWeb3React();
  const [balance, setBalance] = useState(Zero);
  const tokenContract = useTokenContract(t);
  usePoller(() => {
    async function pollBalance() {
      const newBalance = account
        ? await (tokenContract === null || tokenContract === 0
            ? 0
            : tokenContract.balanceOf(account))
        : Zero;
      if (newBalance !== balance) {
        newBalance && setBalance(newBalance);
      }
    }
    if (account && chainId) {
      pollBalance();
    }
  }, BLOCK_TIME);
  return balance;
}
export function usePoolTokenBalances(poolName) {
  const daiTokenBalance = useTokenBalance(DAI);
  const usdcTokenBalance = useTokenBalance(USDC);
  const usdtTokenBalance = useTokenBalance(USDT);
  const busdTokenBalance = useTokenBalance(BUSD);
  const ustTokenBalance = useTokenBalance(UST);
  const dollyTokenBalance = useTokenBalance(DOLLY);
  const btcbTokenBalance = useTokenBalance(BTCB);
  const renbtcTokenBalance = useTokenBalance(renBTC);

  const stableCoinPoolTokenBalances = useMemo(
    () => ({
      [DAI.symbol]: daiTokenBalance,
      [USDC.symbol]: usdcTokenBalance,
      [USDT.symbol]: usdtTokenBalance,
      [BUSD.symbol]: busdTokenBalance,
    }),
    [daiTokenBalance, usdcTokenBalance, usdtTokenBalance, busdTokenBalance]
  );

  const btcPoolTokenBalances = useMemo(
    () => ({
      [BTCB.symbol]: btcbTokenBalance,
      [renBTC.symbol]: renbtcTokenBalance,
    }),
    [btcbTokenBalance, renbtcTokenBalance]
  );

  const ustPoolTokenBalances = useMemo(
    () => ({
      [UST.symbol]: ustTokenBalance,
      [USDT.symbol]: usdtTokenBalance,
      [BUSD.symbol]: busdTokenBalance,
    }),
    [ustTokenBalance, usdtTokenBalance, busdTokenBalance]
  );

  const twoPoolTokenBalances = useMemo(
    () => ({
      [USDT.symbol]: usdtTokenBalance,
      [BUSD.symbol]: busdTokenBalance,
    }),
    [usdtTokenBalance, busdTokenBalance]
  );

  const dollyPoolTokenBalances = useMemo(
    () => ({
      [BUSD.symbol]: busdTokenBalance,
      [USDT.symbol]: usdtTokenBalance,
      [DOLLY.symbol]: dollyTokenBalance,
    }),
    [usdtTokenBalance, busdTokenBalance, dollyTokenBalance]
  );

  const dollyMintPoolTokenBalances = useMemo(
    () => ({
      [BUSD.symbol]: busdTokenBalance,
      [USDT.symbol]: usdtTokenBalance,
    }),
    [usdtTokenBalance, busdTokenBalance]
  );

  const swapPoolTokenBalances = useMemo(
    () => ({
      [DAI.symbol]: daiTokenBalance,
      [USDC.symbol]: usdcTokenBalance,
      [USDT.symbol]: usdtTokenBalance,
      [BUSD.symbol]: busdTokenBalance,
      [UST.symbol]: ustTokenBalance,
      [DOLLY.symbol]: dollyTokenBalance,
      [BTCB.symbol]: btcbTokenBalance,
      [renBTC.symbol]: renbtcTokenBalance,
    }),
    [
      daiTokenBalance,
      usdcTokenBalance,
      usdtTokenBalance,
      busdTokenBalance,
      ustTokenBalance,
      dollyTokenBalance,
      btcbTokenBalance,
      renbtcTokenBalance,
    ]
  );

  switch (poolName) {
    case STABLE_COIN_POOL_NAME: {
      return stableCoinPoolTokenBalances;
    }
    case UST_POOL_NAME: {
      return ustPoolTokenBalances;
    }
    case TWO_POOL_NAME: {
      return twoPoolTokenBalances;
    }
    case DOLLY_MINT_POOL_NAME: {
      return dollyMintPoolTokenBalances;
    }
    case DOLLY_POOL_NAME: {
      return dollyPoolTokenBalances;
    }
    case BTC_POOL_NAME: {
      return btcPoolTokenBalances;
    }

    // add for swap page
    case "SWAP_POOL_TOKEN_BALANCES": {
      return swapPoolTokenBalances;
    }
    default:
      return null;
  }
}
