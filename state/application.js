import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  lastTransactionTimes: {},
};
const applicationSlice = createSlice({
  name: "application",
  initialState,
  reducers: {
    updateGasPrices(state, action) {
      const { gasStandard, gasFast, gasInstant } = action.payload;
      state.gasStandard = gasStandard ? gasStandard : 5;
      state.gasFast = gasFast ? gasFast : 5;
      state.gasInstant = gasInstant ? gasInstant : 5;
    },
    updateTokensPricesUSD(state, action) {
      state.tokenPricesUSD = action.payload;
    },
    updateLastTransactionTimes(state, action) {
      state.lastTransactionTimes = Object.assign(
        Object.assign({}, state.lastTransactionTimes),
        action.payload
      );
    },
  },
});
export const {
  updateGasPrices,
  updateTokensPricesUSD,
  updateLastTransactionTimes,
} = applicationSlice.actions;
export default applicationSlice.reducer;
