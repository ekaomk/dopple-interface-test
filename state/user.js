import { numberInputStateCreator } from "../utils/numberInputState";
import { createSlice } from "@reduxjs/toolkit";
import { BigNumber } from "@ethersproject/bignumber";
import { Zero } from "@ethersproject/constants";
export var GasPrices;
(function (GasPrices) {
  GasPrices["Standard"] = "STANDARD";
  GasPrices["Fast"] = "FAST";
  GasPrices["Instant"] = "INSTANT";
  GasPrices["Custom"] = "CUSTOM";
})(GasPrices || (GasPrices = {}));
export var Slippages;
(function (Slippages) {
  Slippages["One"] = "ONE";
  Slippages["OneTenth"] = "ONE_TENTH";
  Slippages["Custom"] = "CUSTOM";
})(Slippages || (Slippages = {}));
export var Deadlines;
(function (Deadlines) {
  Deadlines["Ten"] = "TEN";
  Deadlines["Thirty"] = "THIRTY";
  Deadlines["Custom"] = "CUSTOM";
})(Deadlines || (Deadlines = {}));
export const initialState = {
  userSwapAdvancedMode: false,
  userPoolAdvancedMode: false,
  userDarkMode: false,
  gasPriceSelected: GasPrices.Standard,
  slippageSelected: Slippages.OneTenth,
  infiniteApproval: true,
  transactionDeadlineSelected: Deadlines.Ten,
  apiData: null,
};
const gasCustomStateCreator = numberInputStateCreator(
  0, // gas is in wei
  Zero
);
const slippageCustomStateCreator = numberInputStateCreator(
  4,
  BigNumber.from(10).pow(4).mul(1)
);
const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    updateSwapAdvancedMode(state, action) {
      state.userSwapAdvancedMode = action.payload;
    },
    updatePoolAdvancedMode(state, action) {
      state.userPoolAdvancedMode = action.payload;
    },
    updateDarkMode(state, action) {
      // this will be phased out in favor of chakra's colorMode
      state.userDarkMode = action.payload;
    },
    updateGasPriceCustom(state, action) {
      state.gasCustom = gasCustomStateCreator(action.payload);
    },
    updateGasPriceSelected(state, action) {
      state.gasPriceSelected = action.payload;
      if (action.payload !== GasPrices.Custom) {
        // clear custom value when standard option selected
        state.gasCustom = gasCustomStateCreator("");
      }
    },
    updateSlippageSelected(state, action) {
      state.slippageSelected = action.payload;
      if (action.payload !== Slippages.Custom) {
        // clear custom value when standard option selected
        state.slippageCustom = slippageCustomStateCreator("");
      }
    },
    updateSlippageCustom(state, action) {
      state.slippageCustom = slippageCustomStateCreator(action.payload);
    },
    updateInfiniteApproval(state, action) {
      state.infiniteApproval = action.payload;
    },
    updateTransactionDeadlineSelected(state, action) {
      state.transactionDeadlineSelected = action.payload;
      // clear custom value when standard option selected
      if (action.payload !== Deadlines.Custom) {
        state.transactionDeadlineCustom = "";
      }
    },
    updateTransactionDeadlineCustom(state, action) {
      state.transactionDeadlineCustom = action.payload;
    },
    updateApiData(state, action) {
      state.apiData = action.payload;
    },
  },
});
export const {
  updateSwapAdvancedMode,
  updatePoolAdvancedMode,
  updateDarkMode,
  updateGasPriceCustom,
  updateGasPriceSelected,
  updateSlippageCustom,
  updateSlippageSelected,
  updateInfiniteApproval,
  updateTransactionDeadlineSelected,
  updateTransactionDeadlineCustom,
  updateApiData,
} = userSlice.actions;
export default userSlice.reducer;
