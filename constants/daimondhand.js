const DIAMONDHAND_LIST = [
  {
    name: "DOP",
    add_lp_url:
      "https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28",
    address: "0xa0F22ef29Bd51013E8bd0aE438fe74898ba5F070",
    deposit_token_address: "0x844FA82f1E54824655470970F7004Dd90546bB28",
    reward_token_address: "0x844FA82f1E54824655470970F7004Dd90546bB28",
    icon: "/images/diamond-hand/dop-token.svg",
    percentage: "25%",
  },
  {
    name: "TWIN",
    add_lp_url:
      "https://twindex.com/#/swap?outputCurrency=0x3806aae953a3a873D02595f76C7698a57d4C7A57",
    address: "0x182a11f9CC9Cd3Cd7C5aD38131cF1902CCfD3615",
    deposit_token_address: "0x3806aae953a3a873D02595f76C7698a57d4C7A57",
    reward_token_address: "0x844FA82f1E54824655470970F7004Dd90546bB28",
    icon: "/images/diamond-hand/twin-token.svg",
    percentage: "75%",
  },
];

export default DIAMONDHAND_LIST;
