const doppleLogo = "/images/logo.svg";
const daiLogo = "/images/icons/dai.svg";
const usdcLogo = "/images/icons/usdc.svg";
const usdtLogo = "/images/icons/usdt.svg";
const busdLogo = "/images/icons/busd.svg";
const pancakeLogo = "/images/icons/swap/logo-pancake.png";
const stableCoinLogo = "/images/icons/stable-coin.svg";
const twoPoolLogo = "/images/icons/busd-usdt.svg";
const ustPoolLogo = "/images/icons/ust-pool.png";
const ustLogo = "/images/icons/ust.png";
const dopBUSDLogo = "/images/icons/dop-busd.svg";
const dollyLogo = "/images/icons/dolly.svg";
const dopBNBLogo = "/images/icons/dop-bnb.svg";
const dopDAILogo = "/images/icons/dop-dai.svg";
const dopUSDTLogo = "/images/icons/dop-usdt.svg";
const dollyPoolLogo = "/images/icons/dolly-pool.svg";
const dopDollyLogo = "/images/icons/dop-dolly.svg";
const btcPoolLogo = "/images/icons/btcb-renbtc.svg";
const btcbLogo = "/images/icons/btcb.svg";
const renbtcLogo = "/images/icons/renbtc-icon.svg";

export const NetworkContextName = "NETWORK";
export const STABLE_COIN_POOL_NAME = "Stable Coin Pool";
export const TWO_POOL_NAME = "Two Pool";
export const UST_POOL_NAME = "UST Pool";
export const DOLLY_POOL_NAME = "DOLLY Pool";
export const DOLLY_MINT_POOL_NAME = "Mint Dolly Pool";
export const BTC_POOL_NAME = "BTC Pool";

// ABIS
import FAIR_LAUNCH_ABIS_BSC from "../constants/abis/fairLaunch.abi.json";
import FAIR_LAUNCH_ABIS_BSC_TESTNET from "../constants/abis/fairLaunchTestNet.json";

export const POOL_LIST = [
  STABLE_COIN_POOL_NAME,
  TWO_POOL_NAME,
  UST_POOL_NAME,
  DOLLY_POOL_NAME,
  BTC_POOL_NAME,
  // DOLLY_MINT_POOL_NAME,
];

export const ACTIVE_POOL_NAME = STABLE_COIN_POOL_NAME;

export const CONNECTOR_STORAGE_KEY = "connector";

export var ChainId;
(function (ChainId) {
  ChainId[(ChainId["BSC_MAINNET"] = 56)] = "BSC_MAINNET";
  ChainId[(ChainId["BSC_TESTNET"] = 97)] = "BSC_TESTNET";
  ChainId[(ChainId["HARDHAT"] = 31337)] = "HARDHAT";
})(ChainId || (ChainId = {}));

export class Token {
  constructor(addresses, decimals, symbol, geckoId, name, icon) {
    this.addresses = addresses;
    this.decimals = decimals;
    this.symbol = symbol;
    this.geckoId = geckoId;
    this.name = name;
    this.icon = icon;
  }
}

export class StakingToken {
  constructor(
    addresses,
    decimals,
    symbol,
    geckoId,
    name,
    icon,
    pool,
    isActive = false,
    description,
    order
  ) {
    this.addresses = addresses;
    this.decimals = decimals;
    this.symbol = symbol;
    this.geckoId = geckoId;
    this.name = name;
    this.icon = icon;
    this.pool = pool;
    this.isActive = isActive;
    this.description = description;
    this.order = order;
  }
}

export const BLOCK_TIME = 3000;
export const NETWORK = ChainId.BSC_MAINNET;
export const STABLECOIN_SWAP_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

export const MERKLETREE_DATA = {
  [ChainId.BSC_MAINNET]: "mainnetTestAccounts.json",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "hardhat.json",
};

export const STABLECOIN_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

export const SWAP_CONTRACT_ADDRESS = {
  [ChainId.BSC_MAINNET]: "0x5162f992EDF7101637446ecCcD5943A9dcC63A8A",
  [ChainId.BSC_TESTNET]: "0x0cEbb8651adb8490b5e190D4a5ED495e7c6CbF4f",
  [ChainId.HARDHAT]: "",
};

export const SWAP_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x9116F04092828390799514Bac9986529d70c3791",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

// SWAP CONTRACT
export const STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x5162f992EDF7101637446ecCcD5943A9dcC63A8A",
  [ChainId.BSC_TESTNET]: "0x0cEbb8651adb8490b5e190D4a5ED495e7c6CbF4f",
  [ChainId.HARDHAT]: "",
};

export const UST_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  // POOL ADDRESS
  [ChainId.BSC_MAINNET]: "0x830e287ac5947B1C0DA865dfB3Afd7CdF7900464",
  [ChainId.BSC_TESTNET]: "0x276F1805F642a4736baac08e6F15A938c3868f81",
  [ChainId.HARDHAT]: "",
};

export const TWO_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  // POOL ADDRESS
  [ChainId.BSC_MAINNET]: "0x449256e20ac3ed7F9AE81c2583068f7508d15c02",
  [ChainId.BSC_TESTNET]: "0xde3d63936b754846D9339895c54014a8CDBd0828",
  [ChainId.HARDHAT]: "",
};

export const BTC_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  // POOL ADDRESS
  [ChainId.BSC_MAINNET]: "0x43AbDc46B14De7c96eA46bf1Fc670ddCE9863f3e",
  [ChainId.BSC_TESTNET]: "0xf182797D138F5421ABe29e8012932Eb33126cD7A",
  [ChainId.HARDHAT]: "",
};

// Dopple Govern Token
export const DOPPLE_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x844FA82f1E54824655470970F7004Dd90546bB28",
  [ChainId.BSC_TESTNET]: "0x2fc9ec5870e4bE90037c39f62a2A5962dB08327C",
  [ChainId.HARDHAT]: "",
};

// Diamondhand Token
// DOP
export const DOP_DIAMONDHAND_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xa0F22ef29Bd51013E8bd0aE438fe74898ba5F070",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

// TWIN
export const TWIN_DIAMONDHAND_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x182a11f9CC9Cd3Cd7C5aD38131cF1902CCfD3615",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

// ***MINT DOLLY***  SWAP CONTRACT
export const DOLLY_MINT_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  // POOL ADDRESS
  [ChainId.BSC_MAINNET]: "0x7A9634F6917b9736832f50f4C19b3A231B36F79b",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};

export const DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES = {
  // POOL ADDRESS
  [ChainId.BSC_MAINNET]: "0x61f864a7dFE66Cc818a4Fd0baabe845323D70454",
  [ChainId.BSC_TESTNET]: "0x6dbE866654ee6e94f73FC70518f7EA00B2B3dCd7",
  [ChainId.HARDHAT]: "",
};

// DOLLY Token
export const DOLLY_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xff54da7caf3bc3d34664891fc8f3c9b6dea6c7a5",
  [ChainId.BSC_TESTNET]: "0xCfcbd372097aca93C810C67fF769caEF250aAF9B",
  [ChainId.HARDHAT]: "",
};

export const DOPPLE = new Token(
  DOPPLE_TOKEN_CONTRACT_ADDRESSES,
  18,
  "Dopple",
  "dopple",
  "DOPPLE",
  doppleLogo
);

export const DOP = new Token(
  DOPPLE_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP",
  "dopple",
  "DOP",
  doppleLogo
);

// Supper dopple
export const SDOP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "",
  [ChainId.BSC_TESTNET]: "0x33b1B665B9a3a9D9c3562Ad61F885F810172444F",
  [ChainId.HARDHAT]: "",
};

export const SDOP = new Token(
  SDOP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "Super Dopple",
  "sdop",
  "sDOP",
  doppleLogo
);

// DOLLY Token
export const DOLLY = new Token(
  DOLLY_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOLLY",
  "dolly",
  "DOLLY",
  dollyLogo
);

export const STABLECOIN_SWAP_TOKEN = new Token(
  STABLECOIN_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "doppleUSD Swap",
  "doppleusd",
  "Dopple DAI/USDC/USDT/BUSD",
  doppleLogo
);
export const STABLE_SWAP_TOKEN = new Token(
  STABLE_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOPPLE Swap",
  "dopple",
  "Dopple DAI/USDC/USDT/BUSD",
  doppleLogo
);

export const UST_POOL_SWAP_TOKEN = new Token(
  // address of pool
  UST_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "UST Swap",
  "ust",
  "UST UST/USDT/BUSD",
  ustPoolLogo
);

export const TWO_POOL_SWAP_TOKEN = new Token(
  // address of pool
  TWO_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "2",
  "2",
  "2 USDT/BUSD",
  twoPoolLogo
);

export const DOLLY_POOL_SWAP_TOKEN = new Token(
  DOLLY_POOL_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOLLY Swap",
  "dolly",
  "DOLLY DOLLY/USDT/BUSD",
  dollyLogo
);

export const DOLLY_MINT_POOL_SWAP_TOKEN = new Token(
  DOLLY_MINT_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOLLY Mint",
  "dolly",
  "DOLLY DOLLY/USDT/BUSD",
  dollyLogo
);

export const BTC_POOL_SWAP_TOKEN = new Token(
  // address of pool
  BTC_SWAP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "BTC Swap",
  "btc",
  "BTC BTCB/renBTC",
  btcPoolLogo
);

// coin Token
const DAI_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x1af3f329e8be154074d8769d1ffa4ee058b1dbc3",
  [ChainId.BSC_TESTNET]: "0xf73417dEE348D4B274b169e7091d10aC8be1b60a",
  [ChainId.HARDHAT]: "",
};
export const DAI = new Token(
  DAI_CONTRACT_ADDRESSES,
  18,
  "DAI",
  "dai",
  "DAI",
  daiLogo
);
const USDC_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d",
  [ChainId.BSC_TESTNET]: "0x3259de0df54349a8d6efe37aa049ec548c7dfa29",
  [ChainId.HARDHAT]: "",
};
export const USDC = new Token(
  USDC_CONTRACT_ADDRESSES,
  18,
  "USDC",
  "usd-coin",
  "USDC",
  usdcLogo
);
const USDT_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x55d398326f99059ff775485246999027b3197955",
  [ChainId.BSC_TESTNET]: "0xA0E0B3459d79ec9CD9f9f1AD068bc0b746c5648C",
  [ChainId.HARDHAT]: "",
};

export const USDT = new Token(
  USDT_CONTRACT_ADDRESSES,
  18,
  "USDT",
  "tether",
  "USDT",
  usdtLogo
);
const BUSD_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xe9e7cea3dedca5984780bafc599bd69add087d56",
  [ChainId.BSC_TESTNET]: "0x18b1d45BD7f6f1F6e2D975B3621Ca9a478baCF4E",
  [ChainId.HARDHAT]: "",
};
export const BUSD = new Token(
  BUSD_CONTRACT_ADDRESSES,
  18,
  "BUSD",
  "busd",
  "BUSD",
  busdLogo
);

const BNB_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xbb4cdb9cbd36b01bd1cbaebf2de08d9173bc095c",
  [ChainId.BSC_TESTNET]: "",
  [ChainId.HARDHAT]: "",
};
export const BNB = new Token(
  BNB_CONTRACT_ADDRESSES,
  18,
  "BNB",
  "Wrapped BNB",
  "BNB",
  //TODO
  busdLogo
);

const UST_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x23396cf899ca06c4472205fc903bdb4de249d6fc",
  [ChainId.BSC_TESTNET]: "0x66896b043f7Bd1766605CcB5360a3918DB2EDb4E",
  [ChainId.HARDHAT]: "",
};
export const UST = new Token(
  UST_CONTRACT_ADDRESSES,
  18,
  "UST",
  "Wrapped UST",
  "UST",
  //TODO
  ustLogo
);

const BTCB_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c",
  [ChainId.BSC_TESTNET]: "0x1f4e06De7712B78c492116D5FDc07a931a146827",
  [ChainId.HARDHAT]: "",
};
export const BTCB = new Token(
  BTCB_CONTRACT_ADDRESSES,
  18,
  "BTCB",
  "btcb",
  "BTCB",
  //TODO
  btcbLogo
);

const renBTC_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xfCe146bF3146100cfe5dB4129cf6C82b0eF4Ad8c",
  [ChainId.BSC_TESTNET]: "0xB8e3CFcBD50D70b65d84E14636AbdD4Cd2CbFB85",
  [ChainId.HARDHAT]: "",
};
export const renBTC = new Token(
  renBTC_CONTRACT_ADDRESSES,
  8,
  "renBTC",
  "renbtc",
  "renBTC",
  //TODO
  renbtcLogo
);

export const DOP_DIAMONDHAND = new Token(
  DOP_DIAMONDHAND_TOKEN_CONTRACT_ADDRESSES,
  18,
  "Dop_Diamondhand",
  "Dop_Diamondhand",
  "Dop_Diamondhand",
  doppleLogo
);

export const TWIN_DIAMONDHAND = new Token(
  TWIN_DIAMONDHAND_TOKEN_CONTRACT_ADDRESSES,
  18,
  "Twin_Diamondhand",
  "Twin_Diamondhand",
  "Twin_Diamondhand",
  doppleLogo
);

// should set index of token same as BSC SCAN
export const STABLECOIN_POOL_TOKENS = [USDC, USDT, BUSD];
export const STABLE_COIN_POOL_TOKENS = [DAI, BUSD, USDT, USDC];
export const TWO_POOL_TOKENS = [BUSD, USDT];
export const UST_POOL_TOKENS = [UST, BUSD, USDT];
export const DOLLY_POOL_TOKEN = [BUSD, USDT, DOLLY];
export const BTC_POOL_TOKEN = [BTCB, renBTC];

// for mint dolly
export const DOLLY_MINT_POOL_TOKEN = [BUSD, USDT];

// Staking
export const FAIRLAUNCH_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xDa0a175960007b0919DBF11a38e6EC52896bddbE",
  [ChainId.BSC_TESTNET]: "0x467f2eA47EE8A074673A93eb3a526cFF377a34Ce",
};

export const DOPPLE_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x9116F04092828390799514Bac9986529d70c3791",
  [ChainId.BSC_TESTNET]: "0xbbd7ca69490ffd8fa33c314cc3de677b45d2f95e",
};

export const TWO_POOLS_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x124166103814E5a033869c88e0F40c61700Fca17",
  [ChainId.BSC_TESTNET]: "0xa853424590db5bba4c120247e55667ab0b3ac154",
};

export const UST_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x7Edcdc8cD062948CE9A9bc38c477E6aa244dD545",
  [ChainId.BSC_TESTNET]: "0x5d5fb998c25cb0d39a911f55923c7b7d98978222",
};

export const DOLLY_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xAA5509Ce0ecEA324bff504A46Fc61EB75Cb68B0c",
  [ChainId.BSC_TESTNET]: "0x9b172c356871839cb7a3b10f81f6927f927d5e0d",
};

export const BTC_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x9378F642A14936733d3635BD86897690329C6fF1",
  [ChainId.BSC_TESTNET]: "0x02b39f69b78a4c2ec67a9349e12be939d71ca1c8",
};

// Twindex LP
export const TWINDEX_DOP_DOLLY_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xaF8f9922Ea7b00A8F7F9Cb50729B65f0D99446D6",
  [ChainId.BSC_TESTNET]: "",
};
export const TWINDEX_DOP_BNB_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x64de2eb6Af1C101f4053BE1DEa89Cb8455A64f0D",
  [ChainId.BSC_TESTNET]: "",
};
export const TWINDEX_DOP_BUSD_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xC789F6C658809eED4d1769a46fc7BCe5dbB8316E",
  [ChainId.BSC_TESTNET]: "",
};
export const TWINDEX_DOP_DAI_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xA7E936917446f9e493A453cd360ac5d2bB785f3A",
  [ChainId.BSC_TESTNET]: "",
};
export const TWINDEX_DOP_USDT_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xA4F808994B90f63006a6421A76f24783d931395f",
  [ChainId.BSC_TESTNET]: "",
};

// Legacy Cake LP
export const CAKE_LP_TOKEN_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x070659c37D40029280DfEc931b20E315f2cdFfbC",
  [ChainId.BSC_TESTNET]: "",
};
export const DOP_BUSD_LP_TOKEN_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xb694ec7C2a7C433E69200b1dA3EBc86907B4578B",
  [ChainId.BSC_TESTNET]: "",
};
export const DOP_DOLLY_LP_TOKEN_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xDC62BE962c1754DEdA214848614A87B88e7fF646",
  [ChainId.BSC_TESTNET]: "",
};
export const LEGACY_CAKE_LP_TOKEN_ADDRESS = {
  [ChainId.BSC_MAINNET]: "0xa86365c3e6bf2e3c90d5fdf20d95c8fe7732a4fe",
  [ChainId.BSC_TESTNET]: "",
};
export const LEGACY_DOP_BUSD_LP_TOKEN_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x9610Ed6E29BF112e8944492aA9bEC4dcff216Be6",
  [ChainId.BSC_TESTNET]: "",
};

export const DOPPLE_STAKING_TOKEN = new StakingToken(
  DOPPLE_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP",
  "dop",
  "DOP",
  doppleLogo,
  2, // Pool id here
  true,
  {
    text: "BUY DOP",
    url: "https://twindex.com/#/swap?outputCurrency=0x844fa82f1e54824655470970f7004dd90546bb28",
  },
  1 //order
);

export const CAKE_LP_TOKEN = new StakingToken(
  CAKE_LP_TOKEN_ADDRESSES,
  18,
  "CAKE-LP",
  "Cake-lp",
  "Legacy DOP-BNB LPs",
  dopBNBLogo,
  8, // Pool id here
  true,
  {
    text: "Add DOP-BNB LP",
    url: "https://exchange.pancakeswap.finance/#/add/BNB/0x844FA82f1E54824655470970F7004Dd90546bB28",
  },
  2 //order
);

export const DOP_BUSD_LP_TOKEN = new StakingToken(
  DOP_BUSD_LP_TOKEN_ADDRESSES,
  18,
  "DOP-BUSD-LP",
  "dop-busd-lp",
  "Legacy DOP-BUSD LPs",
  dopBUSDLogo,
  9, // Pool id here
  true,
  {
    text: "Add DOP-BUSD LP",
    url: "https://exchange.pancakeswap.finance/#/add/0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56/0x844FA82f1E54824655470970F7004Dd90546bB28",
  },
  3 //order
);

export const DOPPLE_LP_TOKEN = new StakingToken(
  DOPPLE_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-LP",
  "dop-lp",
  "DOP LPs",
  stableCoinLogo,
  1, // Pool id here
  true,
  {
    text: "Add DOP LP",
    url: "/Deposit/dop-lps",
  },
  4 //order
);

export const TWO_POOLS_LP_TOKEN = new StakingToken(
  TWO_POOLS_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "2-POOLS-LP",
  "2-pools-lp",
  "2 Pools LPs",
  twoPoolLogo,
  5, // Pool id here
  true,
  {
    text: "Add 2 Pools LP",
    url: "/Deposit/2-pools-lps",
  },
  5 //order
);

export const UST_LP_TOKEN = new StakingToken(
  UST_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "UST-LP",
  "ust-lp",
  "UST LPs",
  ustPoolLogo,
  4, // Pool id here
  true,
  {
    text: "Add UST LP",
    url: "/Deposit/ust-pools-lps",
  },
  6 //order
);

export const DOLLY_LP_TOKEN = new StakingToken(
  DOLLY_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOLLY-LP",
  "dolly-lp",
  "DOLLY LPs",
  dollyPoolLogo,
  12, // Pool id here
  true,
  {
    text: "Add DOLLY LP",
    url: "/Deposit/dolly-lps",
  },
  7 //order
);

export const DOP_DOLLY_LP_TOKEN = new StakingToken(
  DOP_DOLLY_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-DOLLY-LP",
  "dop-dolly-lp",
  "Legacy DOP-DOLLY LPs",
  dopDollyLogo,
  13, // Pool id here
  true,
  {
    text: "Add DOP-DOLLY LP",
    url: "https://exchange.pancakeswap.finance/#/add/0xff54da7caf3bc3d34664891fc8f3c9b6dea6c7a5/0x844FA82f1E54824655470970F7004Dd90546bB28",
  },
  7 //order
);

export const BTC_LP_TOKEN = new StakingToken(
  BTC_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "BTC-LP",
  "btc-lp",
  "BTC LPs",
  btcPoolLogo,
  14, // Pool id here
  true, // active
  {
    text: "Add BTC LP",
    url: "/Deposit/btc-lps",
  },
  15 //order
);

export const LEGACY_CAKE_LP_TOKEN = new StakingToken(
  LEGACY_CAKE_LP_TOKEN_ADDRESS,
  18,
  "Legacy CAKE-LP",
  "Legacy Cake-lp",
  "Legacy DOP-BNB LPs",
  dopBNBLogo,
  0, // Pool id here
  true,
  {
    text: "Remove DOP-BNB LP",
    url: "https://v1exchange.pancakeswap.finance/#/pool",
  },
  2 //order
);

export const LEGACY_DOP_BUSD_LP_TOKEN = new StakingToken(
  LEGACY_DOP_BUSD_LP_TOKEN_ADDRESSES,
  18,
  "Legacy DOP-BUSD-LP",
  "legacy dop-busd-lp",
  "Legacy DOP-BUSD LPs",
  dopBUSDLogo,
  3, // Pool id here
  true,
  {
    text: "Remove DOP-BUSD LP",
    url: "https://v1exchange.pancakeswap.finance/#/pool",
  },
  3 //order
);

// Twindex staking pool
export const TWINDEX_DOP_DOLLY_LP_TOKEN = new StakingToken(
  TWINDEX_DOP_DOLLY_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-DOLLY-LP",
  "dop-dolly-lp",
  "DOP-DOLLY LPs",
  dopDollyLogo,
  15, // Pool id here
  true,
  {
    text: "Add DOP-DOLLY LP",
    url: "https://twindex.com/#/add/0x844FA82f1E54824655470970F7004Dd90546bB28/0xfF54da7CAF3BC3D34664891fC8f3c9B6DeA6c7A5",
  },
  3 //order
);

export const TWINDEX_DOP_BNB_LP_TOKEN = new StakingToken(
  TWINDEX_DOP_BNB_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-BNB-LP",
  "dop-bnb-lp",
  "DOP-BNB LPs",
  dopBNBLogo,
  16, // Pool id here
  true,
  {
    text: "Add DOP-BNB LP",
    url: "https://twindex.com/#/add/0x844FA82f1E54824655470970F7004Dd90546bB28/BNB",
  },
  3 //order
);

export const TWINDEX_DOP_BUSD_LP_TOKEN = new StakingToken(
  TWINDEX_DOP_BUSD_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-BUSD-LP",
  "dop-busd-lp",
  "Legacy DOP-BUSD LPs",
  dopBUSDLogo,
  17, // Pool id here
  true,
  {
    text: "Add DOP-BUSD LP",
    url: "https://twindex.com/#/add/0x844FA82f1E54824655470970F7004Dd90546bB28/0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56",
  },
  3 //order
);

export const TWINDEX_DOP_DAI_LP_TOKEN = new StakingToken(
  TWINDEX_DOP_DAI_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-DAI-LP",
  "dop-dai-lp",
  "Legacy DOP-DAI LPs",
  dopDAILogo,
  18, // Pool id here
  true,
  {
    text: "Add DOP-DAI LP",
    url: "https://twindex.com/#/add/0x844FA82f1E54824655470970F7004Dd90546bB28/0x1AF3F329e8BE154074D8769D1FFa4eE058B1DBc3",
  },
  3 //order
);

export const TWINDEX_DOP_USDT_LP_TOKEN = new StakingToken(
  TWINDEX_DOP_USDT_LP_TOKEN_CONTRACT_ADDRESSES,
  18,
  "DOP-USDT-LP",
  "dop-usdt-lp",
  "Legacy DOP-USDT LPs",
  dopUSDTLogo,
  19, // Pool id here
  true,
  {
    text: "Add DOP-USDT LP",
    url: "https://twindex.com/#/add/0x844FA82f1E54824655470970F7004Dd90546bB28/0x55d398326f99059fF775485246999027B3197955",
  },
  3 //order
);

//start ordering staking page here
export const DOP_STAKING_TOKENS = [
  TWINDEX_DOP_DOLLY_LP_TOKEN,
  TWINDEX_DOP_BNB_LP_TOKEN,

  DOPPLE_STAKING_TOKEN,
];

export const LEGACY_PANCAKE_DOP_STAKING_TOKENS = [
  TWINDEX_DOP_BUSD_LP_TOKEN,
  TWINDEX_DOP_USDT_LP_TOKEN,
  TWINDEX_DOP_DAI_LP_TOKEN,
  DOP_DOLLY_LP_TOKEN,
  CAKE_LP_TOKEN,
  DOP_BUSD_LP_TOKEN,
];

export const STABLE_COIN_STAKING_TOKENS = [
  DOLLY_LP_TOKEN,
  DOPPLE_LP_TOKEN,
  UST_LP_TOKEN,
  TWO_POOLS_LP_TOKEN,
];

// end ordering staking page here

export const STAKING_DOPPLE_LP_TOKENS_LIST = {
  [STABLE_COIN_POOL_NAME]: DOPPLE_LP_TOKEN,
  [TWO_POOL_NAME]: TWO_POOLS_LP_TOKEN,
  [UST_POOL_NAME]: UST_LP_TOKEN,
  [DOLLY_POOL_NAME]: DOLLY_LP_TOKEN,
  [DOLLY_MINT_POOL_NAME]: DOLLY,
  [BTC_POOL_NAME]: BTC_LP_TOKEN,
};

export const POOLS_LIST = [
  {
    name: "Stable Coin Pool",
    title: "DOP LPs",
    slug: "dop-lps",
    icon: stableCoinLogo,
  },
  {
    name: "Two Pool",
    title: "2 Pools LPs",
    slug: "2-pools-lps",
    icon: twoPoolLogo,
  },
  {
    name: "UST Pool",
    title: "UST LPs",
    slug: "ust-pools-lps",
    icon: ustPoolLogo,
  },
  {
    name: "DOLLY Pool",
    title: "DOLLY LPs",
    slug: "dolly-lps",
    icon: dollyPoolLogo,
  },
  {
    name: "BTC Pool",
    title: "BTC LPs",
    slug: "btc-lps",
    icon: btcPoolLogo,
  },
];

// maps a symbol string to a token object
export const TOKENS_MAP = STABLE_COIN_POOL_TOKENS.concat(TWO_POOL_TOKENS)
  .concat(UST_POOL_TOKENS)
  .concat(DOLLY_POOL_TOKEN)
  .concat(BTC_POOL_TOKEN)
  .reduce(
    (acc, token) =>
      Object.assign(Object.assign({}, acc), { [token.symbol]: token }),
    {}
  );

export const POOLS_MAP = {
  [STABLE_COIN_POOL_NAME]: {
    lpToken: STABLE_SWAP_TOKEN,
    poolTokens: STABLE_COIN_POOL_TOKENS,
  },
  [TWO_POOL_NAME]: {
    lpToken: TWO_POOL_SWAP_TOKEN,
    poolTokens: TWO_POOL_TOKENS,
  },
  [UST_POOL_NAME]: {
    lpToken: UST_POOL_SWAP_TOKEN,
    poolTokens: UST_POOL_TOKENS,
  },
  [DOLLY_POOL_NAME]: {
    lpToken: DOLLY_POOL_SWAP_TOKEN,
    poolTokens: DOLLY_POOL_TOKEN,
  },
  [DOLLY_MINT_POOL_NAME]: {
    lpToken: DOLLY_MINT_POOL_SWAP_TOKEN,
    poolTokens: DOLLY_MINT_POOL_TOKEN,
  },
  [BTC_POOL_NAME]: {
    lpToken: BTC_POOL_SWAP_TOKEN,
    poolTokens: BTC_POOL_TOKEN,
  },
};

export const TRANSACTION_TYPES = {
  DEPOSIT: "DEPOSIT",
  WITHDRAW: "WITHDRAW",
  SWAP: "SWAP",
};

export const MINT_TOKEN_MAP = [BUSD, USDT];
export const MINT_TOKEN_USER_INFO_MAP = [DOLLY, DOP, BUSD, USDT];

export const POOL_FEE_PRECISION = 10;
export const DEPLOYED_BLOCK = {
  [ChainId.BSC_MAINNET]: 11656944,
  [ChainId.BSC_TESTNET]: 7174462,
  [ChainId.HARDHAT]: 0,
};
export const POOL_STATS_URL = {
  [ChainId.BSC_MAINNET]: "https://ipfs.dopple.exchange/pool-stats.json",
  [ChainId.HARDHAT]:
    "https://mehmeta-team-bucket.storage.fleek.co/pool-stats-dev.json",
};

// Swap token List Condition
export const SWAP_TOKEN_CONDITION_LIST = {
  DAI: [BUSD, USDT, USDC],
  BUSD: [DAI, USDT, USDC, UST, DOLLY],
  USDT: [DAI, BUSD, USDC, UST, DOLLY],
  USDC: [DAI, BUSD, USDT],
  UST: [BUSD, USDT],
  DOLLY: [BUSD, USDT],
  BTCB: [renBTC],
  renBTC: [BTCB],
};

// External swap exchange

export class SwapExchange {
  constructor(name, factoryAddresses, routerAddresses, fee, decimals, icon) {
    this.name = name;
    this.factoryAddresses = factoryAddresses;
    this.routerAddresses = routerAddresses;
    this.fee = fee;
    this.decimals = decimals;
    this.icon = icon;
  }
}

// Pancake swap
export const PANCAKE_SWAP_FACTORY_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0xBCfCcbde45cE874adCB698cC183deBcF17952812",
  [ChainId.BSC_TESTNET]: "",
};

export const PANCAKE_SWAP_ROUTER_CONTRACT_ADDRESSES = {
  [ChainId.BSC_MAINNET]: "0x05fF2B0DB69458A0750badebc4f9e13aDd608C7F",
  [ChainId.BSC_TESTNET]: "",
};

export const PANCAKE_SWAP = new SwapExchange(
  "Pancake",
  PANCAKE_SWAP_FACTORY_CONTRACT_ADDRESSES,
  PANCAKE_SWAP_ROUTER_CONTRACT_ADDRESSES,
  0.3034,
  18,
  pancakeLogo
);

export const SWAP_EXCHANGE = [PANCAKE_SWAP];

export const TOKEN_LIST = {
  [DAI.symbol]: DAI,
  [USDT.symbol]: USDT,
  [USDC.symbol]: USDC,
  [BUSD.symbol]: BUSD,
  [DOP.symbol]: DOP,
  [DOLLY.symbol]: DOLLY,
  [UST.symbol]: UST,
  [BTCB.symbol]: BTCB,
  [renBTC.symbol]: renBTC,
};

export const FAIRLAUNCH_ABIS = {
  [ChainId.BSC_MAINNET]: FAIR_LAUNCH_ABIS_BSC,
  [ChainId.BSC_TESTNET]: FAIR_LAUNCH_ABIS_BSC_TESTNET,
};
