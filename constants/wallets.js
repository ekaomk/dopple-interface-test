import { injected, walletconnect, walletlink, bsc } from "../connectors";

export const wallets = [{
        name: "MetaMask",
        icon: "/images/icons/metamask.svg",
        connector: injected,
    },
    // {
    //   name: "Coinbase Wallet",
    //   icon: "/images/icons/coinbasewallet.svg",
    //   connector: walletlink,
    // },
    {
        name: "Binance Chain Wallet",
        icon: "/images/icons/bsc.svg",
        connector: bsc,
    },
    {
        name: "Wallet Connect",
        icon: "/images/icons/walletconnect.svg",
        connector: walletconnect,
    },
];